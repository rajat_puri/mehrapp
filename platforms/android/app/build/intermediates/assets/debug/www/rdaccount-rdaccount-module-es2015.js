(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["rdaccount-rdaccount-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/rdaccount/rdaccount.page.html":
/*!*************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/rdaccount/rdaccount.page.html ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n\t<ion-toolbar class=\"toolbar-blue\">\n\t\t<ion-buttons slot=\"start\">\n\t\t\t<ion-menu-button></ion-menu-button>\n\t\t</ion-buttons>\n\t\t<ion-title class=\"title-top\">\n\t\t\tStatement\n\t\t</ion-title>\n\t</ion-toolbar>\n</ion-header>\n<ion-item>\n\t<ion-label>Products</ion-label>\n\t<ion-select (ionChange)=\"onChange($event)\" placeholder=\"Select One\" [(ngModel)]=\"productSelected\">\n\t\t<ion-select-option *ngFor=\"let product of allProducts\" value=\"{{product.Id}}\">{{product.C_Product_Name}}</ion-select-option>\n\n\t</ion-select>\n</ion-item>\n<ion-datetime [(ngModel)]=\"startDate\" display-timezone=\"utc\"></ion-datetime>\n<ion-datetime [(ngModel)]=\"endDate\" display-timezone=\"utc\"></ion-datetime>\n<ion-button color=\"secondary\" (click)=\"fetchLedger()\">Get</ion-button>\n\n<ion-content>\n\t<div class=\"card-bg-full\">\n\n\n\t\t<ion-card class=\"card black-card-sm\">\n\t\t\t<ion-row class=\"row\">\n\t\t\t\t<ion-col class=\"col\" size=\"7\">\n\t\t\t\t\t<h3>RD Account</h3>\n\t\t\t\t\t<h2> {{ledgerDetails.accno}}</h2>\n\t\t\t\t</ion-col>\n\t\t\t\t<ion-col class=\"col\" size=\"5\" text-end>\n\t\t\t\t\t<h3>Available Balance</h3>\n\t\t\t\t\t<h2>₹ {{ledgerDetails.balance}}\n\t\t\t\t\t\t<!-- <sub>.00</sub> -->\n\t\t\t\t\t</h2>\n\t\t\t\t</ion-col>\n\t\t\t</ion-row>\n\t\t</ion-card>\n\n\t</div>\n\n\t<ion-grid class=\"successfully-footer ion-no-padding\">\n\t\t<ion-row class=\"successfully-footer\">\n\t\t\t<ion-col class=\"pa0\">\n\t\t\t\t<button class=\"filter\" ion-button block>\n\t\t\t\t\t<ion-icon name=\"clipboard\"></ion-icon> Download Statement</button>\n\t\t\t</ion-col>\n\n\t\t</ion-row>\n\t</ion-grid>\n\t<!-- List of Text Items -->\n\t<ion-list>\n\n\t\t<ion-item *ngFor=\"let ledgerDetail of ledgerDetails.ledgerdetail\">\n\t\t\t<ion-label>\n\t\t\t\t<div class=\"transaction-head\">{{ledgerDetail.particulars}}</div>\n\t\t\t\t<div class=\"transaction-date\">{{ledgerDetail.date}} </div>\n\t\t\t\t<div class=\"transaction-amt \" [ngClass]=\"ledgerDetail.entrytype=='Dr'?'red':'green'\">₹ {{ledgerDetail.Amount}}\n\t\t\t\t\t<sub>{{ledgerDetail.entrytype}}</sub>\n\t\t\t\t</div>\n\t\t\t</ion-label>\n\t\t</ion-item>\n\t\t<!-- <ion-item>\n\t\t\t<ion-label>\n\t\t\t\t<div class=\"transaction-head\">EMI Debited</div>\n\t\t\t\t<div class=\"transaction-date\">12 June, 2019 </div>\n\t\t\t\t<div class=\"transaction-amt red\">₹ 7676.00\n\t\t\t\t\t<sub>Dr</sub>\n\t\t\t\t</div>\n\t\t\t</ion-label>\n\t\t</ion-item>\n\t\t<ion-item>\n\t\t\t<ion-label>\n\t\t\t\t<div class=\"transaction-head\">EMI Debited</div>\n\t\t\t\t<div class=\"transaction-date\">12 June, 2019 </div>\n\t\t\t\t<div class=\"transaction-amt green\">₹ 22398.00\n\t\t\t\t\t<sub>Cr</sub>\n\t\t\t\t</div>\n\t\t\t</ion-label>\n\t\t</ion-item>\n\t\t<ion-item>\n\t\t\t<ion-label>\n\t\t\t\t<div class=\"transaction-head\">EMI Debited</div>\n\t\t\t\t<div class=\"transaction-date\">12 June, 2019 </div>\n\t\t\t\t<div class=\"transaction-amt red\">₹ 78764.00\n\t\t\t\t\t<sub>Dr</sub>\n\t\t\t\t</div>\n\t\t\t</ion-label>\n\t\t</ion-item>\n\t\t<ion-item>\n\t\t\t<ion-label>\n\t\t\t\t<div class=\"transaction-head\">EMI Debited</div>\n\t\t\t\t<div class=\"transaction-date\">12 June, 2019 </div>\n\t\t\t\t<div class=\"transaction-amt green\">₹ 455.00\n\t\t\t\t\t<sub>Cr</sub>\n\t\t\t\t</div>\n\t\t\t</ion-label>\n\t\t</ion-item>\n\t\t<ion-item>\n\t\t\t<ion-label>\n\t\t\t\t<div class=\"transaction-head\">EMI Debited</div>\n\t\t\t\t<div class=\"transaction-date\">12 June, 2019 </div>\n\t\t\t\t<div class=\"transaction-amt green\">₹ 4577.00\n\t\t\t\t\t<sub>Cr</sub>\n\t\t\t\t</div>\n\t\t\t</ion-label>\n\t\t</ion-item>\n\t\t<ion-item>\n\t\t\t<ion-label>\n\t\t\t\t<div class=\"transaction-head\">EMI Debited</div>\n\t\t\t\t<div class=\"transaction-date\">12 June, 2019 </div>\n\t\t\t\t<div class=\"transaction-amt red\">₹ 108100.00\n\t\t\t\t\t<sub>Dr</sub>\n\t\t\t\t</div>\n\t\t\t</ion-label>\n\t\t</ion-item>\n\t\t<ion-item>\n\t\t\t<ion-label>\n\t\t\t\t<div class=\"transaction-head\">EMI Debited</div>\n\t\t\t\t<div class=\"transaction-date\">12 June, 2019 </div>\n\t\t\t\t<div class=\"transaction-amt green\">₹ 1000.00\n\t\t\t\t\t<sub>Cr</sub>\n\t\t\t\t</div>\n\t\t\t</ion-label>\n\t\t</ion-item> -->\n\n\t</ion-list>\n\n</ion-content>"

/***/ }),

/***/ "./src/app/rdaccount/rdaccount-routing.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/rdaccount/rdaccount-routing.module.ts ***!
  \*******************************************************/
/*! exports provided: RdaccountPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RdaccountPageRoutingModule", function() { return RdaccountPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _rdaccount_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./rdaccount.page */ "./src/app/rdaccount/rdaccount.page.ts");




const routes = [
    {
        path: '',
        component: _rdaccount_page__WEBPACK_IMPORTED_MODULE_3__["RdaccountPage"]
    }
];
let RdaccountPageRoutingModule = class RdaccountPageRoutingModule {
};
RdaccountPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], RdaccountPageRoutingModule);



/***/ }),

/***/ "./src/app/rdaccount/rdaccount.module.ts":
/*!***********************************************!*\
  !*** ./src/app/rdaccount/rdaccount.module.ts ***!
  \***********************************************/
/*! exports provided: RdaccountPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RdaccountPageModule", function() { return RdaccountPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _rdaccount_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./rdaccount-routing.module */ "./src/app/rdaccount/rdaccount-routing.module.ts");
/* harmony import */ var _rdaccount_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./rdaccount.page */ "./src/app/rdaccount/rdaccount.page.ts");







let RdaccountPageModule = class RdaccountPageModule {
};
RdaccountPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _rdaccount_routing_module__WEBPACK_IMPORTED_MODULE_5__["RdaccountPageRoutingModule"]
        ],
        declarations: [_rdaccount_page__WEBPACK_IMPORTED_MODULE_6__["RdaccountPage"]]
    })
], RdaccountPageModule);



/***/ }),

/***/ "./src/app/rdaccount/rdaccount.page.scss":
/*!***********************************************!*\
  !*** ./src/app/rdaccount/rdaccount.page.scss ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3JkYWNjb3VudC9yZGFjY291bnQucGFnZS5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/rdaccount/rdaccount.page.ts":
/*!*********************************************!*\
  !*** ./src/app/rdaccount/rdaccount.page.ts ***!
  \*********************************************/
/*! exports provided: RdaccountPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RdaccountPage", function() { return RdaccountPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _services_user_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/user.service */ "./src/services/user.service.ts");
/* harmony import */ var _services_cookie_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/cookie.service */ "./src/services/cookie.service.ts");





let RdaccountPage = class RdaccountPage {
    constructor(alertController, loadingController, userService, cookieService) {
        this.alertController = alertController;
        this.loadingController = loadingController;
        this.userService = userService;
        this.cookieService = cookieService;
        this.startDate = '01/04/2016';
        this.endDate = '01/04/2020';
        this.ledgerDetails = [];
        this.allProducts = [];
        this.productSelected = "";
    }
    ngOnInit() {
        this.fetchProducts();
    }
    presentAlert(type, subHeader, message) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                header: type,
                subHeader: subHeader,
                message: message,
                buttons: ['OK']
            });
            yield alert.present();
        });
    }
    fetchLedger() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            this.ledgerDetails = [];
            if (this.productSelected == "") {
                this.presentAlert("Error", "", "Please Select a Product");
                return false;
            }
            var dataToSend = {
                startDate: this.startDate,
                endDate: this.endDate,
                accTye: '5',
                productId: this.productSelected
            };
            const loading = yield this.loadingController.create({
                message: 'loading...',
            });
            yield loading.present();
            this.userService.fetchLedger(dataToSend).subscribe((res) => {
                loading.dismiss();
                if (res != '') {
                    var details = JSON.parse(res);
                    this.ledgerDetails = details[0];
                    console.log(this.ledgerDetails);
                }
            }, (err) => {
                console.log(err);
                loading.dismiss();
                this.presentAlert("Error", "", "Please Select Correct Dates");
            });
        });
    }
    onChange(event) {
        this.ledgerDetails = [];
    }
    fetchProducts() {
        this.userService.fetchProducts('5').subscribe((res) => {
            console.log(JSON.parse(res));
            this.allProducts = JSON.parse(res);
        }, (err) => {
            console.log(err);
        });
    }
};
RdaccountPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] },
    { type: _services_user_service__WEBPACK_IMPORTED_MODULE_3__["UserService"] },
    { type: _services_cookie_service__WEBPACK_IMPORTED_MODULE_4__["CookieService"] }
];
RdaccountPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-rdaccount',
        template: __webpack_require__(/*! raw-loader!./rdaccount.page.html */ "./node_modules/raw-loader/index.js!./src/app/rdaccount/rdaccount.page.html"),
        providers: [_services_user_service__WEBPACK_IMPORTED_MODULE_3__["UserService"], _services_cookie_service__WEBPACK_IMPORTED_MODULE_4__["CookieService"]],
        styles: [__webpack_require__(/*! ./rdaccount.page.scss */ "./src/app/rdaccount/rdaccount.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"], _services_user_service__WEBPACK_IMPORTED_MODULE_3__["UserService"], _services_cookie_service__WEBPACK_IMPORTED_MODULE_4__["CookieService"]])
], RdaccountPage);



/***/ }),

/***/ "./src/services/cookie.service.ts":
/*!****************************************!*\
  !*** ./src/services/cookie.service.ts ***!
  \****************************************/
/*! exports provided: CookieService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CookieService", function() { return CookieService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");



let CookieService = class CookieService {
    constructor(router) {
        this.router = router;
    }
    setItem(sKey, sValue) {
        // sDomain = environment.domain;
        localStorage.setItem(sKey, sValue);
        return true;
    }
    getItem(sKey) {
        if (!sKey) {
            return null;
        }
        return localStorage.getItem(sKey);
    }
    removeSingleItem(id) {
        localStorage.removeItem(id);
    }
    removeCookies() {
        localStorage.clear();
    }
};
CookieService.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
];
CookieService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
], CookieService);



/***/ })

}]);
//# sourceMappingURL=rdaccount-rdaccount-module-es2015.js.map