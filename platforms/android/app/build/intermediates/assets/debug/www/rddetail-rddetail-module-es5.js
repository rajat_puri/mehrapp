(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["rddetail-rddetail-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/rddetail/rddetail.page.html":
/*!***********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/rddetail/rddetail.page.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header  >\n  <ion-toolbar class=\"toolbar-blue\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <ion-title class=\"title-top\">\n     Recurring Deposit\n    </ion-title>\n  </ion-toolbar>\n</ion-header>\n\n\n\n<ion-content>\n<div class=\"card-bg-full\">\n\n\n<ion-card class=\"card black-card-sm\">\n\t\t\t<ion-row class=\"row\">\n\t\t\t\t<ion-col class=\"col\" size=\"7\">\n\t\t\t\t\t<h3>RD No.</h3>\n\t\t\t\t\t<h2>0612 0150 3518</h2>\n\t\t\t\t</ion-col>\n\t\t\t\t<ion-col class=\"col\" size=\"5\" text-end>\n\t\t\t\t\t<h3>Target Amount</h3>\n\t\t\t\t\t<h2>₹ 10,0000<sub>.00</sub></h2>\n\t\t\t\t</ion-col>\n\t\t\t</ion-row>\n\t\t\t<ion-row class=\"row\">\n\t\t\t\t<ion-col class=\"col\" size=\"7\">\n\t\t\t\t\t<h3>Tenure</h3>\n\t\t\t\t\t<h2>1<sub> Year</sub></h2>\n\t\t\t\t</ion-col>\n\t\t\t\t<ion-col class=\"col\" size=\"5\" text-end>\n\t\t\t\t\t<h3>Remaining Amount</h3>\n\t\t\t\t\t<h2>₹ 40,0000<sub>.00</sub></h2>\n\t\t\t\t</ion-col>\n\t\t\t</ion-row>\n\t\t</ion-card>\n\t\n</div>\n\n\n<!-- List of Text Items -->\n<ion-list>\n\n    <ion-item>\n    <ion-label>\n\t<div class=\"transaction-head\">EMI Debited</div>\t\n\t<div class=\"transaction-date\">12 June, 2019 </div>\n\t<div class=\"transaction-amt green\">₹ 5000.00  <sub>Cr</sub></div>\n\t</ion-label>\n  </ion-item>\n   \n    <ion-item>\n    <ion-label>\n\t<div class=\"transaction-head\">EMI Debited</div>\t\n\t<div class=\"transaction-date\">12 June, 2019 </div>\n\t<div class=\"transaction-amt green\">₹ 5000.00  <sub>Cr</sub></div>\n\t</ion-label>\n  </ion-item>\n   \n    <ion-item>\n    <ion-label>\n\t<div class=\"transaction-head\">EMI Debited</div>\t\n\t<div class=\"transaction-date\">12 June, 2019 </div>\n\t<div class=\"transaction-amt green\">₹ 5000.00  <sub>Cr</sub></div>\n\t</ion-label>\n  </ion-item>\n   \n    <ion-item>\n    <ion-label>\n\t<div class=\"transaction-head\">EMI Debited</div>\t\n\t<div class=\"transaction-date\">12 June, 2019 </div>\n\t<div class=\"transaction-amt green\">₹ 5000.00  <sub>Cr</sub></div>\n\t</ion-label>\n  </ion-item>\n   \n    <ion-item>\n    <ion-label>\n\t<div class=\"transaction-head\">EMI Debited</div>\t\n\t<div class=\"transaction-date\">12 June, 2019 </div>\n\t<div class=\"transaction-amt green\">₹ 5000.00  <sub>Cr</sub></div>\n\t</ion-label>\n  </ion-item>\n   \n    <ion-item>\n    <ion-label>\n\t<div class=\"transaction-head\">EMI Debited</div>\t\n\t<div class=\"transaction-date\">12 June, 2019 </div>\n\t<div class=\"transaction-amt green\">₹ 5000.00  <sub>Cr</sub></div>\n\t</ion-label>\n  </ion-item>\n   \n    <ion-item>\n    <ion-label>\n\t<div class=\"transaction-head\">EMI Debited</div>\t\n\t<div class=\"transaction-date\">12 June, 2019 </div>\n\t<div class=\"transaction-amt green\">₹ 5000.00  <sub>Cr</sub></div>\n\t</ion-label>\n  </ion-item>\n   \n    <ion-item>\n    <ion-label>\n\t<div class=\"transaction-head\">EMI Debited</div>\t\n\t<div class=\"transaction-date\">12 June, 2019 </div>\n\t<div class=\"transaction-amt green\">₹ 5000.00  <sub>Cr</sub></div>\n\t</ion-label>\n  </ion-item>\n   \n    <ion-item>\n    <ion-label>\n\t<div class=\"transaction-head\">EMI Debited</div>\t\n\t<div class=\"transaction-date\">12 June, 2019 </div>\n\t<div class=\"transaction-amt green\">₹ 5000.00  <sub>Cr</sub></div>\n\t</ion-label>\n  </ion-item>\n   \n    <ion-item>\n    <ion-label>\n\t<div class=\"transaction-head\">EMI Debited</div>\t\n\t<div class=\"transaction-date\">12 June, 2019 </div>\n\t<div class=\"transaction-amt green\">₹ 5000.00  <sub>Cr</sub></div>\n\t</ion-label>\n  </ion-item>\n   \n    <ion-item>\n    <ion-label>\n\t<div class=\"transaction-head\">EMI Debited</div>\t\n\t<div class=\"transaction-date\">12 June, 2019 </div>\n\t<div class=\"transaction-amt green\">₹ 5000.00  <sub>Cr</sub></div>\n\t</ion-label>\n  </ion-item>\n   \n  \n</ion-list>\n\n</ion-content>\n\n<ion-footer no-border class=\"successfully-footer\">\n\n   <ion-grid class=\"successfully-footer\">\n  <ion-row class=\"successfully-footer\">\n \n    <ion-col class=\"successfully-footer\">\n   <button [routerLink]=\"['/deposit']\" class=\"btngreen\" ion-button block>Add Deposit in RD</button>\n    </ion-col>\n  \n   \n  </ion-row>\n</ion-grid>\n\n</ion-footer>\n"

/***/ }),

/***/ "./src/app/rddetail/rddetail-routing.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/rddetail/rddetail-routing.module.ts ***!
  \*****************************************************/
/*! exports provided: RddetailPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RddetailPageRoutingModule", function() { return RddetailPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _rddetail_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./rddetail.page */ "./src/app/rddetail/rddetail.page.ts");




var routes = [
    {
        path: '',
        component: _rddetail_page__WEBPACK_IMPORTED_MODULE_3__["RddetailPage"]
    }
];
var RddetailPageRoutingModule = /** @class */ (function () {
    function RddetailPageRoutingModule() {
    }
    RddetailPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
        })
    ], RddetailPageRoutingModule);
    return RddetailPageRoutingModule;
}());



/***/ }),

/***/ "./src/app/rddetail/rddetail.module.ts":
/*!*********************************************!*\
  !*** ./src/app/rddetail/rddetail.module.ts ***!
  \*********************************************/
/*! exports provided: RddetailPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RddetailPageModule", function() { return RddetailPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _rddetail_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./rddetail-routing.module */ "./src/app/rddetail/rddetail-routing.module.ts");
/* harmony import */ var _rddetail_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./rddetail.page */ "./src/app/rddetail/rddetail.page.ts");







var RddetailPageModule = /** @class */ (function () {
    function RddetailPageModule() {
    }
    RddetailPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _rddetail_routing_module__WEBPACK_IMPORTED_MODULE_5__["RddetailPageRoutingModule"]
            ],
            declarations: [_rddetail_page__WEBPACK_IMPORTED_MODULE_6__["RddetailPage"]]
        })
    ], RddetailPageModule);
    return RddetailPageModule;
}());



/***/ }),

/***/ "./src/app/rddetail/rddetail.page.scss":
/*!*********************************************!*\
  !*** ./src/app/rddetail/rddetail.page.scss ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".black-card-sm {\n  background: url('item_bg.png');\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcmRkZXRhaWwvRTpcXG1laGFyLWJhbmstYXBwLW1hc3RlclxcbWVoYXItYmFuay1hcHAtbWFzdGVyL3NyY1xcYXBwXFxyZGRldGFpbFxccmRkZXRhaWwucGFnZS5zY3NzIiwic3JjL2FwcC9yZGRldGFpbC9yZGRldGFpbC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSw4QkFBQTtBQ0NKIiwiZmlsZSI6InNyYy9hcHAvcmRkZXRhaWwvcmRkZXRhaWwucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmJsYWNrLWNhcmQtc20ge1xuICAgIGJhY2tncm91bmQ6IHVybCguLi8uLi9hc3NldHMvaW1ncy9pdGVtX2JnLnBuZyk7XG5cdH0iLCIuYmxhY2stY2FyZC1zbSB7XG4gIGJhY2tncm91bmQ6IHVybCguLi8uLi9hc3NldHMvaW1ncy9pdGVtX2JnLnBuZyk7XG59Il19 */"

/***/ }),

/***/ "./src/app/rddetail/rddetail.page.ts":
/*!*******************************************!*\
  !*** ./src/app/rddetail/rddetail.page.ts ***!
  \*******************************************/
/*! exports provided: RddetailPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RddetailPage", function() { return RddetailPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var RddetailPage = /** @class */ (function () {
    function RddetailPage() {
    }
    RddetailPage.prototype.ngOnInit = function () {
    };
    RddetailPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-rddetail',
            template: __webpack_require__(/*! raw-loader!./rddetail.page.html */ "./node_modules/raw-loader/index.js!./src/app/rddetail/rddetail.page.html"),
            styles: [__webpack_require__(/*! ./rddetail.page.scss */ "./src/app/rddetail/rddetail.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], RddetailPage);
    return RddetailPage;
}());



/***/ })

}]);
//# sourceMappingURL=rddetail-rddetail-module-es5.js.map