(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["loanaccount-loanaccount-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/loanaccount/loanaccount.page.html":
/*!*****************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/loanaccount/loanaccount.page.html ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n\t<ion-toolbar class=\"toolbar-blue\">\n\t\t<ion-buttons slot=\"start\">\n\t\t\t<ion-menu-button></ion-menu-button>\n\t\t</ion-buttons>\n\t\t<ion-title class=\"title-top\">\n\t\t\tLoan Account Statements\n\t\t</ion-title>\n\t</ion-toolbar>\n</ion-header>\n<ion-item>\n\t<ion-label>Products</ion-label>\n\t<ion-select (ionChange)=\"onChange($event)\" placeholder=\"Select One\" [(ngModel)]=\"productSelected\">\n\t\t<ion-select-option *ngFor=\"let product of allProducts\" value=\"{{product.Id}}\">{{product.C_Product_Name}}</ion-select-option>\n\n\t</ion-select>\n</ion-item>\n<ion-datetime [(ngModel)]=\"startDate\" display-timezone=\"utc\"></ion-datetime>\n<ion-datetime [(ngModel)]=\"endDate\" display-timezone=\"utc\"></ion-datetime>\n<ion-button color=\"secondary\" (click)=\"fetchLedger()\">Get</ion-button>\n\n<ion-content>\n\t<div class=\"card-bg-full\" *ngIf=\"ledgerDetails.length>0\">\n\n\n\t\t<ion-card class=\"card black-card-sm\">\n\t\t\t<ion-row class=\"row\">\n\t\t\t\t<ion-col class=\"col\" size=\"7\">\n\t\t\t\t\t<h3>Saving Account</h3>\n\t\t\t\t\t<h2>{{ledgerDetails.AccName}} {{ledgerDetails.accno}}</h2>\n\t\t\t\t</ion-col>\n\t\t\t\t<ion-col class=\"col\" size=\"5\" text-end>\n\t\t\t\t\t<h3>Available Balance</h3>\n\t\t\t\t\t<h2>₹ {{ledgerDetails.balance}}\n\t\t\t\t\t\t<!-- <sub>.00</sub> -->\n\t\t\t\t\t</h2>\n\t\t\t\t</ion-col>\n\t\t\t</ion-row>\n\t\t</ion-card>\n\n\t</div>\n\n\t<ion-grid class=\"successfully-footer ion-no-padding\">\n\t\t<ion-row class=\"successfully-footer\">\n\t\t\t<ion-col class=\"pa0\">\n\t\t\t\t<button class=\"filter\" ion-button block>\n\t\t\t\t\t<ion-icon name=\"clipboard\"></ion-icon> Download Statement</button>\n\t\t\t</ion-col>\n\n\t\t</ion-row>\n\t</ion-grid>\n\t<!-- List of Text Items -->\n\t<ion-list>\n\n\t\t<ion-item *ngFor=\"let ledgerDetail of ledgerDetails.ledgerdetail\">\n\t\t\t<ion-label>\n\t\t\t\t<div class=\"transaction-head\">{{ledgerDetail.particulars}}</div>\n\t\t\t\t<div class=\"transaction-date\">{{ledgerDetail.date}} </div>\n\t\t\t\t<div class=\"transaction-amt \" [ngClass]=\"ledgerDetail.entrytype=='Dr'?'red':'green'\">₹ {{ledgerDetail.Amount}}\n\t\t\t\t\t<sub>{{ledgerDetail.entrytype}}</sub>\n\t\t\t\t</div>\n\t\t\t</ion-label>\n\t\t</ion-item>\n\t\t<!-- <ion-item>\n\t\t\t  <ion-label>\n\t\t\t\t  <div class=\"transaction-head\">EMI Debited</div>\n\t\t\t\t  <div class=\"transaction-date\">12 June, 2019 </div>\n\t\t\t\t  <div class=\"transaction-amt red\">₹ 7676.00\n\t\t\t\t\t  <sub>Dr</sub>\n\t\t\t\t  </div>\n\t\t\t  </ion-label>\n\t\t  </ion-item>\n\t\t  <ion-item>\n\t\t\t  <ion-label>\n\t\t\t\t  <div class=\"transaction-head\">EMI Debited</div>\n\t\t\t\t  <div class=\"transaction-date\">12 June, 2019 </div>\n\t\t\t\t  <div class=\"transaction-amt green\">₹ 22398.00\n\t\t\t\t\t  <sub>Cr</sub>\n\t\t\t\t  </div>\n\t\t\t  </ion-label>\n\t\t  </ion-item>\n\t\t  <ion-item>\n\t\t\t  <ion-label>\n\t\t\t\t  <div class=\"transaction-head\">EMI Debited</div>\n\t\t\t\t  <div class=\"transaction-date\">12 June, 2019 </div>\n\t\t\t\t  <div class=\"transaction-amt red\">₹ 78764.00\n\t\t\t\t\t  <sub>Dr</sub>\n\t\t\t\t  </div>\n\t\t\t  </ion-label>\n\t\t  </ion-item>\n\t\t  <ion-item>\n\t\t\t  <ion-label>\n\t\t\t\t  <div class=\"transaction-head\">EMI Debited</div>\n\t\t\t\t  <div class=\"transaction-date\">12 June, 2019 </div>\n\t\t\t\t  <div class=\"transaction-amt green\">₹ 455.00\n\t\t\t\t\t  <sub>Cr</sub>\n\t\t\t\t  </div>\n\t\t\t  </ion-label>\n\t\t  </ion-item>\n\t\t  <ion-item>\n\t\t\t  <ion-label>\n\t\t\t\t  <div class=\"transaction-head\">EMI Debited</div>\n\t\t\t\t  <div class=\"transaction-date\">12 June, 2019 </div>\n\t\t\t\t  <div class=\"transaction-amt green\">₹ 4577.00\n\t\t\t\t\t  <sub>Cr</sub>\n\t\t\t\t  </div>\n\t\t\t  </ion-label>\n\t\t  </ion-item>\n\t\t  <ion-item>\n\t\t\t  <ion-label>\n\t\t\t\t  <div class=\"transaction-head\">EMI Debited</div>\n\t\t\t\t  <div class=\"transaction-date\">12 June, 2019 </div>\n\t\t\t\t  <div class=\"transaction-amt red\">₹ 108100.00\n\t\t\t\t\t  <sub>Dr</sub>\n\t\t\t\t  </div>\n\t\t\t  </ion-label>\n\t\t  </ion-item>\n\t\t  <ion-item>\n\t\t\t  <ion-label>\n\t\t\t\t  <div class=\"transaction-head\">EMI Debited</div>\n\t\t\t\t  <div class=\"transaction-date\">12 June, 2019 </div>\n\t\t\t\t  <div class=\"transaction-amt green\">₹ 1000.00\n\t\t\t\t\t  <sub>Cr</sub>\n\t\t\t\t  </div>\n\t\t\t  </ion-label>\n\t\t  </ion-item> -->\n\n\t</ion-list>\n\n</ion-content>"

/***/ }),

/***/ "./src/app/loanaccount/loanaccount-routing.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/loanaccount/loanaccount-routing.module.ts ***!
  \***********************************************************/
/*! exports provided: LoanaccountPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoanaccountPageRoutingModule", function() { return LoanaccountPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _loanaccount_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./loanaccount.page */ "./src/app/loanaccount/loanaccount.page.ts");




var routes = [
    {
        path: '',
        component: _loanaccount_page__WEBPACK_IMPORTED_MODULE_3__["LoanaccountPage"]
    }
];
var LoanaccountPageRoutingModule = /** @class */ (function () {
    function LoanaccountPageRoutingModule() {
    }
    LoanaccountPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
        })
    ], LoanaccountPageRoutingModule);
    return LoanaccountPageRoutingModule;
}());



/***/ }),

/***/ "./src/app/loanaccount/loanaccount.module.ts":
/*!***************************************************!*\
  !*** ./src/app/loanaccount/loanaccount.module.ts ***!
  \***************************************************/
/*! exports provided: LoanaccountPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoanaccountPageModule", function() { return LoanaccountPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _loanaccount_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./loanaccount-routing.module */ "./src/app/loanaccount/loanaccount-routing.module.ts");
/* harmony import */ var _loanaccount_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./loanaccount.page */ "./src/app/loanaccount/loanaccount.page.ts");







var LoanaccountPageModule = /** @class */ (function () {
    function LoanaccountPageModule() {
    }
    LoanaccountPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _loanaccount_routing_module__WEBPACK_IMPORTED_MODULE_5__["LoanaccountPageRoutingModule"]
            ],
            declarations: [_loanaccount_page__WEBPACK_IMPORTED_MODULE_6__["LoanaccountPage"]]
        })
    ], LoanaccountPageModule);
    return LoanaccountPageModule;
}());



/***/ }),

/***/ "./src/app/loanaccount/loanaccount.page.scss":
/*!***************************************************!*\
  !*** ./src/app/loanaccount/loanaccount.page.scss ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".black-card-sm {\n  background: url('item_bg.png');\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbG9hbmFjY291bnQvRTpcXG1laGFyLWJhbmstYXBwLW1hc3RlclxcbWVoYXItYmFuay1hcHAtbWFzdGVyL3NyY1xcYXBwXFxsb2FuYWNjb3VudFxcbG9hbmFjY291bnQucGFnZS5zY3NzIiwic3JjL2FwcC9sb2FuYWNjb3VudC9sb2FuYWNjb3VudC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSw4QkFBQTtBQ0NKIiwiZmlsZSI6InNyYy9hcHAvbG9hbmFjY291bnQvbG9hbmFjY291bnQucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmJsYWNrLWNhcmQtc20ge1xuICAgIGJhY2tncm91bmQ6IHVybCguLi8uLi9hc3NldHMvaW1ncy9pdGVtX2JnLnBuZyk7XG5cdH0iLCIuYmxhY2stY2FyZC1zbSB7XG4gIGJhY2tncm91bmQ6IHVybCguLi8uLi9hc3NldHMvaW1ncy9pdGVtX2JnLnBuZyk7XG59Il19 */"

/***/ }),

/***/ "./src/app/loanaccount/loanaccount.page.ts":
/*!*************************************************!*\
  !*** ./src/app/loanaccount/loanaccount.page.ts ***!
  \*************************************************/
/*! exports provided: LoanaccountPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoanaccountPage", function() { return LoanaccountPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _services_user_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/user.service */ "./src/services/user.service.ts");
/* harmony import */ var _services_cookie_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/cookie.service */ "./src/services/cookie.service.ts");





var LoanaccountPage = /** @class */ (function () {
    function LoanaccountPage(alertController, loadingController, userService, cookieService) {
        this.alertController = alertController;
        this.loadingController = loadingController;
        this.userService = userService;
        this.cookieService = cookieService;
        this.startDate = '01/04/2016';
        this.endDate = '01/04/2020';
        this.ledgerDetails = [];
        this.allProducts = [];
        this.productSelected = "";
    }
    LoanaccountPage.prototype.ngOnInit = function () {
        this.fetchProducts();
    };
    LoanaccountPage.prototype.presentAlert = function (type, subHeader, message) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var alert;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertController.create({
                            header: type,
                            subHeader: subHeader,
                            message: message,
                            buttons: ['OK']
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    LoanaccountPage.prototype.fetchLedger = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var dataToSend, loading;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (this.productSelected == "") {
                            this.presentAlert("Error", "", "Please Select a Product");
                            return [2 /*return*/, false];
                        }
                        dataToSend = {
                            startDate: this.startDate,
                            endDate: this.endDate,
                            accTye: '1',
                            productId: this.productSelected
                        };
                        return [4 /*yield*/, this.loadingController.create({
                                message: 'loading...',
                            })];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, loading.present()];
                    case 2:
                        _a.sent();
                        this.userService.fetchLedger(dataToSend).subscribe(function (res) {
                            loading.dismiss();
                            if (res != '') {
                                var details = JSON.parse(res);
                                _this.ledgerDetails = details[0];
                                console.log(_this.ledgerDetails);
                            }
                        }, function (err) {
                            console.log(err);
                            loading.dismiss();
                            _this.presentAlert("Error", "", "Please Select Correct Dates");
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    LoanaccountPage.prototype.onChange = function (event) {
        this.ledgerDetails = [];
    };
    LoanaccountPage.prototype.fetchProducts = function () {
        var _this = this;
        this.userService.fetchProducts('1').subscribe(function (res) {
            console.log(JSON.parse(res));
            _this.allProducts = JSON.parse(res);
        }, function (err) {
            console.log(err);
        });
    };
    LoanaccountPage.ctorParameters = function () { return [
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] },
        { type: _services_user_service__WEBPACK_IMPORTED_MODULE_3__["UserService"] },
        { type: _services_cookie_service__WEBPACK_IMPORTED_MODULE_4__["CookieService"] }
    ]; };
    LoanaccountPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-loanaccount',
            template: __webpack_require__(/*! raw-loader!./loanaccount.page.html */ "./node_modules/raw-loader/index.js!./src/app/loanaccount/loanaccount.page.html"),
            providers: [_services_user_service__WEBPACK_IMPORTED_MODULE_3__["UserService"], _services_cookie_service__WEBPACK_IMPORTED_MODULE_4__["CookieService"]],
            styles: [__webpack_require__(/*! ./loanaccount.page.scss */ "./src/app/loanaccount/loanaccount.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"], _services_user_service__WEBPACK_IMPORTED_MODULE_3__["UserService"], _services_cookie_service__WEBPACK_IMPORTED_MODULE_4__["CookieService"]])
    ], LoanaccountPage);
    return LoanaccountPage;
}());



/***/ }),

/***/ "./src/services/cookie.service.ts":
/*!****************************************!*\
  !*** ./src/services/cookie.service.ts ***!
  \****************************************/
/*! exports provided: CookieService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CookieService", function() { return CookieService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");



var CookieService = /** @class */ (function () {
    function CookieService(router) {
        this.router = router;
    }
    CookieService.prototype.setItem = function (sKey, sValue) {
        // sDomain = environment.domain;
        localStorage.setItem(sKey, sValue);
        return true;
    };
    CookieService.prototype.getItem = function (sKey) {
        if (!sKey) {
            return null;
        }
        return localStorage.getItem(sKey);
    };
    CookieService.prototype.removeSingleItem = function (id) {
        localStorage.removeItem(id);
    };
    CookieService.prototype.removeCookies = function () {
        localStorage.clear();
    };
    CookieService.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
    ]; };
    CookieService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], CookieService);
    return CookieService;
}());



/***/ })

}]);
//# sourceMappingURL=loanaccount-loanaccount-module-es5.js.map