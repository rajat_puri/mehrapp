(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["agritoolbooked-agritoolbooked-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/agritoolbooked/agritoolbooked.page.html":
/*!***********************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/agritoolbooked/agritoolbooked.page.html ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar class=\"toolbar-blue\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <ion-title class=\"title-top\">\n      Tool Booked\n    </ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\n\n\n\n <ion-row class=\"mt10\">\n    <ion-col  [routerLink]=\"['/agritooldetail']\" size=\"6\">\n\t  <ion-card class=\"pa0 ma0 bg-white pa10 ion-text-center\" >\n<ion-row>\n    <ion-col size=\"12\"> <img src=\"assets/imgs/product1.png\">  </ion-col>\n  </ion-row>\n  <div class=\"black-heading mb5\">Bakhsish Combines</div>\n  <div class=\"small-gray mb0  mt5\">You Booked this tool on 2nd dec 2019</div>\n  <div class=\"small-gray mb0 mt5\">for 12-12-2019 to 28-12-2019</div>\n </ion-card>\n\t \n    </ion-col>\n\t <ion-col  [routerLink]=\"['/agritooldetail']\" size=\"6\">\n\t  <ion-card class=\"pa0 ma0 bg-white pa10 ion-text-center\" >\n<ion-row>\n    <ion-col size=\"12\"> <img src=\"assets/imgs/product1.png\">  </ion-col>\n  </ion-row>\n  <div class=\"black-heading mb5\">Bakhsish Combines</div>\n  <div class=\"small-gray mb0  mt5\">You Booked this tool on 2nd dec 2019</div>\n  <div class=\"small-gray mb0 mt5\">for 12-12-2019 to 28-12-2019</div>\n </ion-card>\n\t \n    </ion-col>\n\t <ion-col  [routerLink]=\"['/agritooldetail']\" size=\"6\">\n\t  <ion-card class=\"pa0 ma0 bg-white pa10 ion-text-center\" >\n<ion-row>\n    <ion-col size=\"12\"> <img src=\"assets/imgs/product1.png\">  </ion-col>\n  </ion-row>\n  <div class=\"black-heading mb5\">Bakhsish Combines</div>\n  <div class=\"small-gray mb0  mt5\">You Booked this tool on 2nd dec 2019</div>\n  <div class=\"small-gray mb0 mt5\">for 12-12-2019 to 28-12-2019</div>\n </ion-card>\n\t \n    </ion-col>\n\t <ion-col  [routerLink]=\"['/agritooldetail']\" size=\"6\">\n\t  <ion-card class=\"pa0 ma0 bg-white pa10 ion-text-center\" >\n<ion-row>\n    <ion-col size=\"12\"> <img src=\"assets/imgs/product1.png\">  </ion-col>\n  </ion-row>\n  <div class=\"black-heading mb5\">Bakhsish Combines</div>\n  <div class=\"small-gray mb0  mt5\">You Booked this tool on 2nd dec 2019</div>\n  <div class=\"small-gray mb0 mt5\">for 12-12-2019 to 28-12-2019</div>\n </ion-card>\t \n    </ion-col>  \n  </ion-row>\n\n\n\n\n\n\n\n\n</ion-content>"

/***/ }),

/***/ "./src/app/agritoolbooked/agritoolbooked-routing.module.ts":
/*!*****************************************************************!*\
  !*** ./src/app/agritoolbooked/agritoolbooked-routing.module.ts ***!
  \*****************************************************************/
/*! exports provided: AgritoolbookedPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AgritoolbookedPageRoutingModule", function() { return AgritoolbookedPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _agritoolbooked_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./agritoolbooked.page */ "./src/app/agritoolbooked/agritoolbooked.page.ts");




const routes = [
    {
        path: '',
        component: _agritoolbooked_page__WEBPACK_IMPORTED_MODULE_3__["AgritoolbookedPage"]
    }
];
let AgritoolbookedPageRoutingModule = class AgritoolbookedPageRoutingModule {
};
AgritoolbookedPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], AgritoolbookedPageRoutingModule);



/***/ }),

/***/ "./src/app/agritoolbooked/agritoolbooked.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/agritoolbooked/agritoolbooked.module.ts ***!
  \*********************************************************/
/*! exports provided: AgritoolbookedPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AgritoolbookedPageModule", function() { return AgritoolbookedPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _agritoolbooked_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./agritoolbooked-routing.module */ "./src/app/agritoolbooked/agritoolbooked-routing.module.ts");
/* harmony import */ var _agritoolbooked_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./agritoolbooked.page */ "./src/app/agritoolbooked/agritoolbooked.page.ts");







let AgritoolbookedPageModule = class AgritoolbookedPageModule {
};
AgritoolbookedPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _agritoolbooked_routing_module__WEBPACK_IMPORTED_MODULE_5__["AgritoolbookedPageRoutingModule"]
        ],
        declarations: [_agritoolbooked_page__WEBPACK_IMPORTED_MODULE_6__["AgritoolbookedPage"]]
    })
], AgritoolbookedPageModule);



/***/ }),

/***/ "./src/app/agritoolbooked/agritoolbooked.page.scss":
/*!*********************************************************!*\
  !*** ./src/app/agritoolbooked/agritoolbooked.page.scss ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-content {\n  --ion-background-color:#f3f5f5;\n}\n\nion-item {\n  --ion-background-color:#fff;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYWdyaXRvb2xib29rZWQvRTpcXG1laGFyLWJhbmstYXBwLW1hc3RlclxcbWVoYXItYmFuay1hcHAtbWFzdGVyL3NyY1xcYXBwXFxhZ3JpdG9vbGJvb2tlZFxcYWdyaXRvb2xib29rZWQucGFnZS5zY3NzIiwic3JjL2FwcC9hZ3JpdG9vbGJvb2tlZC9hZ3JpdG9vbGJvb2tlZC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSw4QkFBQTtBQ0NKOztBREVBO0VBQ0csMkJBQUE7QUNDSCIsImZpbGUiOiJzcmMvYXBwL2Fncml0b29sYm9va2VkL2Fncml0b29sYm9va2VkLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1jb250ZW50e1xuICAgIC0taW9uLWJhY2tncm91bmQtY29sb3I6I2YzZjVmNTtcbn1cblxuaW9uLWl0ZW17XG4gICAtLWlvbi1iYWNrZ3JvdW5kLWNvbG9yOiNmZmY7XG59XG4iLCJpb24tY29udGVudCB7XG4gIC0taW9uLWJhY2tncm91bmQtY29sb3I6I2YzZjVmNTtcbn1cblxuaW9uLWl0ZW0ge1xuICAtLWlvbi1iYWNrZ3JvdW5kLWNvbG9yOiNmZmY7XG59Il19 */"

/***/ }),

/***/ "./src/app/agritoolbooked/agritoolbooked.page.ts":
/*!*******************************************************!*\
  !*** ./src/app/agritoolbooked/agritoolbooked.page.ts ***!
  \*******************************************************/
/*! exports provided: AgritoolbookedPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AgritoolbookedPage", function() { return AgritoolbookedPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let AgritoolbookedPage = class AgritoolbookedPage {
    constructor() { }
    ngOnInit() {
    }
};
AgritoolbookedPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-agritoolbooked',
        template: __webpack_require__(/*! raw-loader!./agritoolbooked.page.html */ "./node_modules/raw-loader/index.js!./src/app/agritoolbooked/agritoolbooked.page.html"),
        styles: [__webpack_require__(/*! ./agritoolbooked.page.scss */ "./src/app/agritoolbooked/agritoolbooked.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], AgritoolbookedPage);



/***/ })

}]);
//# sourceMappingURL=agritoolbooked-agritoolbooked-module-es2015.js.map