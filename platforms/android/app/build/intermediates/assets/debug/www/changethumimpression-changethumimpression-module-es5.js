(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["changethumimpression-changethumimpression-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/changethumimpression/changethumimpression.page.html":
/*!***********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/changethumimpression/changethumimpression.page.html ***!
  \***********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header  >\n  <ion-toolbar class=\"toolbar-blue\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <ion-title class=\"title-top\">\n  Change Fingerprint Impression\n    </ion-title>\n  </ion-toolbar>\n</ion-header>\n\n\n\n<ion-content>\n<div class=\"pa15\">\n    <div class=\"thumb_impression mt30\" text-center=\"\">\n      <h2>Scan you Fingerprint to Update</h2>\n      <div class=\"fingerprint\">\n        <img src=\"assets/imgs/ic_thumb.png\">\n      </div>\n\t        \n    </div>\n\n<button  class=\"btngreen mt30\" ion-button block outline [routerLink]=\"['/login']\">Sign In</button>\n</div>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/changethumimpression/changethumimpression-routing.module.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/changethumimpression/changethumimpression-routing.module.ts ***!
  \*****************************************************************************/
/*! exports provided: ChangethumimpressionPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChangethumimpressionPageRoutingModule", function() { return ChangethumimpressionPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _changethumimpression_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./changethumimpression.page */ "./src/app/changethumimpression/changethumimpression.page.ts");




var routes = [
    {
        path: '',
        component: _changethumimpression_page__WEBPACK_IMPORTED_MODULE_3__["ChangethumimpressionPage"]
    }
];
var ChangethumimpressionPageRoutingModule = /** @class */ (function () {
    function ChangethumimpressionPageRoutingModule() {
    }
    ChangethumimpressionPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
        })
    ], ChangethumimpressionPageRoutingModule);
    return ChangethumimpressionPageRoutingModule;
}());



/***/ }),

/***/ "./src/app/changethumimpression/changethumimpression.module.ts":
/*!*********************************************************************!*\
  !*** ./src/app/changethumimpression/changethumimpression.module.ts ***!
  \*********************************************************************/
/*! exports provided: ChangethumimpressionPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChangethumimpressionPageModule", function() { return ChangethumimpressionPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _changethumimpression_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./changethumimpression-routing.module */ "./src/app/changethumimpression/changethumimpression-routing.module.ts");
/* harmony import */ var _changethumimpression_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./changethumimpression.page */ "./src/app/changethumimpression/changethumimpression.page.ts");







var ChangethumimpressionPageModule = /** @class */ (function () {
    function ChangethumimpressionPageModule() {
    }
    ChangethumimpressionPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _changethumimpression_routing_module__WEBPACK_IMPORTED_MODULE_5__["ChangethumimpressionPageRoutingModule"]
            ],
            declarations: [_changethumimpression_page__WEBPACK_IMPORTED_MODULE_6__["ChangethumimpressionPage"]]
        })
    ], ChangethumimpressionPageModule);
    return ChangethumimpressionPageModule;
}());



/***/ }),

/***/ "./src/app/changethumimpression/changethumimpression.page.scss":
/*!*********************************************************************!*\
  !*** ./src/app/changethumimpression/changethumimpression.page.scss ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NoYW5nZXRodW1pbXByZXNzaW9uL2NoYW5nZXRodW1pbXByZXNzaW9uLnBhZ2Uuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/changethumimpression/changethumimpression.page.ts":
/*!*******************************************************************!*\
  !*** ./src/app/changethumimpression/changethumimpression.page.ts ***!
  \*******************************************************************/
/*! exports provided: ChangethumimpressionPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChangethumimpressionPage", function() { return ChangethumimpressionPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var ChangethumimpressionPage = /** @class */ (function () {
    function ChangethumimpressionPage() {
    }
    ChangethumimpressionPage.prototype.ngOnInit = function () {
    };
    ChangethumimpressionPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-changethumimpression',
            template: __webpack_require__(/*! raw-loader!./changethumimpression.page.html */ "./node_modules/raw-loader/index.js!./src/app/changethumimpression/changethumimpression.page.html"),
            styles: [__webpack_require__(/*! ./changethumimpression.page.scss */ "./src/app/changethumimpression/changethumimpression.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], ChangethumimpressionPage);
    return ChangethumimpressionPage;
}());



/***/ })

}]);
//# sourceMappingURL=changethumimpression-changethumimpression-module-es5.js.map