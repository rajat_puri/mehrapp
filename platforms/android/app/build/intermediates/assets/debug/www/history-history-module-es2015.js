(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["history-history-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/history/history.page.html":
/*!*********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/history/history.page.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header  >\n  <ion-toolbar class=\"toolbar-blue\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <ion-title class=\"title-top\">\n     History\n    </ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\n \n \n<ion-grid>\n  <ion-row> \n  <ion-col size=\"12\">  \n\t<div class=\"transaction-head\">Past orders and Tool booked</div>\t\n </ion-col>\n  </ion-row>\n</ion-grid>\n\n\n  <ion-card class=\"ma10\">\n\n<ion-item lines=\"none\" class=\"bg-white\">\n  <ion-label class=\"\">\n  <div class=\"black-heading pt5\">Rice Seed 1024 No.</div>\n  <div class=\"small-gray pt5\">₹  320.00</div>\n\n  </ion-label>\n</ion-item>\n </ion-card>\n \n\n \n<ion-grid>\n  <ion-row> \n  <ion-col size=\"12\">  \n\t<div class=\"transaction-head\">Order Details</div>\t\n </ion-col>\n  </ion-row>\n</ion-grid>\n\n\n<!-- List of Text Items -->\n<ion-list>\n  <ion-item lines=\"none\" >\n    <ion-label text-wrap>\n\t<div class=\"transaction-head\">Seed 4</div>\t\n\t<div class=\"small-gray\">₹ 189.00</div>\n\t</ion-label>\n\t  <ion-badge  color=\"light\" slot=\"end\">  3 </ion-badge> \n\t  </ion-item>\n\t  \n\t    <ion-item lines=\"none\" >\n    <ion-label text-wrap>\n\t<div class=\"transaction-head\">Rice Seed 1024 No. </div>\t\n\t<div class=\"small-gray\">₹ 52.00</div>\n\t</ion-label>\n\t   <ion-badge  color=\"light\" slot=\"end\">  3 </ion-badge> \n\t  </ion-item>\n\t  \n\t    <ion-item lines=\"none\" >\n    <ion-label text-wrap>\n\t<div class=\"transaction-head\">UREA 152</div>\t\n\t<div class=\"small-gray\">₹ 420.00</div>\n\t</ion-label>\n <ion-badge  color=\"light\" slot=\"end\">  3 </ion-badge> \t  \n\t  </ion-item>\n\t  </ion-list>\n<ion-grid class=\"bg-white\">\n  <ion-row class=\"bg-white\"> \n  <ion-col size=\"8\">  \n\t<div class=\"small-gray\">Item Total</div>\t\n </ion-col>\n  <ion-col size=\"4\">  \n\t<div class=\"small-gray text-right-all\">₹ 1200</div>\t\n </ion-col>\n  </ion-row>\n  \n  <ion-row> \n  <ion-col size=\"8\">  \n\t<div class=\"small-gray\">Coupon Discount</div>\t\n </ion-col>\n  <ion-col size=\"4\">  \n\t<div class=\"small-gray text-right-all\">₹ 120</div>\t\n </ion-col>\n  </ion-row>\n  \n   <ion-row> \n  <ion-col size=\"8\">  \n\t<div class=\"small-gray\">TAX 3%</div>\t\n </ion-col>\n  <ion-col size=\"4\">  \n\t<div class=\"small-gray text-right-all\">₹ 68</div>\t\n </ion-col>\n  </ion-row>\n  <ion-row> \n  <ion-col size=\"8\">  \n\t<div class=\"small-gray\">To Pay </div>\t\n </ion-col>\n  <ion-col size=\"4\">  \n\t<div class=\"small-gray text-right-all\">₹ 1388</div>\t\n </ion-col>\n  </ion-row>\n</ion-grid>\n \n\n \n\n\n \n \n \n \n \n \n \n \n \n \n\n\n</ion-content>\n"

/***/ }),

/***/ "./src/app/history/history-routing.module.ts":
/*!***************************************************!*\
  !*** ./src/app/history/history-routing.module.ts ***!
  \***************************************************/
/*! exports provided: HistoryPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HistoryPageRoutingModule", function() { return HistoryPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _history_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./history.page */ "./src/app/history/history.page.ts");




const routes = [
    {
        path: '',
        component: _history_page__WEBPACK_IMPORTED_MODULE_3__["HistoryPage"]
    }
];
let HistoryPageRoutingModule = class HistoryPageRoutingModule {
};
HistoryPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], HistoryPageRoutingModule);



/***/ }),

/***/ "./src/app/history/history.module.ts":
/*!*******************************************!*\
  !*** ./src/app/history/history.module.ts ***!
  \*******************************************/
/*! exports provided: HistoryPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HistoryPageModule", function() { return HistoryPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _history_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./history-routing.module */ "./src/app/history/history-routing.module.ts");
/* harmony import */ var _history_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./history.page */ "./src/app/history/history.page.ts");







let HistoryPageModule = class HistoryPageModule {
};
HistoryPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _history_routing_module__WEBPACK_IMPORTED_MODULE_5__["HistoryPageRoutingModule"]
        ],
        declarations: [_history_page__WEBPACK_IMPORTED_MODULE_6__["HistoryPage"]]
    })
], HistoryPageModule);



/***/ }),

/***/ "./src/app/history/history.page.scss":
/*!*******************************************!*\
  !*** ./src/app/history/history.page.scss ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-content {\n  --ion-background-color:#f3f5f5;\n}\n\nion-item {\n  --ion-background-color:#fff;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaGlzdG9yeS9FOlxcbWVoYXItYmFuay1hcHAtbWFzdGVyXFxtZWhhci1iYW5rLWFwcC1tYXN0ZXIvc3JjXFxhcHBcXGhpc3RvcnlcXGhpc3RvcnkucGFnZS5zY3NzIiwic3JjL2FwcC9oaXN0b3J5L2hpc3RvcnkucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksOEJBQUE7QUNDSjs7QURFQTtFQUNHLDJCQUFBO0FDQ0giLCJmaWxlIjoic3JjL2FwcC9oaXN0b3J5L2hpc3RvcnkucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWNvbnRlbnR7XG4gICAgLS1pb24tYmFja2dyb3VuZC1jb2xvcjojZjNmNWY1O1xufVxuXG5pb24taXRlbXtcbiAgIC0taW9uLWJhY2tncm91bmQtY29sb3I6I2ZmZjtcbn0iLCJpb24tY29udGVudCB7XG4gIC0taW9uLWJhY2tncm91bmQtY29sb3I6I2YzZjVmNTtcbn1cblxuaW9uLWl0ZW0ge1xuICAtLWlvbi1iYWNrZ3JvdW5kLWNvbG9yOiNmZmY7XG59Il19 */"

/***/ }),

/***/ "./src/app/history/history.page.ts":
/*!*****************************************!*\
  !*** ./src/app/history/history.page.ts ***!
  \*****************************************/
/*! exports provided: HistoryPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HistoryPage", function() { return HistoryPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let HistoryPage = class HistoryPage {
    constructor() { }
    ngOnInit() {
    }
};
HistoryPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-history',
        template: __webpack_require__(/*! raw-loader!./history.page.html */ "./node_modules/raw-loader/index.js!./src/app/history/history.page.html"),
        styles: [__webpack_require__(/*! ./history.page.scss */ "./src/app/history/history.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], HistoryPage);



/***/ })

}]);
//# sourceMappingURL=history-history-module-es2015.js.map