(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["fundtransfersuccessfully-fundtransfersuccessfully-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/fundtransfersuccessfully/fundtransfersuccessfully.page.html":
/*!*******************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/fundtransfersuccessfully/fundtransfersuccessfully.page.html ***!
  \*******************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header  no-border >\n  <ion-toolbar class=\"toolbar-blue\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <ion-title class=\"title-top\">\n    Done\n    </ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\n  <div class=\"card-bg-200height\"></div>\n  \n\n<ion-card class=\"card-successfully\">\n\n\n<div class=\"successfully-green-round\"><ion-icon name=\"checkmark\"></ion-icon></div>\n \n    <ion-card-title class=\"successfully\">Booked Successfully !!</ion-card-title>\n \n\n\n<ion-list class=\"pt0\">\n  <ion-item lines=\"none\">\n    <ion-label>\n\t<div class=\"small-gray\">From Date and Time</div>\n\t<div class=\"black-heading\">12-12-2019 | 05:00PM</div>\n\n\t</ion-label>\n  </ion-item>\n  \n  <ion-item lines=\"none\">\n    <ion-label>\n\t<div class=\"small-gray\">End Date and Time</div>\n\t<div class=\"black-heading\">28-12-2019 | 06:00PM</div>\n\n\t</ion-label>\n  </ion-item>\n  \n    <ion-item lines=\"none\">\n    <ion-label>\n\t<div class=\"small-gray\">Village</div>\n\t<div class=\"black-heading\">Sansarpur</div>\n\n\t</ion-label>\n  </ion-item>\n  \n    <ion-item lines=\"none\">\n    <ion-label text-wrap>\n\t<div class=\"small-gray\">Address</div>\n\t<div class=\"black-heading\">Hno. 95 VPO Sansarpur Jalandhar Cantt Punjab India</div>\n\n\t</ion-label>\n  </ion-item>\n  \n</ion-list>\n  \n  \n\n</ion-card>\n\n\n\n\n\n</ion-content>\n<ion-footer no-border class=\"successfully-footer\">\n\n   <ion-grid class=\"successfully-footer\">\n  <ion-row class=\"successfully-footer\">\n    <ion-col class=\"successfully-footer\">\n        <button [routerLink]=\"['/aritoolbooking']\" class=\"btnlightblue\" ion-button block>Book More Tool</button>\n    </ion-col>\n    <ion-col class=\"successfully-footer\">\n   <button [routerLink]=\"['/home']\" class=\"btngreen\" ion-button block>Back to Home</button>\n    </ion-col>\n  \n   \n  </ion-row>\n</ion-grid>\n\n</ion-footer>"

/***/ }),

/***/ "./src/app/fundtransfersuccessfully/fundtransfersuccessfully-routing.module.ts":
/*!*************************************************************************************!*\
  !*** ./src/app/fundtransfersuccessfully/fundtransfersuccessfully-routing.module.ts ***!
  \*************************************************************************************/
/*! exports provided: FundtransfersuccessfullyPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FundtransfersuccessfullyPageRoutingModule", function() { return FundtransfersuccessfullyPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _fundtransfersuccessfully_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./fundtransfersuccessfully.page */ "./src/app/fundtransfersuccessfully/fundtransfersuccessfully.page.ts");




const routes = [
    {
        path: '',
        component: _fundtransfersuccessfully_page__WEBPACK_IMPORTED_MODULE_3__["FundtransfersuccessfullyPage"]
    }
];
let FundtransfersuccessfullyPageRoutingModule = class FundtransfersuccessfullyPageRoutingModule {
};
FundtransfersuccessfullyPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], FundtransfersuccessfullyPageRoutingModule);



/***/ }),

/***/ "./src/app/fundtransfersuccessfully/fundtransfersuccessfully.module.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/fundtransfersuccessfully/fundtransfersuccessfully.module.ts ***!
  \*****************************************************************************/
/*! exports provided: FundtransfersuccessfullyPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FundtransfersuccessfullyPageModule", function() { return FundtransfersuccessfullyPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _fundtransfersuccessfully_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./fundtransfersuccessfully-routing.module */ "./src/app/fundtransfersuccessfully/fundtransfersuccessfully-routing.module.ts");
/* harmony import */ var _fundtransfersuccessfully_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./fundtransfersuccessfully.page */ "./src/app/fundtransfersuccessfully/fundtransfersuccessfully.page.ts");







let FundtransfersuccessfullyPageModule = class FundtransfersuccessfullyPageModule {
};
FundtransfersuccessfullyPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _fundtransfersuccessfully_routing_module__WEBPACK_IMPORTED_MODULE_5__["FundtransfersuccessfullyPageRoutingModule"]
        ],
        declarations: [_fundtransfersuccessfully_page__WEBPACK_IMPORTED_MODULE_6__["FundtransfersuccessfullyPage"]]
    })
], FundtransfersuccessfullyPageModule);



/***/ }),

/***/ "./src/app/fundtransfersuccessfully/fundtransfersuccessfully.page.scss":
/*!*****************************************************************************!*\
  !*** ./src/app/fundtransfersuccessfully/fundtransfersuccessfully.page.scss ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-content {\n  --ion-background-color:#f3f5f5;\n}\n\nion-item {\n  --ion-background-color:#ffffff;\n}\n\nion-footer {\n  background-color: #f3f5f5 !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZnVuZHRyYW5zZmVyc3VjY2Vzc2Z1bGx5L0U6XFxtZWhhci1iYW5rLWFwcC1tYXN0ZXJcXG1laGFyLWJhbmstYXBwLW1hc3Rlci9zcmNcXGFwcFxcZnVuZHRyYW5zZmVyc3VjY2Vzc2Z1bGx5XFxmdW5kdHJhbnNmZXJzdWNjZXNzZnVsbHkucGFnZS5zY3NzIiwic3JjL2FwcC9mdW5kdHJhbnNmZXJzdWNjZXNzZnVsbHkvZnVuZHRyYW5zZmVyc3VjY2Vzc2Z1bGx5LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLDhCQUFBO0FDQ0o7O0FEQ0E7RUFDRyw4QkFBQTtBQ0VIOztBREFBO0VBRVEsb0NBQUE7QUNFUiIsImZpbGUiOiJzcmMvYXBwL2Z1bmR0cmFuc2ZlcnN1Y2Nlc3NmdWxseS9mdW5kdHJhbnNmZXJzdWNjZXNzZnVsbHkucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWNvbnRlbnR7XG4gICAgLS1pb24tYmFja2dyb3VuZC1jb2xvcjojZjNmNWY1O1xufVxuaW9uLWl0ZW17XG4gICAtLWlvbi1iYWNrZ3JvdW5kLWNvbG9yOiNmZmZmZmY7XG59XG5pb24tZm9vdGVye1xuICAgICAgIFxuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjNmNWY1ICFpbXBvcnRhbnQ7XG4gIFxufSIsImlvbi1jb250ZW50IHtcbiAgLS1pb24tYmFja2dyb3VuZC1jb2xvcjojZjNmNWY1O1xufVxuXG5pb24taXRlbSB7XG4gIC0taW9uLWJhY2tncm91bmQtY29sb3I6I2ZmZmZmZjtcbn1cblxuaW9uLWZvb3RlciB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmM2Y1ZjUgIWltcG9ydGFudDtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/fundtransfersuccessfully/fundtransfersuccessfully.page.ts":
/*!***************************************************************************!*\
  !*** ./src/app/fundtransfersuccessfully/fundtransfersuccessfully.page.ts ***!
  \***************************************************************************/
/*! exports provided: FundtransfersuccessfullyPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FundtransfersuccessfullyPage", function() { return FundtransfersuccessfullyPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let FundtransfersuccessfullyPage = class FundtransfersuccessfullyPage {
    constructor() { }
    ngOnInit() {
    }
};
FundtransfersuccessfullyPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-fundtransfersuccessfully',
        template: __webpack_require__(/*! raw-loader!./fundtransfersuccessfully.page.html */ "./node_modules/raw-loader/index.js!./src/app/fundtransfersuccessfully/fundtransfersuccessfully.page.html"),
        styles: [__webpack_require__(/*! ./fundtransfersuccessfully.page.scss */ "./src/app/fundtransfersuccessfully/fundtransfersuccessfully.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], FundtransfersuccessfullyPage);



/***/ })

}]);
//# sourceMappingURL=fundtransfersuccessfully-fundtransfersuccessfully-module-es2015.js.map