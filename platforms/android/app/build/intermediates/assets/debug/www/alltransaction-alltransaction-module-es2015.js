(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["alltransaction-alltransaction-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/alltransaction/alltransaction.page.html":
/*!***********************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/alltransaction/alltransaction.page.html ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n\t<ion-toolbar class=\"toolbar-blue\">\n\t\t<ion-buttons slot=\"start\">\n\t\t\t<ion-menu-button></ion-menu-button>\n\t\t</ion-buttons>\n\t\t<ion-title class=\"title-top\">\n\t\t\tStatement\n\t\t</ion-title>\n\t</ion-toolbar>\n</ion-header>\n\n<ion-datetime [(ngModel)]=\"startDate\" display-timezone=\"utc\"></ion-datetime>\n<ion-datetime [(ngModel)]=\"endDate\" display-timezone=\"utc\"></ion-datetime>\n<ion-button color=\"secondary\" (click)=\"fetchLedger()\">Get</ion-button>\n\n<ion-content>\n\t<div class=\"card-bg-full\">\n\n\n\t\t<ion-card class=\"card black-card-sm\">\n\t\t\t<ion-row class=\"row\">\n\t\t\t\t<ion-col class=\"col\" size=\"7\">\n\t\t\t\t\t<h3>Saving Account</h3>\n\t\t\t\t\t<h2>{{ledgerDetails.AccName}} {{ledgerDetails.accno}}</h2>\n\t\t\t\t</ion-col>\n\t\t\t\t<ion-col class=\"col\" size=\"5\" text-end>\n\t\t\t\t\t<h3>Available Balance</h3>\n\t\t\t\t\t<h2>₹ {{ledgerDetails.balance}}\n\t\t\t\t\t\t<!-- <sub>.00</sub> -->\n\t\t\t\t\t</h2>\n\t\t\t\t</ion-col>\n\t\t\t</ion-row>\n\t\t</ion-card>\n\n\t</div>\n\n\t<ion-grid class=\"successfully-footer ion-no-padding\">\n\t\t<ion-row class=\"successfully-footer\">\n\t\t\t<ion-col class=\"pa0\">\n\t\t\t\t<button class=\"filter\" ion-button block>\n\t\t\t\t\t<ion-icon name=\"clipboard\"></ion-icon> Download Statement</button>\n\t\t\t</ion-col>\n\n\t\t</ion-row>\n\t</ion-grid>\n\t<!-- List of Text Items -->\n\t<ion-list>\n\n\t\t<ion-item *ngFor=\"let ledgerDetail of ledgerDetails.ledgerdetail\">\n\t\t\t<ion-label>\n\t\t\t\t<div class=\"transaction-head\">{{ledgerDetail.particulars}}</div>\n\t\t\t\t<div class=\"transaction-date\">{{ledgerDetail.date}} </div>\n\t\t\t\t<div class=\"transaction-amt \" [ngClass]=\"ledgerDetail.entrytype=='Dr'?'red':'green'\">₹ {{ledgerDetail.Amount}}\n\t\t\t\t\t<sub>{{ledgerDetail.entrytype}}</sub>\n\t\t\t\t</div>\n\t\t\t</ion-label>\n\t\t</ion-item>\n\t\t<!-- <ion-item>\n\t\t\t<ion-label>\n\t\t\t\t<div class=\"transaction-head\">EMI Debited</div>\n\t\t\t\t<div class=\"transaction-date\">12 June, 2019 </div>\n\t\t\t\t<div class=\"transaction-amt red\">₹ 7676.00\n\t\t\t\t\t<sub>Dr</sub>\n\t\t\t\t</div>\n\t\t\t</ion-label>\n\t\t</ion-item>\n\t\t<ion-item>\n\t\t\t<ion-label>\n\t\t\t\t<div class=\"transaction-head\">EMI Debited</div>\n\t\t\t\t<div class=\"transaction-date\">12 June, 2019 </div>\n\t\t\t\t<div class=\"transaction-amt green\">₹ 22398.00\n\t\t\t\t\t<sub>Cr</sub>\n\t\t\t\t</div>\n\t\t\t</ion-label>\n\t\t</ion-item>\n\t\t<ion-item>\n\t\t\t<ion-label>\n\t\t\t\t<div class=\"transaction-head\">EMI Debited</div>\n\t\t\t\t<div class=\"transaction-date\">12 June, 2019 </div>\n\t\t\t\t<div class=\"transaction-amt red\">₹ 78764.00\n\t\t\t\t\t<sub>Dr</sub>\n\t\t\t\t</div>\n\t\t\t</ion-label>\n\t\t</ion-item>\n\t\t<ion-item>\n\t\t\t<ion-label>\n\t\t\t\t<div class=\"transaction-head\">EMI Debited</div>\n\t\t\t\t<div class=\"transaction-date\">12 June, 2019 </div>\n\t\t\t\t<div class=\"transaction-amt green\">₹ 455.00\n\t\t\t\t\t<sub>Cr</sub>\n\t\t\t\t</div>\n\t\t\t</ion-label>\n\t\t</ion-item>\n\t\t<ion-item>\n\t\t\t<ion-label>\n\t\t\t\t<div class=\"transaction-head\">EMI Debited</div>\n\t\t\t\t<div class=\"transaction-date\">12 June, 2019 </div>\n\t\t\t\t<div class=\"transaction-amt green\">₹ 4577.00\n\t\t\t\t\t<sub>Cr</sub>\n\t\t\t\t</div>\n\t\t\t</ion-label>\n\t\t</ion-item>\n\t\t<ion-item>\n\t\t\t<ion-label>\n\t\t\t\t<div class=\"transaction-head\">EMI Debited</div>\n\t\t\t\t<div class=\"transaction-date\">12 June, 2019 </div>\n\t\t\t\t<div class=\"transaction-amt red\">₹ 108100.00\n\t\t\t\t\t<sub>Dr</sub>\n\t\t\t\t</div>\n\t\t\t</ion-label>\n\t\t</ion-item>\n\t\t<ion-item>\n\t\t\t<ion-label>\n\t\t\t\t<div class=\"transaction-head\">EMI Debited</div>\n\t\t\t\t<div class=\"transaction-date\">12 June, 2019 </div>\n\t\t\t\t<div class=\"transaction-amt green\">₹ 1000.00\n\t\t\t\t\t<sub>Cr</sub>\n\t\t\t\t</div>\n\t\t\t</ion-label>\n\t\t</ion-item> -->\n\n\t</ion-list>\n\n</ion-content>"

/***/ }),

/***/ "./src/app/alltransaction/alltransaction-routing.module.ts":
/*!*****************************************************************!*\
  !*** ./src/app/alltransaction/alltransaction-routing.module.ts ***!
  \*****************************************************************/
/*! exports provided: AlltransactionPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AlltransactionPageRoutingModule", function() { return AlltransactionPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _alltransaction_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./alltransaction.page */ "./src/app/alltransaction/alltransaction.page.ts");




const routes = [
    {
        path: '',
        component: _alltransaction_page__WEBPACK_IMPORTED_MODULE_3__["AlltransactionPage"]
    }
];
let AlltransactionPageRoutingModule = class AlltransactionPageRoutingModule {
};
AlltransactionPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], AlltransactionPageRoutingModule);



/***/ }),

/***/ "./src/app/alltransaction/alltransaction.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/alltransaction/alltransaction.module.ts ***!
  \*********************************************************/
/*! exports provided: AlltransactionPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AlltransactionPageModule", function() { return AlltransactionPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _alltransaction_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./alltransaction-routing.module */ "./src/app/alltransaction/alltransaction-routing.module.ts");
/* harmony import */ var _alltransaction_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./alltransaction.page */ "./src/app/alltransaction/alltransaction.page.ts");







let AlltransactionPageModule = class AlltransactionPageModule {
};
AlltransactionPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _alltransaction_routing_module__WEBPACK_IMPORTED_MODULE_5__["AlltransactionPageRoutingModule"]
        ],
        declarations: [_alltransaction_page__WEBPACK_IMPORTED_MODULE_6__["AlltransactionPage"]]
    })
], AlltransactionPageModule);



/***/ }),

/***/ "./src/app/alltransaction/alltransaction.page.scss":
/*!*********************************************************!*\
  !*** ./src/app/alltransaction/alltransaction.page.scss ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".black-card-sm {\n  background: url('item_bg.png');\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYWxsdHJhbnNhY3Rpb24vRTpcXG1laGFyLWJhbmstYXBwLW1hc3RlclxcbWVoYXItYmFuay1hcHAtbWFzdGVyL3NyY1xcYXBwXFxhbGx0cmFuc2FjdGlvblxcYWxsdHJhbnNhY3Rpb24ucGFnZS5zY3NzIiwic3JjL2FwcC9hbGx0cmFuc2FjdGlvbi9hbGx0cmFuc2FjdGlvbi5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSw4QkFBQTtBQ0NKIiwiZmlsZSI6InNyYy9hcHAvYWxsdHJhbnNhY3Rpb24vYWxsdHJhbnNhY3Rpb24ucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmJsYWNrLWNhcmQtc20ge1xuICAgIGJhY2tncm91bmQ6IHVybCguLi8uLi9hc3NldHMvaW1ncy9pdGVtX2JnLnBuZyk7XG5cdH0iLCIuYmxhY2stY2FyZC1zbSB7XG4gIGJhY2tncm91bmQ6IHVybCguLi8uLi9hc3NldHMvaW1ncy9pdGVtX2JnLnBuZyk7XG59Il19 */"

/***/ }),

/***/ "./src/app/alltransaction/alltransaction.page.ts":
/*!*******************************************************!*\
  !*** ./src/app/alltransaction/alltransaction.page.ts ***!
  \*******************************************************/
/*! exports provided: AlltransactionPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AlltransactionPage", function() { return AlltransactionPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _services_user_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/user.service */ "./src/services/user.service.ts");
/* harmony import */ var _services_cookie_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/cookie.service */ "./src/services/cookie.service.ts");





let AlltransactionPage = class AlltransactionPage {
    constructor(alertController, loadingController, userService, cookieService) {
        this.alertController = alertController;
        this.loadingController = loadingController;
        this.userService = userService;
        this.cookieService = cookieService;
        this.startDate = '01/04/2016';
        this.endDate = '01/04/2020';
        this.ledgerDetails = [];
    }
    ngOnInit() {
    }
    presentAlert() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                header: 'Error',
                subHeader: 'wrong values passed',
                message: 'Please select correct values',
                buttons: ['OK']
            });
            yield alert.present();
        });
    }
    fetchLedger() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            var dataToSend = {
                startDate: this.startDate,
                endDate: this.endDate
            };
            const loading = yield this.loadingController.create({
                message: 'loading...',
            });
            yield loading.present();
            this.userService.fetchLedger(dataToSend).subscribe((res) => {
                loading.dismiss();
                var details = JSON.parse(res);
                this.ledgerDetails = details[0];
                console.log(this.ledgerDetails);
            }, (err) => {
                console.log(err);
                loading.dismiss();
                this.presentAlert();
            });
        });
    }
};
AlltransactionPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] },
    { type: _services_user_service__WEBPACK_IMPORTED_MODULE_3__["UserService"] },
    { type: _services_cookie_service__WEBPACK_IMPORTED_MODULE_4__["CookieService"] }
];
AlltransactionPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-alltransaction',
        template: __webpack_require__(/*! raw-loader!./alltransaction.page.html */ "./node_modules/raw-loader/index.js!./src/app/alltransaction/alltransaction.page.html"),
        providers: [_services_user_service__WEBPACK_IMPORTED_MODULE_3__["UserService"], _services_cookie_service__WEBPACK_IMPORTED_MODULE_4__["CookieService"]],
        styles: [__webpack_require__(/*! ./alltransaction.page.scss */ "./src/app/alltransaction/alltransaction.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"], _services_user_service__WEBPACK_IMPORTED_MODULE_3__["UserService"], _services_cookie_service__WEBPACK_IMPORTED_MODULE_4__["CookieService"]])
], AlltransactionPage);



/***/ }),

/***/ "./src/services/cookie.service.ts":
/*!****************************************!*\
  !*** ./src/services/cookie.service.ts ***!
  \****************************************/
/*! exports provided: CookieService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CookieService", function() { return CookieService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");



let CookieService = class CookieService {
    constructor(router) {
        this.router = router;
    }
    setItem(sKey, sValue) {
        // sDomain = environment.domain;
        localStorage.setItem(sKey, sValue);
        return true;
    }
    getItem(sKey) {
        if (!sKey) {
            return null;
        }
        return localStorage.getItem(sKey);
    }
    removeSingleItem(id) {
        localStorage.removeItem(id);
    }
    removeCookies() {
        localStorage.clear();
    }
};
CookieService.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
];
CookieService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
], CookieService);



/***/ })

}]);
//# sourceMappingURL=alltransaction-alltransaction-module-es2015.js.map