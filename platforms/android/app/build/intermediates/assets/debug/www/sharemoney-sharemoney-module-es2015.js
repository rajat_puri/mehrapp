(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["sharemoney-sharemoney-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/sharemoney/sharemoney.page.html":
/*!***************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/sharemoney/sharemoney.page.html ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar class=\"toolbar-blue\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <ion-title class=\"title-top\">\n      Statement\n    </ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-datetime [(ngModel)]=\"startDate\" display-timezone=\"utc\"></ion-datetime>\n<ion-datetime [(ngModel)]=\"endDate\" display-timezone=\"utc\"></ion-datetime>\n<ion-button color=\"secondary\" (click)=\"fetchShareMoney()\">Get</ion-button>\n\n<ion-content>\n  <div class=\"card-bg-full\">\n\n\n    <ion-card class=\"card black-card-sm\">\n      <ion-row class=\"row\">\n        <ion-col class=\"col\" size=\"7\">\n          <h3>Saving Account</h3>\n          <h2> {{ledgerDetails.accno}}</h2>\n        </ion-col>\n        <ion-col class=\"col\" size=\"5\" text-end>\n          <h3>Available Balance</h3>\n          <h2>₹ {{ledgerDetails.balance}}\n            <!-- <sub>.00</sub> -->\n          </h2>\n        </ion-col>\n      </ion-row>\n    </ion-card>\n\n  </div>\n\n  <ion-grid class=\"successfully-footer ion-no-padding\">\n    <ion-row class=\"successfully-footer\">\n      <ion-col class=\"pa0\">\n        <button class=\"filter\" ion-button block>\n          <ion-icon name=\"clipboard\"></ion-icon> Download Statement</button>\n      </ion-col>\n\n    </ion-row>\n  </ion-grid>\n  <!-- List of Text Items -->\n  <ion-list>\n\n    <ion-item *ngFor=\"let ledgerDetail of ledgerDetails.ledgerdetail\">\n      <ion-label>\n        <div class=\"transaction-head\">{{ledgerDetail.particulars}}</div>\n        <div class=\"transaction-date\">{{ledgerDetail.date}} </div>\n        <div class=\"transaction-amt \" [ngClass]=\"ledgerDetail.entrytype=='Dr'?'red':'green'\">₹ {{ledgerDetail.Amount}}\n          <sub>{{ledgerDetail.entrytype}}</sub>\n        </div>\n      </ion-label>\n    </ion-item>\n    <!-- <ion-item>\n\t\t\t<ion-label>\n\t\t\t\t<div class=\"transaction-head\">EMI Debited</div>\n\t\t\t\t<div class=\"transaction-date\">12 June, 2019 </div>\n\t\t\t\t<div class=\"transaction-amt red\">₹ 7676.00\n\t\t\t\t\t<sub>Dr</sub>\n\t\t\t\t</div>\n\t\t\t</ion-label>\n\t\t</ion-item>\n\t\t<ion-item>\n\t\t\t<ion-label>\n\t\t\t\t<div class=\"transaction-head\">EMI Debited</div>\n\t\t\t\t<div class=\"transaction-date\">12 June, 2019 </div>\n\t\t\t\t<div class=\"transaction-amt green\">₹ 22398.00\n\t\t\t\t\t<sub>Cr</sub>\n\t\t\t\t</div>\n\t\t\t</ion-label>\n\t\t</ion-item>\n\t\t<ion-item>\n\t\t\t<ion-label>\n\t\t\t\t<div class=\"transaction-head\">EMI Debited</div>\n\t\t\t\t<div class=\"transaction-date\">12 June, 2019 </div>\n\t\t\t\t<div class=\"transaction-amt red\">₹ 78764.00\n\t\t\t\t\t<sub>Dr</sub>\n\t\t\t\t</div>\n\t\t\t</ion-label>\n\t\t</ion-item>\n\t\t<ion-item>\n\t\t\t<ion-label>\n\t\t\t\t<div class=\"transaction-head\">EMI Debited</div>\n\t\t\t\t<div class=\"transaction-date\">12 June, 2019 </div>\n\t\t\t\t<div class=\"transaction-amt green\">₹ 455.00\n\t\t\t\t\t<sub>Cr</sub>\n\t\t\t\t</div>\n\t\t\t</ion-label>\n\t\t</ion-item>\n\t\t<ion-item>\n\t\t\t<ion-label>\n\t\t\t\t<div class=\"transaction-head\">EMI Debited</div>\n\t\t\t\t<div class=\"transaction-date\">12 June, 2019 </div>\n\t\t\t\t<div class=\"transaction-amt green\">₹ 4577.00\n\t\t\t\t\t<sub>Cr</sub>\n\t\t\t\t</div>\n\t\t\t</ion-label>\n\t\t</ion-item>\n\t\t<ion-item>\n\t\t\t<ion-label>\n\t\t\t\t<div class=\"transaction-head\">EMI Debited</div>\n\t\t\t\t<div class=\"transaction-date\">12 June, 2019 </div>\n\t\t\t\t<div class=\"transaction-amt red\">₹ 108100.00\n\t\t\t\t\t<sub>Dr</sub>\n\t\t\t\t</div>\n\t\t\t</ion-label>\n\t\t</ion-item>\n\t\t<ion-item>\n\t\t\t<ion-label>\n\t\t\t\t<div class=\"transaction-head\">EMI Debited</div>\n\t\t\t\t<div class=\"transaction-date\">12 June, 2019 </div>\n\t\t\t\t<div class=\"transaction-amt green\">₹ 1000.00\n\t\t\t\t\t<sub>Cr</sub>\n\t\t\t\t</div>\n\t\t\t</ion-label>\n\t\t</ion-item> -->\n\n  </ion-list>\n\n</ion-content>"

/***/ }),

/***/ "./src/app/sharemoney/sharemoney-routing.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/sharemoney/sharemoney-routing.module.ts ***!
  \*********************************************************/
/*! exports provided: SharemoneyPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SharemoneyPageRoutingModule", function() { return SharemoneyPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _sharemoney_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./sharemoney.page */ "./src/app/sharemoney/sharemoney.page.ts");




const routes = [
    {
        path: '',
        component: _sharemoney_page__WEBPACK_IMPORTED_MODULE_3__["sharemoneyPage"]
    }
];
let SharemoneyPageRoutingModule = class SharemoneyPageRoutingModule {
};
SharemoneyPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], SharemoneyPageRoutingModule);



/***/ }),

/***/ "./src/app/sharemoney/sharemoney.module.ts":
/*!*************************************************!*\
  !*** ./src/app/sharemoney/sharemoney.module.ts ***!
  \*************************************************/
/*! exports provided: SharemoneyPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SharemoneyPageModule", function() { return SharemoneyPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _sharemoney_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./sharemoney-routing.module */ "./src/app/sharemoney/sharemoney-routing.module.ts");
/* harmony import */ var _sharemoney_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./sharemoney.page */ "./src/app/sharemoney/sharemoney.page.ts");







let SharemoneyPageModule = class SharemoneyPageModule {
};
SharemoneyPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _sharemoney_routing_module__WEBPACK_IMPORTED_MODULE_5__["SharemoneyPageRoutingModule"]
        ],
        declarations: [_sharemoney_page__WEBPACK_IMPORTED_MODULE_6__["sharemoneyPage"]]
    })
], SharemoneyPageModule);



/***/ }),

/***/ "./src/app/sharemoney/sharemoney.page.scss":
/*!*************************************************!*\
  !*** ./src/app/sharemoney/sharemoney.page.scss ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlbW9uZXkvc2hhcmVtb25leS5wYWdlLnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/sharemoney/sharemoney.page.ts":
/*!***********************************************!*\
  !*** ./src/app/sharemoney/sharemoney.page.ts ***!
  \***********************************************/
/*! exports provided: sharemoneyPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "sharemoneyPage", function() { return sharemoneyPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _services_user_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/user.service */ "./src/services/user.service.ts");
/* harmony import */ var _services_cookie_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/cookie.service */ "./src/services/cookie.service.ts");





let sharemoneyPage = class sharemoneyPage {
    constructor(alertController, loadingController, userService, cookieService) {
        this.alertController = alertController;
        this.loadingController = loadingController;
        this.userService = userService;
        this.cookieService = cookieService;
        this.startDate = '01/04/2016';
        this.endDate = '01/04/2020';
        this.ledgerDetails = [];
        this.allProducts = [];
        this.productSelected = "";
    }
    ngOnInit() {
    }
    presentAlert(type, subHeader, message) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                header: type,
                subHeader: subHeader,
                message: message,
                buttons: ['OK']
            });
            yield alert.present();
        });
    }
    fetchShareMoney() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            this.ledgerDetails = [];
            var dataToSend = {
                startDate: this.startDate,
                endDate: this.endDate,
                accTye: '4',
            };
            const loading = yield this.loadingController.create({
                message: 'loading...',
            });
            yield loading.present();
            this.userService.fetchShareMoney(dataToSend).subscribe((res) => {
                loading.dismiss();
                if (res != '') {
                    var details = JSON.parse(res);
                    this.ledgerDetails = details[0];
                    console.log(this.ledgerDetails);
                }
            }, (err) => {
                console.log(err);
                loading.dismiss();
                this.presentAlert("Error", "", "Please Select Correct Dates");
            });
        });
    }
};
sharemoneyPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] },
    { type: _services_user_service__WEBPACK_IMPORTED_MODULE_3__["UserService"] },
    { type: _services_cookie_service__WEBPACK_IMPORTED_MODULE_4__["CookieService"] }
];
sharemoneyPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-sharemoney',
        template: __webpack_require__(/*! raw-loader!./sharemoney.page.html */ "./node_modules/raw-loader/index.js!./src/app/sharemoney/sharemoney.page.html"),
        providers: [_services_user_service__WEBPACK_IMPORTED_MODULE_3__["UserService"], _services_cookie_service__WEBPACK_IMPORTED_MODULE_4__["CookieService"]],
        styles: [__webpack_require__(/*! ./sharemoney.page.scss */ "./src/app/sharemoney/sharemoney.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"], _services_user_service__WEBPACK_IMPORTED_MODULE_3__["UserService"], _services_cookie_service__WEBPACK_IMPORTED_MODULE_4__["CookieService"]])
], sharemoneyPage);



/***/ }),

/***/ "./src/services/cookie.service.ts":
/*!****************************************!*\
  !*** ./src/services/cookie.service.ts ***!
  \****************************************/
/*! exports provided: CookieService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CookieService", function() { return CookieService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");



let CookieService = class CookieService {
    constructor(router) {
        this.router = router;
    }
    setItem(sKey, sValue) {
        // sDomain = environment.domain;
        localStorage.setItem(sKey, sValue);
        return true;
    }
    getItem(sKey) {
        if (!sKey) {
            return null;
        }
        return localStorage.getItem(sKey);
    }
    removeSingleItem(id) {
        localStorage.removeItem(id);
    }
    removeCookies() {
        localStorage.clear();
    }
};
CookieService.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
];
CookieService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
], CookieService);



/***/ })

}]);
//# sourceMappingURL=sharemoney-sharemoney-module-es2015.js.map