(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["aritoolbooking-aritoolbooking-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/aritoolbooking/aritoolbooking.page.html":
/*!***********************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/aritoolbooking/aritoolbooking.page.html ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header  >\n  <ion-toolbar class=\"toolbar-blue\">\n    <ion-buttons slot=\"secondary\">\n        <ion-button  [routerLink]=\"['/search']\" >\n      <ion-icon class=\"white\" slot=\"icon-only\" name=\"search\"></ion-icon>\n    </ion-button>\n  </ion-buttons>\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <ion-title class=\"title-top\">\n     Tool Booking\n    </ion-title>\n  </ion-toolbar>\n</ion-header>\n<ion-content>\n   <ion-grid  class=\"successfully-footer ion-no-padding\">\n  <ion-row class=\"successfully-footer\">\n    <ion-col class=\"pa0\">\n       <button [routerLink]=\"['/filter']\" class=\"filter\" ion-button block>  <ion-icon name=\"clipboard\"></ion-icon> Sort</button>\n    </ion-col>\n\t<ion-col class=\"pa0\">\n        <button [routerLink]=\"['/filter']\" class=\"filter\" ion-button block><ion-icon name=\"funnel\"></ion-icon> Filter</button>\n    </ion-col>\n  </ion-row>\n</ion-grid>\n\n\n\n\n\n <ion-row class=\"mt10\">\n    <ion-col  [routerLink]=\"['/agritooldetail']\" size=\"6\">\n\t  <ion-card class=\"pa0 ma0 bg-white pa10 ion-text-center\" >\n<ion-row>\n    <ion-col size=\"12\"> <img src=\"assets/imgs/product1.png\">  </ion-col>\n  </ion-row>\n  <div class=\"black-heading mb5\">Bakhsish Combines</div>\n  <div class=\"small-gray mb0  mt5\">Available From :<br /> 12-12-2019 | 05:00pm</div>\n  <div class=\"small-gray mb0 mt5\">Available To :<br /> 28-12-2019 | 06:00pm</div>\n  <div class=\"mt10\"><div class=\"green-btn pt0\">View Details</div>  </div>  \n </ion-card>\n\t \n    </ion-col>\n\t\n\t\n\t\n\t  <ion-col  [routerLink]=\"['/agritooldetail']\" size=\"6\">\n\t  <ion-card class=\"pa0 ma0 bg-white pa10 ion-text-center\" >\n<ion-row>\n    <ion-col size=\"12\"> <img src=\"assets/imgs/product1.png\">  </ion-col>\n  </ion-row>\n  <div class=\"black-heading mb5\">Bakhsish Combines</div>\n  <div class=\"small-gray mb0  mt5\">Available From :<br /> 12-12-2019 | 05:00pm</div>\n  <div class=\"small-gray mb0 mt5\">Available To :<br /> 28-12-2019 | 06:00pm</div>\n  <div class=\"mt10\"><div class=\"green-btn pt0\">View Details</div>  </div>  \n </ion-card>\n\t \n    </ion-col>\n\t\n\t  <ion-col  [routerLink]=\"['/agritooldetail']\" size=\"6\">\n\t  <ion-card class=\"pa0 ma0 bg-white pa10 ion-text-center\" >\n<ion-row>\n    <ion-col size=\"12\"> <img src=\"assets/imgs/product1.png\">  </ion-col>\n  </ion-row>\n  <div class=\"black-heading mb5\">Bakhsish Combines</div>\n  <div class=\"small-gray mb0  mt5\">Available From :<br /> 12-12-2019 | 05:00pm</div>\n  <div class=\"small-gray mb0 mt5\">Available To :<br /> 28-12-2019 | 06:00pm</div>\n  <div class=\"mt10\"><div class=\"green-btn pt0\">View Details</div>  </div>  \n </ion-card>\n\t \n    </ion-col>\n\t\n\t  <ion-col  [routerLink]=\"['/agritooldetail']\" size=\"6\">\n\t  <ion-card class=\"pa0 ma0 bg-white pa10 ion-text-center\" >\n<ion-row>\n    <ion-col size=\"12\"> <img src=\"assets/imgs/product1.png\">  </ion-col>\n  </ion-row>\n  <div class=\"black-heading mb5\">Bakhsish Combines</div>\n  <div class=\"small-gray mb0  mt5\">Available From :<br /> 12-12-2019 | 05:00pm</div>\n  <div class=\"small-gray mb0 mt5\">Available To :<br /> 28-12-2019 | 06:00pm</div>\n  <div class=\"mt10\"><div class=\"green-btn pt0\">View Details</div>  </div>  \n </ion-card>\n\t \n    </ion-col>\n\t\n\t  <ion-col  [routerLink]=\"['/agritooldetail']\" size=\"6\">\n\t  <ion-card class=\"pa0 ma0 bg-white pa10 ion-text-center\" >\n<ion-row>\n    <ion-col size=\"12\"> <img src=\"assets/imgs/product1.png\">  </ion-col>\n  </ion-row>\n  <div class=\"black-heading mb5\">Bakhsish Combines</div>\n  <div class=\"small-gray mb0  mt5\">Available From :<br /> 12-12-2019 | 05:00pm</div>\n  <div class=\"small-gray mb0 mt5\">Available To :<br /> 28-12-2019 | 06:00pm</div>\n  <div class=\"mt10\"><div class=\"green-btn pt0\">View Details</div>  </div>  \n </ion-card>\n\t \n    </ion-col>\n\t\n\t\t \n  \n  </ion-row>\n\n \n\n\n\n</ion-content>\n"

/***/ }),

/***/ "./src/app/aritoolbooking/aritoolbooking-routing.module.ts":
/*!*****************************************************************!*\
  !*** ./src/app/aritoolbooking/aritoolbooking-routing.module.ts ***!
  \*****************************************************************/
/*! exports provided: AritoolbookingPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AritoolbookingPageRoutingModule", function() { return AritoolbookingPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _aritoolbooking_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./aritoolbooking.page */ "./src/app/aritoolbooking/aritoolbooking.page.ts");




var routes = [
    {
        path: '',
        component: _aritoolbooking_page__WEBPACK_IMPORTED_MODULE_3__["AritoolbookingPage"]
    }
];
var AritoolbookingPageRoutingModule = /** @class */ (function () {
    function AritoolbookingPageRoutingModule() {
    }
    AritoolbookingPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
        })
    ], AritoolbookingPageRoutingModule);
    return AritoolbookingPageRoutingModule;
}());



/***/ }),

/***/ "./src/app/aritoolbooking/aritoolbooking.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/aritoolbooking/aritoolbooking.module.ts ***!
  \*********************************************************/
/*! exports provided: AritoolbookingPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AritoolbookingPageModule", function() { return AritoolbookingPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _aritoolbooking_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./aritoolbooking-routing.module */ "./src/app/aritoolbooking/aritoolbooking-routing.module.ts");
/* harmony import */ var _aritoolbooking_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./aritoolbooking.page */ "./src/app/aritoolbooking/aritoolbooking.page.ts");







var AritoolbookingPageModule = /** @class */ (function () {
    function AritoolbookingPageModule() {
    }
    AritoolbookingPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _aritoolbooking_routing_module__WEBPACK_IMPORTED_MODULE_5__["AritoolbookingPageRoutingModule"]
            ],
            declarations: [_aritoolbooking_page__WEBPACK_IMPORTED_MODULE_6__["AritoolbookingPage"]]
        })
    ], AritoolbookingPageModule);
    return AritoolbookingPageModule;
}());



/***/ }),

/***/ "./src/app/aritoolbooking/aritoolbooking.page.scss":
/*!*********************************************************!*\
  !*** ./src/app/aritoolbooking/aritoolbooking.page.scss ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-content {\n  --ion-background-color:#f3f5f5;\n}\n\nion-item {\n  --ion-background-color:#fff;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYXJpdG9vbGJvb2tpbmcvRTpcXG1laGFyLWJhbmstYXBwLW1hc3RlclxcbWVoYXItYmFuay1hcHAtbWFzdGVyL3NyY1xcYXBwXFxhcml0b29sYm9va2luZ1xcYXJpdG9vbGJvb2tpbmcucGFnZS5zY3NzIiwic3JjL2FwcC9hcml0b29sYm9va2luZy9hcml0b29sYm9va2luZy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSw4QkFBQTtBQ0NKOztBREVBO0VBQ0csMkJBQUE7QUNDSCIsImZpbGUiOiJzcmMvYXBwL2FyaXRvb2xib29raW5nL2FyaXRvb2xib29raW5nLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1jb250ZW50e1xuICAgIC0taW9uLWJhY2tncm91bmQtY29sb3I6I2YzZjVmNTtcbn1cblxuaW9uLWl0ZW17XG4gICAtLWlvbi1iYWNrZ3JvdW5kLWNvbG9yOiNmZmY7XG59IiwiaW9uLWNvbnRlbnQge1xuICAtLWlvbi1iYWNrZ3JvdW5kLWNvbG9yOiNmM2Y1ZjU7XG59XG5cbmlvbi1pdGVtIHtcbiAgLS1pb24tYmFja2dyb3VuZC1jb2xvcjojZmZmO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/aritoolbooking/aritoolbooking.page.ts":
/*!*******************************************************!*\
  !*** ./src/app/aritoolbooking/aritoolbooking.page.ts ***!
  \*******************************************************/
/*! exports provided: AritoolbookingPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AritoolbookingPage", function() { return AritoolbookingPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var AritoolbookingPage = /** @class */ (function () {
    function AritoolbookingPage() {
    }
    AritoolbookingPage.prototype.ngOnInit = function () {
    };
    AritoolbookingPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-aritoolbooking',
            template: __webpack_require__(/*! raw-loader!./aritoolbooking.page.html */ "./node_modules/raw-loader/index.js!./src/app/aritoolbooking/aritoolbooking.page.html"),
            styles: [__webpack_require__(/*! ./aritoolbooking.page.scss */ "./src/app/aritoolbooking/aritoolbooking.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], AritoolbookingPage);
    return AritoolbookingPage;
}());



/***/ })

}]);
//# sourceMappingURL=aritoolbooking-aritoolbooking-module-es5.js.map