(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~changepassword-changepassword-module~login-login-module~logout-logout-module"],{

/***/ "./node_modules/@ionic-native/android-fingerprint-auth/ngx/index.js":
/*!**************************************************************************!*\
  !*** ./node_modules/@ionic-native/android-fingerprint-auth/ngx/index.js ***!
  \**************************************************************************/
/*! exports provided: AndroidFingerprintAuth */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AndroidFingerprintAuth", function() { return AndroidFingerprintAuth; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_native_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic-native/core */ "./node_modules/@ionic-native/core/index.js");



var AndroidFingerprintAuth = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](AndroidFingerprintAuth, _super);
    function AndroidFingerprintAuth() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        /**
         * Convenience property containing all possible errors
         */
        _this.ERRORS = {
            BAD_PADDING_EXCEPTION: 'BAD_PADDING_EXCEPTION',
            CERTIFICATE_EXCEPTION: 'CERTIFICATE_EXCEPTION',
            FINGERPRINT_CANCELLED: 'FINGERPRINT_CANCELLED',
            FINGERPRINT_DATA_NOT_DELETED: 'FINGERPRINT_DATA_NOT_DELETED',
            FINGERPRINT_ERROR: 'FINGERPRINT_ERROR',
            FINGERPRINT_NOT_AVAILABLE: 'FINGERPRINT_NOT_AVAILABLE',
            FINGERPRINT_PERMISSION_DENIED: 'FINGERPRINT_PERMISSION_DENIED',
            FINGERPRINT_PERMISSION_DENIED_SHOW_REQUEST: 'FINGERPRINT_PERMISSION_DENIED_SHOW_REQUEST',
            ILLEGAL_BLOCK_SIZE_EXCEPTION: 'ILLEGAL_BLOCK_SIZE_EXCEPTION',
            INIT_CIPHER_FAILED: 'INIT_CIPHER_FAILED',
            INVALID_ALGORITHM_PARAMETER_EXCEPTION: 'INVALID_ALGORITHM_PARAMETER_EXCEPTION',
            IO_EXCEPTION: 'IO_EXCEPTION',
            JSON_EXCEPTION: 'JSON_EXCEPTION',
            MINIMUM_SDK: 'MINIMUM_SDK',
            MISSING_ACTION_PARAMETERS: 'MISSING_ACTION_PARAMETERS',
            MISSING_PARAMETERS: 'MISSING_PARAMETERS',
            NO_SUCH_ALGORITHM_EXCEPTION: 'NO_SUCH_ALGORITHM_EXCEPTION',
            SECURITY_EXCEPTION: 'SECURITY_EXCEPTION'
        };
        return _this;
    }
    AndroidFingerprintAuth.prototype.encrypt = function (options) { return Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_2__["cordova"])(this, "encrypt", {}, arguments); };
    AndroidFingerprintAuth.prototype.decrypt = function (options) { return Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_2__["cordova"])(this, "decrypt", {}, arguments); };
    AndroidFingerprintAuth.prototype.isAvailable = function () { return Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_2__["cordova"])(this, "isAvailable", {}, arguments); };
    AndroidFingerprintAuth.prototype.delete = function (options) { return Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_2__["cordova"])(this, "delete", {}, arguments); };
    AndroidFingerprintAuth.pluginName = "AndroidFingerprintAuth";
    AndroidFingerprintAuth.plugin = "cordova-plugin-android-fingerprint-auth";
    AndroidFingerprintAuth.pluginRef = "FingerprintAuth";
    AndroidFingerprintAuth.repo = "https://github.com/mjwheatley/cordova-plugin-android-fingerprint-auth";
    AndroidFingerprintAuth.platforms = ["Android"];
    AndroidFingerprintAuth = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()
    ], AndroidFingerprintAuth);
    return AndroidFingerprintAuth;
}(_ionic_native_core__WEBPACK_IMPORTED_MODULE_2__["IonicNativePlugin"]));

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi8uLi9zcmMvQGlvbmljLW5hdGl2ZS9wbHVnaW5zL2FuZHJvaWQtZmluZ2VycHJpbnQtYXV0aC9uZ3gvaW5kZXgudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyw4QkFBc0MsTUFBTSxvQkFBb0IsQ0FBQzs7SUErSjVCLGtEQUFpQjs7O1FBQzNEOztXQUVHO1FBQ0gsWUFBTSxHQW1CRjtZQUNGLHFCQUFxQixFQUFFLHVCQUF1QjtZQUM5QyxxQkFBcUIsRUFBRSx1QkFBdUI7WUFDOUMscUJBQXFCLEVBQUUsdUJBQXVCO1lBQzlDLDRCQUE0QixFQUFFLDhCQUE4QjtZQUM1RCxpQkFBaUIsRUFBRSxtQkFBbUI7WUFDdEMseUJBQXlCLEVBQUUsMkJBQTJCO1lBQ3RELDZCQUE2QixFQUFFLCtCQUErQjtZQUM5RCwwQ0FBMEMsRUFBRSw0Q0FBNEM7WUFDeEYsNEJBQTRCLEVBQUUsOEJBQThCO1lBQzVELGtCQUFrQixFQUFFLG9CQUFvQjtZQUN4QyxxQ0FBcUMsRUFBRSx1Q0FBdUM7WUFDOUUsWUFBWSxFQUFFLGNBQWM7WUFDNUIsY0FBYyxFQUFFLGdCQUFnQjtZQUNoQyxXQUFXLEVBQUUsYUFBYTtZQUMxQix5QkFBeUIsRUFBRSwyQkFBMkI7WUFDdEQsa0JBQWtCLEVBQUUsb0JBQW9CO1lBQ3hDLDJCQUEyQixFQUFFLDZCQUE2QjtZQUMxRCxrQkFBa0IsRUFBRSxvQkFBb0I7U0FDekMsQ0FBQzs7O0lBUUYsd0NBQU8sYUFBQyxPQUF1QjtJQVUvQix3Q0FBTyxhQUFDLE9BQXVCO0lBUy9CLDRDQUFXO0lBVVgsdUNBQU0sYUFBQyxPQUF5Qjs7Ozs7O0lBL0VyQixzQkFBc0I7UUFEbEMsVUFBVSxFQUFFO09BQ0Esc0JBQXNCO2lDQWhLbkM7RUFnSzRDLGlCQUFpQjtTQUFoRCxzQkFBc0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBDb3Jkb3ZhLCBJb25pY05hdGl2ZVBsdWdpbiwgUGx1Z2luIH0gZnJvbSAnQGlvbmljLW5hdGl2ZS9jb3JlJztcblxuZXhwb3J0IGludGVyZmFjZSBBRkFBdXRoT3B0aW9ucyB7XG4gIC8qKlxuICAgKiBSZXF1aXJlZFxuICAgKiBVc2VkIGFzIHRoZSBhbGlhcyBmb3IgeW91ciBrZXkgaW4gdGhlIEFuZHJvaWQgS2V5IFN0b3JlLlxuICAgKi9cbiAgY2xpZW50SWQ6IHN0cmluZztcblxuICAvKipcbiAgICogVXNlZCB0byBjcmVhdGUgY3JlZGVudGlhbCBzdHJpbmcgZm9yIGVuY3J5cHRlZCB0b2tlbiBhbmQgYXMgYWxpYXMgdG8gcmV0cmlldmUgdGhlIGNpcGhlci5cbiAgICovXG4gIHVzZXJuYW1lPzogc3RyaW5nO1xuXG4gIC8qKlxuICAgKiBVc2VkIHRvIGNyZWF0ZSBjcmVkZW50aWFsIHN0cmluZyBmb3IgZW5jcnlwdGVkIHRva2VuXG4gICAqL1xuICBwYXNzd29yZD86IHN0cmluZztcblxuICAvKipcbiAgICogUmVxdWlyZWQgZm9yIGRlY3J5cHQoKVxuICAgKiBFbmNyeXB0ZWQgdXNlciBjcmVkZW50aWFscyB0byBkZWNyeXB0IHVwb24gc3VjY2Vzc2Z1bCBhdXRoZW50aWNhdGlvbi5cbiAgICovXG4gIHRva2VuPzogc3RyaW5nO1xuXG4gIC8qKlxuICAgKiBTZXQgdG8gdHJ1ZSB0byByZW1vdmUgdGhlIFwiVVNFIEJBQ0tVUFwiIGJ1dHRvblxuICAgKi9cbiAgZGlzYWJsZUJhY2t1cD86IGJvb2xlYW47XG5cbiAgLyoqXG4gICAqIENoYW5nZSB0aGUgbGFuZ3VhZ2UuIChlbl9VUyBvciBlcylcbiAgICovXG4gIGxvY2FsZT86IHN0cmluZztcblxuICAvKipcbiAgICogVGhlIGRldmljZSBtYXggaXMgNSBhdHRlbXB0cy4gU2V0IHRoaXMgcGFyYW1ldGVyIGlmIHlvdSB3YW50IHRvIGFsbG93IGZld2VyIHRoYW4gNSBhdHRlbXB0cy5cbiAgICovXG4gIG1heEF0dGVtcHRzPzogbnVtYmVyO1xuXG4gIC8qKlxuICAgKiBSZXF1aXJlIHRoZSB1c2VyIHRvIGF1dGhlbnRpY2F0ZSB3aXRoIGEgZmluZ2VycHJpbnQgdG8gYXV0aG9yaXplIGV2ZXJ5IHVzZSBvZiB0aGUga2V5LlxuICAgKiBOZXcgZmluZ2VycHJpbnQgZW5yb2xsbWVudCB3aWxsIGludmFsaWRhdGUga2V5IGFuZCByZXF1aXJlIGJhY2t1cCBhdXRoZW50aWNhdGUgdG9cbiAgICogcmUtZW5hYmxlIHRoZSBmaW5nZXJwcmludCBhdXRoZW50aWNhdGlvbiBkaWFsb2cuXG4gICAqL1xuICB1c2VyQXV0aFJlcXVpcmVkPzogYm9vbGVhbjtcblxuICAvKipcbiAgICogU2V0IHRoZSB0aXRsZSBvZiB0aGUgZmluZ2VycHJpbnQgYXV0aGVudGljYXRpb24gZGlhbG9nLlxuICAgKi9cbiAgZGlhbG9nVGl0bGU/OiBzdHJpbmc7XG5cbiAgLyoqXG4gICAqIFNldCB0aGUgbWVzc2FnZSBvZiB0aGUgZmluZ2VycHJpbnQgYXV0aGVudGljYXRpb24gZGlhbG9nLlxuICAgKi9cbiAgZGlhbG9nTWVzc2FnZT86IHN0cmluZztcblxuICAvKipcbiAgICogU2V0IHRoZSBoaW50IGRpc3BsYXllZCBieSB0aGUgZmluZ2VycHJpbnQgaWNvbiBvbiB0aGUgZmluZ2VycHJpbnQgYXV0aGVudGljYXRpb24gZGlhbG9nLlxuICAgKi9cbiAgZGlhbG9nSGludD86IHN0cmluZztcbn1cblxuZXhwb3J0IGludGVyZmFjZSBBRkFEZWNyeXB0T3B0aW9ucyB7XG4gIC8qKlxuICAgKiBCaW9tZXRyaWMgYXV0aGVudGljYXRpb25cbiAgICovXG4gIHdpdGhGaW5nZXJwcmludDogYm9vbGVhbjtcbiAgLyoqXG4gICAqIEF1dGhlbnRpY2F0aW9uIHVzaW5nIGJhY2t1cCBjcmVkZW50aWFsIGFjdGl2aXR5XG4gICAqL1xuICB3aXRoQmFja3VwOiBib29sZWFuO1xuICAvKipcbiAgICogRmluZ2VycHJpbnRBdXRoLkNpcGhlck1vZGUuREVDUllQVFxuICAgKiBEZWNyeXB0ZWQgcGFzc3dvcmRcbiAgICovXG4gIHBhc3N3b3JkOiBzdHJpbmc7XG59XG5cbmV4cG9ydCBpbnRlcmZhY2UgQUZBRW5jcnlwdFJlc3BvbnNlIHtcbiAgLyoqXG4gICAqIEJpb21ldHJpYyBhdXRoZW50aWNhdGlvblxuICAgKi9cbiAgd2l0aEZpbmdlcnByaW50OiBib29sZWFuO1xuICAvKipcbiAgICogQXV0aGVudGljYXRpb24gdXNpbmcgYmFja3VwIGNyZWRlbnRpYWwgYWN0aXZpdHlcbiAgICovXG4gIHdpdGhCYWNrdXA6IGJvb2xlYW47XG4gIC8qKlxuICAgKiBiYXNlNjRlbmNvZGVkIHN0cmluZyByZXByZXNlbnRhdGlvbiBvZiB1c2VyIGNyZWRlbnRpYWxzXG4gICAqL1xuICB0b2tlbjogc3RyaW5nO1xufVxuXG5leHBvcnQgaW50ZXJmYWNlIEFGQUF2YWlsYWJsZVJlc3BvbnNlIHtcbiAgaXNBdmFpbGFibGU6IGJvb2xlYW47XG4gIGlzSGFyZHdhcmVEZXRlY3RlZDogYm9vbGVhbjtcbiAgaGFzRW5yb2xsZWRGaW5nZXJwcmludHM6IGJvb2xlYW47XG59XG5cbmV4cG9ydCBpbnRlcmZhY2UgQUZBRGVsZXRlT3B0aW9ucyB7XG4gIGNsaWVudElkOiBzdHJpbmc7XG4gIHVzZXJuYW1lOiBzdHJpbmc7XG59XG5cbi8qKlxuICogQG5hbWUgQW5kcm9pZCBGaW5nZXJwcmludCBBdXRoXG4gKiBAZGVzY3JpcHRpb25cbiAqIFRoaXMgcGx1Z2luIHdpbGwgb3BlbiBhIG5hdGl2ZSBkaWFsb2cgZnJhZ21lbnQgcHJvbXB0aW5nIHRoZSB1c2VyIHRvIGF1dGhlbnRpY2F0ZSB1c2luZyB0aGVpciBmaW5nZXJwcmludC4gSWYgdGhlIGRldmljZSBoYXMgYSBzZWN1cmUgbG9ja3NjcmVlbiAocGF0dGVybiwgUElOLCBvciBwYXNzd29yZCksIHRoZSB1c2VyIG1heSBvcHQgdG8gYXV0aGVudGljYXRlIHVzaW5nIHRoYXQgbWV0aG9kIGFzIGEgYmFja3VwLlxuICogQHVzYWdlXG4gKiBgYGB0eXBlc2NyaXB0XG4gKiBpbXBvcnQgeyBBbmRyb2lkRmluZ2VycHJpbnRBdXRoIH0gZnJvbSAnQGlvbmljLW5hdGl2ZS9hbmRyb2lkLWZpbmdlcnByaW50LWF1dGgvbmd4JztcbiAqXG4gKiBjb25zdHJ1Y3Rvcihwcml2YXRlIGFuZHJvaWRGaW5nZXJwcmludEF1dGg6IEFuZHJvaWRGaW5nZXJwcmludEF1dGgpIHsgfVxuICpcbiAqIC4uLlxuICpcbiAqXG4gKiB0aGlzLmFuZHJvaWRGaW5nZXJwcmludEF1dGguaXNBdmFpbGFibGUoKVxuICogICAudGhlbigocmVzdWx0KT0+IHtcbiAqICAgICBpZihyZXN1bHQuaXNBdmFpbGFibGUpe1xuICogICAgICAgLy8gaXQgaXMgYXZhaWxhYmxlXG4gKlxuICogICAgICAgdGhpcy5hbmRyb2lkRmluZ2VycHJpbnRBdXRoLmVuY3J5cHQoeyBjbGllbnRJZDogJ215QXBwTmFtZScsIHVzZXJuYW1lOiAnbXlVc2VybmFtZScsIHBhc3N3b3JkOiAnbXlQYXNzd29yZCcgfSlcbiAqICAgICAgICAgLnRoZW4ocmVzdWx0ID0+IHtcbiAqICAgICAgICAgICAgaWYgKHJlc3VsdC53aXRoRmluZ2VycHJpbnQpIHtcbiAqICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKCdTdWNjZXNzZnVsbHkgZW5jcnlwdGVkIGNyZWRlbnRpYWxzLicpO1xuICogICAgICAgICAgICAgICAgY29uc29sZS5sb2coJ0VuY3J5cHRlZCBjcmVkZW50aWFsczogJyArIHJlc3VsdC50b2tlbik7XG4gKiAgICAgICAgICAgIH0gZWxzZSBpZiAocmVzdWx0LndpdGhCYWNrdXApIHtcbiAqICAgICAgICAgICAgICBjb25zb2xlLmxvZygnU3VjY2Vzc2Z1bGx5IGF1dGhlbnRpY2F0ZWQgd2l0aCBiYWNrdXAgcGFzc3dvcmQhJyk7XG4gKiAgICAgICAgICAgIH0gZWxzZSBjb25zb2xlLmxvZygnRGlkblxcJ3QgYXV0aGVudGljYXRlIScpO1xuICogICAgICAgICB9KVxuICogICAgICAgICAuY2F0Y2goZXJyb3IgPT4ge1xuICogICAgICAgICAgICBpZiAoZXJyb3IgPT09IHRoaXMuYW5kcm9pZEZpbmdlcnByaW50QXV0aC5FUlJPUlMuRklOR0VSUFJJTlRfQ0FOQ0VMTEVEKSB7XG4gKiAgICAgICAgICAgICAgY29uc29sZS5sb2coJ0ZpbmdlcnByaW50IGF1dGhlbnRpY2F0aW9uIGNhbmNlbGxlZCcpO1xuICogICAgICAgICAgICB9IGVsc2UgY29uc29sZS5lcnJvcihlcnJvcilcbiAqICAgICAgICAgfSk7XG4gKlxuICogICAgIH0gZWxzZSB7XG4gKiAgICAgICAvLyBmaW5nZXJwcmludCBhdXRoIGlzbid0IGF2YWlsYWJsZVxuICogICAgIH1cbiAqICAgfSlcbiAqICAgLmNhdGNoKGVycm9yID0+IGNvbnNvbGUuZXJyb3IoZXJyb3IpKTtcbiAqIGBgYFxuICogQGludGVyZmFjZXNcbiAqIEFGQUF1dGhPcHRpb25zXG4gKiBBRkFFbmNyeXB0UmVzcG9uc2VcbiAqIEFGQURlY3J5cHRPcHRpb25zXG4gKiBBRkFBdmFpbGFibGVSZXNwb25zZVxuICogQUZBRGVsZXRlT3B0aW9uc1xuICovXG5AUGx1Z2luKHtcbiAgcGx1Z2luTmFtZTogJ0FuZHJvaWRGaW5nZXJwcmludEF1dGgnLFxuICBwbHVnaW46ICdjb3Jkb3ZhLXBsdWdpbi1hbmRyb2lkLWZpbmdlcnByaW50LWF1dGgnLFxuICBwbHVnaW5SZWY6ICdGaW5nZXJwcmludEF1dGgnLFxuICByZXBvOiAnaHR0cHM6Ly9naXRodWIuY29tL21qd2hlYXRsZXkvY29yZG92YS1wbHVnaW4tYW5kcm9pZC1maW5nZXJwcmludC1hdXRoJyxcbiAgcGxhdGZvcm1zOiBbJ0FuZHJvaWQnXVxufSlcbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBBbmRyb2lkRmluZ2VycHJpbnRBdXRoIGV4dGVuZHMgSW9uaWNOYXRpdmVQbHVnaW4ge1xuICAvKipcbiAgICogQ29udmVuaWVuY2UgcHJvcGVydHkgY29udGFpbmluZyBhbGwgcG9zc2libGUgZXJyb3JzXG4gICAqL1xuICBFUlJPUlM6IHtcbiAgICBCQURfUEFERElOR19FWENFUFRJT046IHN0cmluZztcbiAgICBDRVJUSUZJQ0FURV9FWENFUFRJT046IHN0cmluZztcbiAgICBGSU5HRVJQUklOVF9DQU5DRUxMRUQ6IHN0cmluZztcbiAgICBGSU5HRVJQUklOVF9EQVRBX05PVF9ERUxFVEVEOiBzdHJpbmc7XG4gICAgRklOR0VSUFJJTlRfRVJST1I6IHN0cmluZztcbiAgICBGSU5HRVJQUklOVF9OT1RfQVZBSUxBQkxFOiBzdHJpbmc7XG4gICAgRklOR0VSUFJJTlRfUEVSTUlTU0lPTl9ERU5JRUQ6IHN0cmluZztcbiAgICBGSU5HRVJQUklOVF9QRVJNSVNTSU9OX0RFTklFRF9TSE9XX1JFUVVFU1Q6IHN0cmluZztcbiAgICBJTExFR0FMX0JMT0NLX1NJWkVfRVhDRVBUSU9OOiBzdHJpbmc7XG4gICAgSU5JVF9DSVBIRVJfRkFJTEVEOiBzdHJpbmc7XG4gICAgSU5WQUxJRF9BTEdPUklUSE1fUEFSQU1FVEVSX0VYQ0VQVElPTjogc3RyaW5nO1xuICAgIElPX0VYQ0VQVElPTjogc3RyaW5nO1xuICAgIEpTT05fRVhDRVBUSU9OOiBzdHJpbmc7XG4gICAgTUlOSU1VTV9TREs6IHN0cmluZztcbiAgICBNSVNTSU5HX0FDVElPTl9QQVJBTUVURVJTOiBzdHJpbmc7XG4gICAgTUlTU0lOR19QQVJBTUVURVJTOiBzdHJpbmc7XG4gICAgTk9fU1VDSF9BTEdPUklUSE1fRVhDRVBUSU9OOiBzdHJpbmc7XG4gICAgU0VDVVJJVFlfRVhDRVBUSU9OOiBzdHJpbmc7XG4gIH0gPSB7XG4gICAgQkFEX1BBRERJTkdfRVhDRVBUSU9OOiAnQkFEX1BBRERJTkdfRVhDRVBUSU9OJyxcbiAgICBDRVJUSUZJQ0FURV9FWENFUFRJT046ICdDRVJUSUZJQ0FURV9FWENFUFRJT04nLFxuICAgIEZJTkdFUlBSSU5UX0NBTkNFTExFRDogJ0ZJTkdFUlBSSU5UX0NBTkNFTExFRCcsXG4gICAgRklOR0VSUFJJTlRfREFUQV9OT1RfREVMRVRFRDogJ0ZJTkdFUlBSSU5UX0RBVEFfTk9UX0RFTEVURUQnLFxuICAgIEZJTkdFUlBSSU5UX0VSUk9SOiAnRklOR0VSUFJJTlRfRVJST1InLFxuICAgIEZJTkdFUlBSSU5UX05PVF9BVkFJTEFCTEU6ICdGSU5HRVJQUklOVF9OT1RfQVZBSUxBQkxFJyxcbiAgICBGSU5HRVJQUklOVF9QRVJNSVNTSU9OX0RFTklFRDogJ0ZJTkdFUlBSSU5UX1BFUk1JU1NJT05fREVOSUVEJyxcbiAgICBGSU5HRVJQUklOVF9QRVJNSVNTSU9OX0RFTklFRF9TSE9XX1JFUVVFU1Q6ICdGSU5HRVJQUklOVF9QRVJNSVNTSU9OX0RFTklFRF9TSE9XX1JFUVVFU1QnLFxuICAgIElMTEVHQUxfQkxPQ0tfU0laRV9FWENFUFRJT046ICdJTExFR0FMX0JMT0NLX1NJWkVfRVhDRVBUSU9OJyxcbiAgICBJTklUX0NJUEhFUl9GQUlMRUQ6ICdJTklUX0NJUEhFUl9GQUlMRUQnLFxuICAgIElOVkFMSURfQUxHT1JJVEhNX1BBUkFNRVRFUl9FWENFUFRJT046ICdJTlZBTElEX0FMR09SSVRITV9QQVJBTUVURVJfRVhDRVBUSU9OJyxcbiAgICBJT19FWENFUFRJT046ICdJT19FWENFUFRJT04nLFxuICAgIEpTT05fRVhDRVBUSU9OOiAnSlNPTl9FWENFUFRJT04nLFxuICAgIE1JTklNVU1fU0RLOiAnTUlOSU1VTV9TREsnLFxuICAgIE1JU1NJTkdfQUNUSU9OX1BBUkFNRVRFUlM6ICdNSVNTSU5HX0FDVElPTl9QQVJBTUVURVJTJyxcbiAgICBNSVNTSU5HX1BBUkFNRVRFUlM6ICdNSVNTSU5HX1BBUkFNRVRFUlMnLFxuICAgIE5PX1NVQ0hfQUxHT1JJVEhNX0VYQ0VQVElPTjogJ05PX1NVQ0hfQUxHT1JJVEhNX0VYQ0VQVElPTicsXG4gICAgU0VDVVJJVFlfRVhDRVBUSU9OOiAnU0VDVVJJVFlfRVhDRVBUSU9OJ1xuICB9O1xuXG4gIC8qKlxuICAgKiBPcGVucyBhIG5hdGl2ZSBkaWFsb2cgZnJhZ21lbnQgdG8gdXNlIHRoZSBkZXZpY2UgaGFyZHdhcmUgZmluZ2VycHJpbnQgc2Nhbm5lciB0byBhdXRoZW50aWNhdGUgYWdhaW5zdCBmaW5nZXJwcmludHMgcmVnaXN0ZXJlZCBmb3IgdGhlIGRldmljZS5cbiAgICogQHBhcmFtIHtBRkFBdXRoT3B0aW9uc30gb3B0aW9ucyBPcHRpb25zXG4gICAqIEByZXR1cm5zIHtQcm9taXNlPEFGQUVuY3J5cHRSZXNwb25zZT59XG4gICAqL1xuICBAQ29yZG92YSgpXG4gIGVuY3J5cHQob3B0aW9uczogQUZBQXV0aE9wdGlvbnMpOiBQcm9taXNlPEFGQUVuY3J5cHRSZXNwb25zZT4ge1xuICAgIHJldHVybjtcbiAgfVxuXG4gIC8qKlxuICAgKiBPcGVucyBhIG5hdGl2ZSBkaWFsb2cgZnJhZ21lbnQgdG8gdXNlIHRoZSBkZXZpY2UgaGFyZHdhcmUgZmluZ2VycHJpbnQgc2Nhbm5lciB0byBhdXRoZW50aWNhdGUgYWdhaW5zdCBmaW5nZXJwcmludHMgcmVnaXN0ZXJlZCBmb3IgdGhlIGRldmljZS5cbiAgICogQHBhcmFtIHtBRkFBdXRoT3B0aW9uc30gb3B0aW9ucyBPcHRpb25zXG4gICAqIEByZXR1cm5zIHtQcm9taXNlPEFGQURlY3J5cHRPcHRpb25zPn1cbiAgICovXG4gIEBDb3Jkb3ZhKClcbiAgZGVjcnlwdChvcHRpb25zOiBBRkFBdXRoT3B0aW9ucyk6IFByb21pc2U8QUZBRGVjcnlwdE9wdGlvbnM+IHtcbiAgICByZXR1cm47XG4gIH1cblxuICAvKipcbiAgICogQ2hlY2sgaWYgc2VydmljZSBpcyBhdmFpbGFibGVcbiAgICogQHJldHVybnMge1Byb21pc2U8QUZBQXZhaWxhYmxlUmVzcG9uc2U+fSBSZXR1cm5zIGEgUHJvbWlzZSB0aGF0IHJlc29sdmVzIGlmIGZpbmdlcnByaW50IGF1dGggaXMgYXZhaWxhYmxlIG9uIHRoZSBkZXZpY2VcbiAgICovXG4gIEBDb3Jkb3ZhKClcbiAgaXNBdmFpbGFibGUoKTogUHJvbWlzZTxBRkFBdmFpbGFibGVSZXNwb25zZT4ge1xuICAgIHJldHVybjtcbiAgfVxuXG4gIC8qKlxuICAgKiBEZWxldGUgdGhlIGNpcGhlciB1c2VkIGZvciBlbmNyeXB0aW9uIGFuZCBkZWNyeXB0aW9uIGJ5IHVzZXJuYW1lXG4gICAqIEBwYXJhbSB7QUZBRGVsZXRlT3B0aW9uc30gb3B0aW9ucyBPcHRpb25zXG4gICAqIEByZXR1cm5zIHtQcm9taXNlPHsgZGVsZXRlZDogYm9vbGVhbiB9Pn0gUmV0dXJucyBhIFByb21pc2UgdGhhdCByZXNvbHZlcyBpZiB0aGUgY2lwaGVyIHdhcyBzdWNjZXNzZnVsbHkgZGVsZXRlZFxuICAgKi9cbiAgQENvcmRvdmEoKVxuICBkZWxldGUob3B0aW9uczogQUZBRGVsZXRlT3B0aW9ucyk6IFByb21pc2U8eyBkZWxldGVkOiBib29sZWFuIH0+IHtcbiAgICByZXR1cm47XG4gIH1cbn1cbiJdfQ==

/***/ }),

/***/ "./node_modules/@ionic-native/fingerprint-aio/ngx/index.js":
/*!*****************************************************************!*\
  !*** ./node_modules/@ionic-native/fingerprint-aio/ngx/index.js ***!
  \*****************************************************************/
/*! exports provided: FingerprintAIO */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FingerprintAIO", function() { return FingerprintAIO; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_native_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic-native/core */ "./node_modules/@ionic-native/core/index.js");



var FingerprintAIO = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](FingerprintAIO, _super);
    function FingerprintAIO() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        /**
         * Convenience constant
         * @type {number}
         */
        _this.BIOMETRIC_UNKNOWN_ERROR = -100;
        /**
         * Convenience constant
         * @type {number}
         */
        _this.BIOMETRIC_UNAVAILABLE = -101;
        /**
         * Convenience constant
         * @type {number}
         */
        _this.BIOMETRIC_AUTHENTICATION_FAILED = -102;
        /**
         * Convenience constant
         * @type {number}
         */
        _this.BIOMETRIC_SDK_NOT_SUPPORTED = -103;
        /**
         * Convenience constant
         * @type {number}
         */
        _this.BIOMETRIC_HARDWARE_NOT_SUPPORTED = -104;
        /**
         * Convenience constant
         * @type {number}
         */
        _this.BIOMETRIC_PERMISSION_NOT_GRANTED = -105;
        /**
         * Convenience constant
         * @type {number}
         */
        _this.BIOMETRIC_NOT_ENROLLED = -106;
        /**
         * Convenience constant
         * @type {number}
         */
        _this.BIOMETRIC_INTERNAL_PLUGIN_ERROR = -107;
        /**
         * Convenience constant
         * @type {number}
         */
        _this.BIOMETRIC_DISMISSED = -108;
        /**
         * Convenience constant
         * @type {number}
         */
        _this.BIOMETRIC_PIN_OR_PATTERN_DISMISSED = -109;
        /**
         * Convenience constant
         * @type {number}
         */
        _this.BIOMETRIC_SCREEN_GUARD_UNSECURED = -110;
        /**
         * Convenience constant
         * @type {number}
         */
        _this.BIOMETRIC_LOCKED_OUT = -111;
        /**
         * Convenience constant
         * @type {number}
         */
        _this.BIOMETRIC_LOCKED_OUT_PERMANENT = -112;
        return _this;
    }
    FingerprintAIO.prototype.isAvailable = function () { return Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_2__["cordova"])(this, "isAvailable", {}, arguments); };
    FingerprintAIO.prototype.show = function (options) { return Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_2__["cordova"])(this, "show", {}, arguments); };
    FingerprintAIO.pluginName = "FingerprintAIO";
    FingerprintAIO.plugin = "cordova-plugin-fingerprint-aio";
    FingerprintAIO.pluginRef = "Fingerprint";
    FingerprintAIO.repo = "https://github.com/NiklasMerz/cordova-plugin-fingerprint-aio";
    FingerprintAIO.platforms = ["Android", "iOS"];
    FingerprintAIO = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()
    ], FingerprintAIO);
    return FingerprintAIO;
}(_ionic_native_core__WEBPACK_IMPORTED_MODULE_2__["IonicNativePlugin"]));

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi8uLi9zcmMvQGlvbmljLW5hdGl2ZS9wbHVnaW5zL2ZpbmdlcnByaW50LWFpby9uZ3gvaW5kZXgudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyw4QkFBc0MsTUFBTSxvQkFBb0IsQ0FBQzs7SUFrRnBDLDBDQUFpQjs7O1FBRW5EOzs7V0FHRztRQUNILDZCQUF1QixHQUFHLENBQUMsR0FBRyxDQUFDO1FBQy9COzs7V0FHRztRQUNILDJCQUFxQixHQUFHLENBQUMsR0FBRyxDQUFDO1FBQzdCOzs7V0FHRztRQUNILHFDQUErQixHQUFHLENBQUMsR0FBRyxDQUFDO1FBQ3ZDOzs7V0FHRztRQUNILGlDQUEyQixHQUFHLENBQUMsR0FBRyxDQUFDO1FBQ25DOzs7V0FHRztRQUNILHNDQUFnQyxHQUFHLENBQUMsR0FBRyxDQUFDO1FBQ3hDOzs7V0FHRztRQUNILHNDQUFnQyxHQUFHLENBQUMsR0FBRyxDQUFDO1FBQ3hDOzs7V0FHRztRQUNILDRCQUFzQixHQUFHLENBQUMsR0FBRyxDQUFDO1FBQzlCOzs7V0FHRztRQUNILHFDQUErQixHQUFHLENBQUMsR0FBRyxDQUFDO1FBQ3ZDOzs7V0FHRztRQUNILHlCQUFtQixHQUFHLENBQUMsR0FBRyxDQUFDO1FBQzNCOzs7V0FHRztRQUNILHdDQUFrQyxHQUFHLENBQUMsR0FBRyxDQUFDO1FBQzFDOzs7V0FHRztRQUNILHNDQUFnQyxHQUFHLENBQUMsR0FBRyxDQUFDO1FBQ3hDOzs7V0FHRztRQUNILDBCQUFvQixHQUFHLENBQUMsR0FBRyxDQUFDO1FBQzVCOzs7V0FHRztRQUNILG9DQUE4QixHQUFHLENBQUMsR0FBRyxDQUFDOzs7SUFPdEMsb0NBQVc7SUFVWCw2QkFBSSxhQUFDLE9BQTJCOzs7Ozs7SUFuRnJCLGNBQWM7UUFEMUIsVUFBVSxFQUFFO09BQ0EsY0FBYzt5QkFuRjNCO0VBbUZvQyxpQkFBaUI7U0FBeEMsY0FBYyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IENvcmRvdmEsIElvbmljTmF0aXZlUGx1Z2luLCBQbHVnaW4gfSBmcm9tICdAaW9uaWMtbmF0aXZlL2NvcmUnO1xuXG5leHBvcnQgaW50ZXJmYWNlIEZpbmdlcnByaW50T3B0aW9ucyB7XG4gIC8qKlxuICAgKiBUaXRsZSBpbiBiaW9tZXRyaWMgcHJvbXB0IChhbmRyb2lkIG9ubHkpXG4gICAqIEBkZWZhdWx0IHtBUFBfTkFNRX0gQmlvbWV0cmljIFNpZ24gT25cbiAgICovXG4gIHRpdGxlPzogc3RyaW5nO1xuXG4gIC8qKlxuICAgKiBTdWJ0aXRsZSBpbiBiaW9tZXRyaWMgUHJvbXB0IChhbmRyb2lkIG9ubHkpXG4gICAqIEBkZWZhdWx0IG51bGxcbiAgICovXG4gIHN1YnRpdGxlPzogc3RyaW5nO1xuXG4gIC8qKlxuICAgKiBEZXNjcmlwdGlvbiBpbiBiaW9tZXRyaWMgUHJvbXB0XG4gICAqIEBkZWZhdWx0IG51bGxcbiAgICovXG4gIGRlc2NyaXB0aW9uPzogc3RyaW5nO1xuXG4gIC8qKlxuICAgKiBUaXRsZSBvZiBmYWxsYmFjayBidXR0b24uXG4gICAqIEBkZWZhdWx0IFwiVXNlIFBpblwiXG4gICAqL1xuICBmYWxsYmFja0J1dHRvblRpdGxlPzogc3RyaW5nO1xuXG4gIC8qKlxuICAgKiBUaXRsZSBmb3IgY2FuY2VsIGJ1dHRvbiBvbiBBbmRyb2lkXG4gICAqIEBkZWZhdWx0IFwiQ2FuY2VsXCJcbiAgICovXG4gIGNhbmNlbEJ1dHRvblRpdGxlPzogc3RyaW5nO1xuXG4gIC8qKlxuICAgKiBEaXNhYmxlICd1c2UgYmFja3VwJyBvcHRpb24uXG4gICAqIEBkZWZhdWx0IGZhbHNlXG4gICAqL1xuICBkaXNhYmxlQmFja3VwPzogYm9vbGVhbjtcblxufVxuXG4vKipcbiAqIEBuYW1lIEZpbmdlcnByaW50IEFJT1xuICogQGRlc2NyaXB0aW9uXG4gKiBVc2Ugc2ltcGxlIGZpbmdlcnByaW50IGF1dGhlbnRpY2F0aW9uIG9uIEFuZHJvaWQgYW5kIGlPUy5cbiAqIFJlcXVpcmVzIENvcmRvdmEgcGx1Z2luOiBjb3Jkb3ZhLXBsdWdpbi1maW5nZXJwcmludC1haW8uIEZvciBtb3JlIGluZm8gYWJvdXQgcGx1Z2luLCB2aXN0OiBodHRwczovL2dpdGh1Yi5jb20vTmlrbGFzTWVyei9jb3Jkb3ZhLXBsdWdpbi1maW5nZXJwcmludC1haW9cbiAqXG4gKiBAdXNhZ2VcbiAqIGBgYHR5cGVzY3JpcHRcbiAqIGltcG9ydCB7IEZpbmdlcnByaW50QUlPIH0gZnJvbSAnQGlvbmljLW5hdGl2ZS9maW5nZXJwcmludC1haW8vbmd4JztcbiAqXG4gKiBjb25zdHJ1Y3Rvcihwcml2YXRlIGZhaW86IEZpbmdlcnByaW50QUlPKSB7IH1cbiAqXG4gKiAuLi5cbiAqXG4gKiB0aGlzLmZhaW8uc2hvdygpLnRoZW4oKHJlc3VsdDogYW55KSA9PiBjb25zb2xlLmxvZyhyZXN1bHQpKS5jYXRjaCgoZXJyb3I6IGFueSkgPT4gY29uc29sZS5sb2coZXJyb3IpKTtcbiAqXG4gKiAgT1Igd2l0aCBvcHRpb25zLi4uXG4gKlxuICogdGhpcy5mYWlvLnNob3coe1xuICogICAgIHRpdGxlOiAnQmlvbWV0cmljIEF1dGhlbnRpY2F0aW9uJywgLy8gKEFuZHJvaWQgT25seSkgfCBvcHRpb25hbCB8IERlZmF1bHQ6IFwiPEFQUF9OQU1FPiBCaW9tZXRyaWMgU2lnbiBPblwiXG4gKiAgICAgc3VidGl0bGU6ICdDb29sZXN0IFBsdWdpbiBldmVyJyAvLyAoQW5kcm9pZCBPbmx5KSB8IG9wdGlvbmFsIHwgRGVmYXVsdDogbnVsbFxuICogICAgIGRlc2NyaXB0aW9uOiAnUGxlYXNlIGF1dGhlbnRpY2F0ZScgLy8gb3B0aW9uYWwgfCBEZWZhdWx0OiBudWxsXG4gKiAgICAgZmFsbGJhY2tCdXR0b25UaXRsZTogJ1VzZSBCYWNrdXAnLCAvLyBvcHRpb25hbCB8IFdoZW4gZGlzYWJsZUJhY2t1cCBpcyBmYWxzZSBkZWZhdWx0cyB0byBcIlVzZSBQaW5cIi5cbiAqICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vIFdoZW4gZGlzYWJsZUJhY2t1cCBpcyB0cnVlIGRlZmF1bHRzIHRvIFwiQ2FuY2VsXCJcbiAqICAgICBkaXNhYmxlQmFja3VwOnRydWUsICAvLyBvcHRpb25hbCB8IGRlZmF1bHQ6IGZhbHNlXG4gKiB9KVxuICogLnRoZW4oKHJlc3VsdDogYW55KSA9PiBjb25zb2xlLmxvZyhyZXN1bHQpKVxuICogLmNhdGNoKChlcnJvcjogYW55KSA9PiBjb25zb2xlLmxvZyhlcnJvcikpO1xuICpcbiAqIGBgYFxuICogQGludGVyZmFjZXNcbiAqIEZpbmdlcnByaW50T3B0aW9uc1xuICovXG5AUGx1Z2luKHtcbiAgcGx1Z2luTmFtZTogJ0ZpbmdlcnByaW50QUlPJyxcbiAgcGx1Z2luOiAnY29yZG92YS1wbHVnaW4tZmluZ2VycHJpbnQtYWlvJyxcbiAgcGx1Z2luUmVmOiAnRmluZ2VycHJpbnQnLFxuICByZXBvOiAnaHR0cHM6Ly9naXRodWIuY29tL05pa2xhc01lcnovY29yZG92YS1wbHVnaW4tZmluZ2VycHJpbnQtYWlvJyxcbiAgcGxhdGZvcm1zOiBbJ0FuZHJvaWQnLCAnaU9TJ11cbn0pXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgRmluZ2VycHJpbnRBSU8gZXh0ZW5kcyBJb25pY05hdGl2ZVBsdWdpbiB7XG5cbiAgLyoqXG4gICAqIENvbnZlbmllbmNlIGNvbnN0YW50XG4gICAqIEB0eXBlIHtudW1iZXJ9XG4gICAqL1xuICBCSU9NRVRSSUNfVU5LTk9XTl9FUlJPUiA9IC0xMDA7XG4gIC8qKlxuICAgKiBDb252ZW5pZW5jZSBjb25zdGFudFxuICAgKiBAdHlwZSB7bnVtYmVyfVxuICAgKi9cbiAgQklPTUVUUklDX1VOQVZBSUxBQkxFID0gLTEwMTtcbiAgLyoqXG4gICAqIENvbnZlbmllbmNlIGNvbnN0YW50XG4gICAqIEB0eXBlIHtudW1iZXJ9XG4gICAqL1xuICBCSU9NRVRSSUNfQVVUSEVOVElDQVRJT05fRkFJTEVEID0gLTEwMjtcbiAgLyoqXG4gICAqIENvbnZlbmllbmNlIGNvbnN0YW50XG4gICAqIEB0eXBlIHtudW1iZXJ9XG4gICAqL1xuICBCSU9NRVRSSUNfU0RLX05PVF9TVVBQT1JURUQgPSAtMTAzO1xuICAvKipcbiAgICogQ29udmVuaWVuY2UgY29uc3RhbnRcbiAgICogQHR5cGUge251bWJlcn1cbiAgICovXG4gIEJJT01FVFJJQ19IQVJEV0FSRV9OT1RfU1VQUE9SVEVEID0gLTEwNDtcbiAgLyoqXG4gICAqIENvbnZlbmllbmNlIGNvbnN0YW50XG4gICAqIEB0eXBlIHtudW1iZXJ9XG4gICAqL1xuICBCSU9NRVRSSUNfUEVSTUlTU0lPTl9OT1RfR1JBTlRFRCA9IC0xMDU7XG4gIC8qKlxuICAgKiBDb252ZW5pZW5jZSBjb25zdGFudFxuICAgKiBAdHlwZSB7bnVtYmVyfVxuICAgKi9cbiAgQklPTUVUUklDX05PVF9FTlJPTExFRCA9IC0xMDY7XG4gIC8qKlxuICAgKiBDb252ZW5pZW5jZSBjb25zdGFudFxuICAgKiBAdHlwZSB7bnVtYmVyfVxuICAgKi9cbiAgQklPTUVUUklDX0lOVEVSTkFMX1BMVUdJTl9FUlJPUiA9IC0xMDc7XG4gIC8qKlxuICAgKiBDb252ZW5pZW5jZSBjb25zdGFudFxuICAgKiBAdHlwZSB7bnVtYmVyfVxuICAgKi9cbiAgQklPTUVUUklDX0RJU01JU1NFRCA9IC0xMDg7XG4gIC8qKlxuICAgKiBDb252ZW5pZW5jZSBjb25zdGFudFxuICAgKiBAdHlwZSB7bnVtYmVyfVxuICAgKi9cbiAgQklPTUVUUklDX1BJTl9PUl9QQVRURVJOX0RJU01JU1NFRCA9IC0xMDk7XG4gIC8qKlxuICAgKiBDb252ZW5pZW5jZSBjb25zdGFudFxuICAgKiBAdHlwZSB7bnVtYmVyfVxuICAgKi9cbiAgQklPTUVUUklDX1NDUkVFTl9HVUFSRF9VTlNFQ1VSRUQgPSAtMTEwO1xuICAvKipcbiAgICogQ29udmVuaWVuY2UgY29uc3RhbnRcbiAgICogQHR5cGUge251bWJlcn1cbiAgICovXG4gIEJJT01FVFJJQ19MT0NLRURfT1VUID0gLTExMTtcbiAgLyoqXG4gICAqIENvbnZlbmllbmNlIGNvbnN0YW50XG4gICAqIEB0eXBlIHtudW1iZXJ9XG4gICAqL1xuICBCSU9NRVRSSUNfTE9DS0VEX09VVF9QRVJNQU5FTlQgPSAtMTEyO1xuXG4gIC8qKlxuICAgKiBDaGVjayBpZiBmaW5nZXJwcmludCBhdXRoZW50aWNhdGlvbiBpcyBhdmFpbGFibGVcbiAgICogQHJldHVybiB7UHJvbWlzZTxhbnk+fSBSZXR1cm5zIGEgcHJvbWlzZSB3aXRoIHJlc3VsdFxuICAgKi9cbiAgQENvcmRvdmEoKVxuICBpc0F2YWlsYWJsZSgpOiBQcm9taXNlPGFueT4ge1xuICAgIHJldHVybjtcbiAgfVxuXG4gIC8qKlxuICAgKiBTaG93IGF1dGhlbnRpY2F0aW9uIGRpYWxvZ3VlXG4gICAqIEBwYXJhbSB7RmluZ2VycHJpbnRPcHRpb25zfSBvcHRpb25zIE9wdGlvbnMgZm9yIHBsYXRmb3JtIHNwZWNpZmljIGZpbmdlcnByaW50IEFQSVxuICAgKiBAcmV0dXJuIHtQcm9taXNlPGFueT59IFJldHVybnMgYSBwcm9taXNlIHRoYXQgcmVzb2x2ZXMgd2hlbiBhdXRoZW50aWNhdGlvbiB3YXMgc3VjY2Vzc2Z1bFxuICAgKi9cbiAgQENvcmRvdmEoKVxuICBzaG93KG9wdGlvbnM6IEZpbmdlcnByaW50T3B0aW9ucyk6IFByb21pc2U8YW55PiB7XG4gICAgcmV0dXJuO1xuICB9XG59XG4iXX0=

/***/ }),

/***/ "./src/services/cookie.service.ts":
/*!****************************************!*\
  !*** ./src/services/cookie.service.ts ***!
  \****************************************/
/*! exports provided: CookieService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CookieService", function() { return CookieService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");



let CookieService = class CookieService {
    constructor(router) {
        this.router = router;
    }
    setItem(sKey, sValue) {
        // sDomain = environment.domain;
        localStorage.setItem(sKey, sValue);
        return true;
    }
    getItem(sKey) {
        if (!sKey) {
            return null;
        }
        return localStorage.getItem(sKey);
    }
    removeSingleItem(id) {
        localStorage.removeItem(id);
    }
    removeCookies() {
        localStorage.clear();
    }
};
CookieService.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
];
CookieService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
], CookieService);



/***/ }),

/***/ "./src/services/login.service.ts":
/*!***************************************!*\
  !*** ./src/services/login.service.ts ***!
  \***************************************/
/*! exports provided: LoginService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginService", function() { return LoginService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _cookie_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./cookie.service */ "./src/services/cookie.service.ts");

// import { window } from "rxjs/operator/window";






let LoginService = class LoginService {
    constructor(router, http, cookie) {
        this.router = router;
        this.http = http;
        this.cookie = cookie;
    }
    login(loginDetails) {
        var options = {};
        let headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]();
        headers.append("Content-Type", "application/json");
        // return this.http.post<any>('http://csas.meharsoft.com:85/routingmapi/api/routing',
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_5__["environment"].baseURL, { "APIName": "1", "CustomerId": `${loginDetails.customer_id}`, "InputParameters": `${loginDetails.customer_id}¤${loginDetails.password}` })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(res => res));
    }
    logout() {
        var options = {};
        let headers = new Headers();
        headers.append("Content-Type", "application/json");
        let cId = this.cookie.getItem("CustomerId");
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_5__["environment"].baseURL, { "APIName": "2", "CustomerId": cId, "InputParameters": `${cId}¤${2}` })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(res => res));
    }
    changePassword(dataToSend) {
        var options = {};
        let headers = new Headers();
        headers.append("Content-Type", "application/json");
        let cId = this.cookie.getItem("CustomerId");
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_5__["environment"].baseURL, { "APIName": "3", "CustomerId": cId, "InputParameters": `${cId}¤${dataToSend.oldPassword}¤${dataToSend.newPassword}` })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(res => res));
    }
};
LoginService.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] },
    { type: _cookie_service__WEBPACK_IMPORTED_MODULE_6__["CookieService"] }
];
LoginService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"],
        _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"],
        _cookie_service__WEBPACK_IMPORTED_MODULE_6__["CookieService"]])
], LoginService);



/***/ })

}]);
//# sourceMappingURL=default~changepassword-changepassword-module~login-login-module~logout-logout-module-es2015.js.map