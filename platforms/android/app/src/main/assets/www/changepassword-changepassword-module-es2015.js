(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["changepassword-changepassword-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/changepassword/changepassword.page.html":
/*!***********************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/changepassword/changepassword.page.html ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-content class=\"content content-md\">\n\n\n  <div class=\"banner\">\n    <h2 class=\"white-heading ion-text-center\">Bank App</h2>\n  </div>\n\n  <div class=\"ma30\">\n    <h2 class=\"mb20\" text-center=\"\">Change Password</h2>\n\n\n\n    <form [formGroup]=\"changePasswordForm\">\n\n      <ion-input class=\"input-f\" type=\"password\" formControlName=\"oldPassword\" placeholder=\"Enter Old Password\"></ion-input>\n      <ion-input class=\"input-f\" type=\"password\" formControlName=\"newPassword\" placeholder=\"Enter New Password\"></ion-input>\n    </form>\n\n\n\n\n    <button class=\"btngreen mt10\" ion-button block outline (click)=\"changePassword()\">Change Password</button>\n\n\n\n\n\n\n  </div>\n</ion-content>"

/***/ }),

/***/ "./src/app/changepassword/changepassword-routing.module.ts":
/*!*****************************************************************!*\
  !*** ./src/app/changepassword/changepassword-routing.module.ts ***!
  \*****************************************************************/
/*! exports provided: ChangePasswordPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChangePasswordPageRoutingModule", function() { return ChangePasswordPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _changepassword_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./changepassword.page */ "./src/app/changepassword/changepassword.page.ts");




const routes = [
    {
        path: '',
        component: _changepassword_page__WEBPACK_IMPORTED_MODULE_3__["ChangePasswordPage"]
    }
];
let ChangePasswordPageRoutingModule = class ChangePasswordPageRoutingModule {
};
ChangePasswordPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], ChangePasswordPageRoutingModule);



/***/ }),

/***/ "./src/app/changepassword/changepassword.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/changepassword/changepassword.module.ts ***!
  \*********************************************************/
/*! exports provided: ChangePasswordModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChangePasswordModule", function() { return ChangePasswordModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _ionic_native_fingerprint_aio_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic-native/fingerprint-aio/ngx */ "./node_modules/@ionic-native/fingerprint-aio/ngx/index.js");
/* harmony import */ var _ionic_native_android_fingerprint_auth_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic-native/android-fingerprint-auth/ngx */ "./node_modules/@ionic-native/android-fingerprint-auth/ngx/index.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _changepassword_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./changepassword-routing.module */ "./src/app/changepassword/changepassword-routing.module.ts");
/* harmony import */ var _changepassword_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./changepassword.page */ "./src/app/changepassword/changepassword.page.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");









let ChangePasswordModule = class ChangePasswordModule {
};
ChangePasswordModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_8__["FormsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_8__["ReactiveFormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _changepassword_routing_module__WEBPACK_IMPORTED_MODULE_6__["ChangePasswordPageRoutingModule"]
        ],
        declarations: [_changepassword_page__WEBPACK_IMPORTED_MODULE_7__["ChangePasswordPage"]],
        providers: [_ionic_native_fingerprint_aio_ngx__WEBPACK_IMPORTED_MODULE_3__["FingerprintAIO"], _ionic_native_android_fingerprint_auth_ngx__WEBPACK_IMPORTED_MODULE_4__["AndroidFingerprintAuth"]]
    })
], ChangePasswordModule);



/***/ }),

/***/ "./src/app/changepassword/changepassword.page.scss":
/*!*********************************************************!*\
  !*** ./src/app/changepassword/changepassword.page.scss ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NoYW5nZXBhc3N3b3JkL2NoYW5nZXBhc3N3b3JkLnBhZ2Uuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/changepassword/changepassword.page.ts":
/*!*******************************************************!*\
  !*** ./src/app/changepassword/changepassword.page.ts ***!
  \*******************************************************/
/*! exports provided: ChangePasswordPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChangePasswordPage", function() { return ChangePasswordPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _services_login_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/login.service */ "./src/services/login.service.ts");
/* harmony import */ var _services_cookie_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/cookie.service */ "./src/services/cookie.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_native_fingerprint_aio_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic-native/fingerprint-aio/ngx */ "./node_modules/@ionic-native/fingerprint-aio/ngx/index.js");
/* harmony import */ var _ionic_native_android_fingerprint_auth_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic-native/android-fingerprint-auth/ngx */ "./node_modules/@ionic-native/android-fingerprint-auth/ngx/index.js");









let ChangePasswordPage = class ChangePasswordPage {
    constructor(fb, menu, router, alertController, loadingController, loginService, cookieService, faio, androidFingerprintAuth) {
        this.fb = fb;
        this.menu = menu;
        this.router = router;
        this.alertController = alertController;
        this.loadingController = loadingController;
        this.loginService = loginService;
        this.cookieService = cookieService;
        this.faio = faio;
        this.androidFingerprintAuth = androidFingerprintAuth;
        this.emailRequiredError = false;
        this.passwordRequiredError = false;
        this.loading = {};
        this.initLoginForm();
    }
    ngOnInit() {
    }
    initLoginForm() {
        this.changePasswordForm = this.fb.group({
            oldPassword: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            newPassword: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
        });
    }
    presentAlert(type, subHeader, message) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                header: type,
                subHeader: subHeader,
                message: message,
                buttons: ['OK']
            });
            yield alert.present();
        });
    }
    changePassword() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            if (this.changePasswordForm.valid) {
                let dataToSend = {
                    oldPassword: this.changePasswordForm.value.oldPassword,
                    newPassword: this.changePasswordForm.value.newPassword
                };
                const loading = yield this.loadingController.create({
                    message: 'loading...',
                });
                yield loading.present();
                this.loginService.changePassword(dataToSend).subscribe(res => {
                    console.log(res);
                    loading.dismiss();
                    // this.loading = false;
                    if (res == 1) {
                        this.presentAlert("Success", "", "password succesfully changed");
                    }
                    else {
                        this.presentAlert("Error", "Not Matched", "Old Password not matched");
                        //loading.onDidDismiss();
                    }
                });
            }
            else {
                this.emailRequiredError = true;
                this.passwordRequiredError = true;
            }
        });
    }
};
ChangePasswordPage.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["MenuController"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] },
    { type: _services_login_service__WEBPACK_IMPORTED_MODULE_4__["LoginService"] },
    { type: _services_cookie_service__WEBPACK_IMPORTED_MODULE_5__["CookieService"] },
    { type: _ionic_native_fingerprint_aio_ngx__WEBPACK_IMPORTED_MODULE_7__["FingerprintAIO"] },
    { type: _ionic_native_android_fingerprint_auth_ngx__WEBPACK_IMPORTED_MODULE_8__["AndroidFingerprintAuth"] }
];
ChangePasswordPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-changepassword',
        template: __webpack_require__(/*! raw-loader!./changepassword.page.html */ "./node_modules/raw-loader/index.js!./src/app/changepassword/changepassword.page.html"),
        providers: [_services_login_service__WEBPACK_IMPORTED_MODULE_4__["LoginService"], _services_cookie_service__WEBPACK_IMPORTED_MODULE_5__["CookieService"]],
        styles: [__webpack_require__(/*! ./changepassword.page.scss */ "./src/app/changepassword/changepassword.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["MenuController"],
        _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"],
        _services_login_service__WEBPACK_IMPORTED_MODULE_4__["LoginService"],
        _services_cookie_service__WEBPACK_IMPORTED_MODULE_5__["CookieService"],
        _ionic_native_fingerprint_aio_ngx__WEBPACK_IMPORTED_MODULE_7__["FingerprintAIO"],
        _ionic_native_android_fingerprint_auth_ngx__WEBPACK_IMPORTED_MODULE_8__["AndroidFingerprintAuth"]])
], ChangePasswordPage);



/***/ })

}]);
//# sourceMappingURL=changepassword-changepassword-module-es2015.js.map