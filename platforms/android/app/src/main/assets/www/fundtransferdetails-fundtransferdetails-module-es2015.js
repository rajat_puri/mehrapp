(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["fundtransferdetails-fundtransferdetails-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/fundtransferdetails/fundtransferdetails.page.html":
/*!*********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/fundtransferdetails/fundtransferdetails.page.html ***!
  \*********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-title>fundtransferdetails</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\n</ion-content>\n"

/***/ }),

/***/ "./src/app/fundtransferdetails/fundtransferdetails-routing.module.ts":
/*!***************************************************************************!*\
  !*** ./src/app/fundtransferdetails/fundtransferdetails-routing.module.ts ***!
  \***************************************************************************/
/*! exports provided: FundtransferdetailsPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FundtransferdetailsPageRoutingModule", function() { return FundtransferdetailsPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _fundtransferdetails_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./fundtransferdetails.page */ "./src/app/fundtransferdetails/fundtransferdetails.page.ts");




const routes = [
    {
        path: '',
        component: _fundtransferdetails_page__WEBPACK_IMPORTED_MODULE_3__["FundtransferdetailsPage"]
    }
];
let FundtransferdetailsPageRoutingModule = class FundtransferdetailsPageRoutingModule {
};
FundtransferdetailsPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], FundtransferdetailsPageRoutingModule);



/***/ }),

/***/ "./src/app/fundtransferdetails/fundtransferdetails.module.ts":
/*!*******************************************************************!*\
  !*** ./src/app/fundtransferdetails/fundtransferdetails.module.ts ***!
  \*******************************************************************/
/*! exports provided: FundtransferdetailsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FundtransferdetailsPageModule", function() { return FundtransferdetailsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _fundtransferdetails_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./fundtransferdetails-routing.module */ "./src/app/fundtransferdetails/fundtransferdetails-routing.module.ts");
/* harmony import */ var _fundtransferdetails_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./fundtransferdetails.page */ "./src/app/fundtransferdetails/fundtransferdetails.page.ts");







let FundtransferdetailsPageModule = class FundtransferdetailsPageModule {
};
FundtransferdetailsPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _fundtransferdetails_routing_module__WEBPACK_IMPORTED_MODULE_5__["FundtransferdetailsPageRoutingModule"]
        ],
        declarations: [_fundtransferdetails_page__WEBPACK_IMPORTED_MODULE_6__["FundtransferdetailsPage"]]
    })
], FundtransferdetailsPageModule);



/***/ }),

/***/ "./src/app/fundtransferdetails/fundtransferdetails.page.scss":
/*!*******************************************************************!*\
  !*** ./src/app/fundtransferdetails/fundtransferdetails.page.scss ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2Z1bmR0cmFuc2ZlcmRldGFpbHMvZnVuZHRyYW5zZmVyZGV0YWlscy5wYWdlLnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/fundtransferdetails/fundtransferdetails.page.ts":
/*!*****************************************************************!*\
  !*** ./src/app/fundtransferdetails/fundtransferdetails.page.ts ***!
  \*****************************************************************/
/*! exports provided: FundtransferdetailsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FundtransferdetailsPage", function() { return FundtransferdetailsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let FundtransferdetailsPage = class FundtransferdetailsPage {
    constructor() { }
    ngOnInit() {
    }
};
FundtransferdetailsPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-fundtransferdetails',
        template: __webpack_require__(/*! raw-loader!./fundtransferdetails.page.html */ "./node_modules/raw-loader/index.js!./src/app/fundtransferdetails/fundtransferdetails.page.html"),
        styles: [__webpack_require__(/*! ./fundtransferdetails.page.scss */ "./src/app/fundtransferdetails/fundtransferdetails.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], FundtransferdetailsPage);



/***/ })

}]);
//# sourceMappingURL=fundtransferdetails-fundtransferdetails-module-es2015.js.map