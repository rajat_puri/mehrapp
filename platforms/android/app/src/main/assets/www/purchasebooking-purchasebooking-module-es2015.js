(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["purchasebooking-purchasebooking-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/purchasebooking/purchasebooking.page.html":
/*!*************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/purchasebooking/purchasebooking.page.html ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header  >\n  <ion-toolbar class=\"toolbar-blue\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <ion-title class=\"title-top\">\n     Purchase Booking\n    </ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n   <ion-grid  class=\"successfully-footer ion-no-padding\">\n  <ion-row class=\"successfully-footer\">\n    <ion-col class=\"pa0\">\n       <button [routerLink]=\"['/filter']\" class=\"filter\" ion-button block>  <ion-icon name=\"clipboard\"></ion-icon> Sort</button>\n    </ion-col>\n\t<ion-col class=\"pa0\">\n        <button [routerLink]=\"['/filter']\" class=\"filter\" ion-button block><ion-icon name=\"funnel\"></ion-icon> Filter</button>\n    </ion-col>\n  </ion-row>\n</ion-grid>\n\n<ion-row class=\"mt10\">\n    <ion-col size=\"6\">\n\t  <ion-card class=\"pa0 ma0 bg-white pa10 \" >\n<ion-row class=\"pa0 ma0\">\n    <ion-col class=\"pa0 ma0\" size=\"12\"> <img src=\"assets/imgs/product3.png\">  </ion-col>\n  </ion-row>\n  \n  <ion-row>\n  <ion-col>\n  <div class=\"black-heading mb5\">Bakhsish Combines</div>\n    <div class=\"price mb0\"><strong>₹ 556.00 </strong><del>₹ 700.00</del></div>\n    <div class=\"price mb0\">1Kg</div>\n    <div class=\"discount mb0 mt5\">20% Off*</div>  \n    </ion-col>\n\t</ion-row>  \n  <ion-row>\n  <ion-col class=\"ion-text-right\">\n   <span class=\"addcart\" >  <span class=\"text-right-all quantity-outer\"> <span class=\"quantity-gray-min\">-</span> <span  class=\"quantity\">5</span> <span class=\"quantity-gray-plus\">+</span> </span>   </span>\n  </ion-col>\n\t</ion-row>\n  \n </ion-card>\t  \n    </ion-col> \n\n\n    <ion-col size=\"6\">\n\t  <ion-card class=\"pa0 ma0 bg-white pa10 \" >\n<ion-row class=\"pa0 ma0\" >\n    <ion-col class=\"pa0 ma0\" size=\"12\"> <img src=\"assets/imgs/product3.png\">  </ion-col>\n  </ion-row>\n  \n  <ion-row>\n  <ion-col>\n  <div class=\"black-heading mb5\">Bakhsish Combines</div>\n    <div class=\"price mb0\"><strong>₹ 556.00 </strong><del>₹ 700.00</del></div>\n    <div class=\"price mb0\">1Kg</div>\n    <div class=\"discount mb0 mt5\">20% Off*</div>  \n    </ion-col>\n\t</ion-row>  \n  <ion-row>\n  <ion-col class=\"ion-text-right\">\n   <span class=\"addcart\" >  <span class=\"text-right-all quantity-outer\">  <span  class=\"quantity\">Add</span>  </span>   </span>\n  </ion-col>\n\t</ion-row>\n  \n </ion-card>\t  \n    </ion-col> \n\t\n\t<ion-col size=\"6\">\n\t  <ion-card class=\"pa0 ma0 bg-white pa10 \" >\n<ion-row class=\"pa0 ma0\" >\n    <ion-col class=\"pa0 ma0\" size=\"12\"> <img src=\"assets/imgs/product4.png\">  </ion-col>\n  </ion-row>\n  \n  <ion-row>\n  <ion-col>\n  <div class=\"black-heading mb5\">Bakhsish Combines</div>\n    <div class=\"price mb0\"><strong>₹ 556.00 </strong><del>₹ 700.00</del></div>\n    <div class=\"price mb0\">1Kg</div>\n    <div class=\"discount mb0 mt5\">20% Off*</div>  \n    </ion-col>\n\t</ion-row>  \n  <ion-row>\n  <ion-col class=\"ion-text-right\">\n   <span class=\"addcart\" >  <span class=\"text-right-all quantity-outer\">  <span  class=\"quantity\">Add</span>  </span>   </span>\n  </ion-col>\n\t</ion-row>\n  \n </ion-card>\t  \n    </ion-col> \n\t\n\t\n<ion-col size=\"6\">\n\t  <ion-card class=\"pa0 ma0 bg-white pa10 \" >\n<ion-row class=\"pa0 ma0\" >\n    <ion-col class=\"pa0 ma0\" size=\"12\"> <img src=\"assets/imgs/product5.png\">  </ion-col>\n  </ion-row>\n  \n  <ion-row>\n  <ion-col>\n  <div class=\"black-heading mb5\">Bakhsish Combines</div>\n    <div class=\"price mb0\"><strong>₹ 556.00 </strong><del>₹ 700.00</del></div>\n    <div class=\"price mb0\">1Kg</div>\n    <div class=\"discount mb0 mt5\">20% Off*</div>  \n    </ion-col>\n\t</ion-row>  \n  <ion-row>\n  <ion-col class=\"ion-text-right\">\n   <span class=\"addcart\" >  <span class=\"text-right-all quantity-outer\">  <span  class=\"quantity\">Add</span>  </span>   </span>\n  </ion-col>\n\t</ion-row>\n  \n </ion-card>\t  \n    </ion-col> \n\t\n  </ion-row>\n\n \n\n\n \n\n \n\n \n\n\n \n\n\n</ion-content>\n<ion-footer no-border class=\"successfully-footer\">\n\n   <ion-grid class=\"successfully-footer\">\n  <ion-row class=\"successfully-footer\">\n    <ion-col size=\"7\" class=\"successfully-footer\">\n        <button class=\"btnlightblue\" ion-button block>8 Items | ₹ 45000.00</button>\n    </ion-col>\n    <ion-col size=\"5\" class=\"successfully-footer\">\n   <button [routerLink]=\"['/purchasecart']\" class=\"btngreen\" ion-button block>View  Cart</button>\n    </ion-col>\n  \n   \n  </ion-row>\n</ion-grid>\n\n</ion-footer>\n"

/***/ }),

/***/ "./src/app/purchasebooking/purchasebooking-routing.module.ts":
/*!*******************************************************************!*\
  !*** ./src/app/purchasebooking/purchasebooking-routing.module.ts ***!
  \*******************************************************************/
/*! exports provided: PurchasebookingPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PurchasebookingPageRoutingModule", function() { return PurchasebookingPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _purchasebooking_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./purchasebooking.page */ "./src/app/purchasebooking/purchasebooking.page.ts");




const routes = [
    {
        path: '',
        component: _purchasebooking_page__WEBPACK_IMPORTED_MODULE_3__["PurchasebookingPage"]
    }
];
let PurchasebookingPageRoutingModule = class PurchasebookingPageRoutingModule {
};
PurchasebookingPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], PurchasebookingPageRoutingModule);



/***/ }),

/***/ "./src/app/purchasebooking/purchasebooking.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/purchasebooking/purchasebooking.module.ts ***!
  \***********************************************************/
/*! exports provided: PurchasebookingPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PurchasebookingPageModule", function() { return PurchasebookingPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _purchasebooking_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./purchasebooking-routing.module */ "./src/app/purchasebooking/purchasebooking-routing.module.ts");
/* harmony import */ var _purchasebooking_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./purchasebooking.page */ "./src/app/purchasebooking/purchasebooking.page.ts");







let PurchasebookingPageModule = class PurchasebookingPageModule {
};
PurchasebookingPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _purchasebooking_routing_module__WEBPACK_IMPORTED_MODULE_5__["PurchasebookingPageRoutingModule"]
        ],
        declarations: [_purchasebooking_page__WEBPACK_IMPORTED_MODULE_6__["PurchasebookingPage"]]
    })
], PurchasebookingPageModule);



/***/ }),

/***/ "./src/app/purchasebooking/purchasebooking.page.scss":
/*!***********************************************************!*\
  !*** ./src/app/purchasebooking/purchasebooking.page.scss ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3B1cmNoYXNlYm9va2luZy9wdXJjaGFzZWJvb2tpbmcucGFnZS5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/purchasebooking/purchasebooking.page.ts":
/*!*********************************************************!*\
  !*** ./src/app/purchasebooking/purchasebooking.page.ts ***!
  \*********************************************************/
/*! exports provided: PurchasebookingPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PurchasebookingPage", function() { return PurchasebookingPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let PurchasebookingPage = class PurchasebookingPage {
    constructor() { }
    ngOnInit() {
    }
};
PurchasebookingPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-purchasebooking',
        template: __webpack_require__(/*! raw-loader!./purchasebooking.page.html */ "./node_modules/raw-loader/index.js!./src/app/purchasebooking/purchasebooking.page.html"),
        styles: [__webpack_require__(/*! ./purchasebooking.page.scss */ "./src/app/purchasebooking/purchasebooking.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], PurchasebookingPage);



/***/ })

}]);
//# sourceMappingURL=purchasebooking-purchasebooking-module-es2015.js.map