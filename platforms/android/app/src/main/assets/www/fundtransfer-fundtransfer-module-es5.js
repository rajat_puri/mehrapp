(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["fundtransfer-fundtransfer-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/fundtransfer/fundtransfer.page.html":
/*!*******************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/fundtransfer/fundtransfer.page.html ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header  >\n  <ion-toolbar class=\"toolbar-blue\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <ion-title class=\"title-top\">\n   Fund Transfer\n    </ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\n\n \n \n\n \n \n  <ion-card class=\"ma15\">\n\n<ion-item lines=\"none\" class=\"bg-white\">\n  <ion-avatar slot=\"start\" >\n  <img src=\"assets/imgs/ic_transfer.png\">\n  </ion-avatar>\n  <ion-label class=\"\">\n  <div class=\"black-heading pt5\">Fund Transfer</div>\n  <div class=\"small-gray pt5\">Within Bank</div>\n\n  </ion-label>\n</ion-item>\n </ion-card>\n \n \n \n  <ion-card class=\"ma15\">\n\n<ion-item lines=\"none\" class=\"bg-white\">\n  <ion-avatar slot=\"start\" >\n  <img src=\"assets/imgs/ic_transfer.png\">\n  </ion-avatar>\n  <ion-label class=\"\">\n  <div class=\"black-heading pt5\">Fund Transfer</div>\n  <div class=\"small-gray pt5\">To other Bank</div>\n\n  </ion-label>\n</ion-item>\n </ion-card>\n \n  \n  <ion-card class=\"ma15\">\n\n<ion-item lines=\"none\" class=\"bg-white\">\n  <ion-avatar slot=\"start\" >\n  <img src=\"assets/imgs/ic_transfer.png\">\n  </ion-avatar>\n  <ion-label class=\"\">\n  <div class=\"black-heading pt5\">IMPS</div>\n  <div class=\"small-gray pt5\">Within Bank</div>\n\n  </ion-label>\n</ion-item>\n </ion-card>\n \n  \n  <ion-card class=\"ma15\">\n\n<ion-item lines=\"none\" class=\"bg-white\">\n  <ion-avatar slot=\"start\" >\n  <img src=\"assets/imgs/ic_transfer.png\">\n  </ion-avatar>\n  <ion-label class=\"\">\n  <div class=\"black-heading pt5\">NEFT</div>\n  <div class=\"small-gray pt5\">Within Bank</div>\n\n  </ion-label>\n</ion-item>\n </ion-card>\n \n  \n  <ion-card class=\"ma15\">\n\n<ion-item lines=\"none\" class=\"bg-white\">\n  <ion-avatar slot=\"start\" >\n  <img src=\"assets/imgs/ic_transfer.png\">\n  </ion-avatar>\n  <ion-label class=\"\">\n  <div class=\"black-heading pt5\">Pay with UPI</div>\n  <div class=\"small-gray pt5\">UPI transfer</div>\n\n  </ion-label>\n</ion-item>\n </ion-card>\n \n\n</ion-content>\n"

/***/ }),

/***/ "./src/app/fundtransfer/fundtransfer-routing.module.ts":
/*!*************************************************************!*\
  !*** ./src/app/fundtransfer/fundtransfer-routing.module.ts ***!
  \*************************************************************/
/*! exports provided: FundtransferPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FundtransferPageRoutingModule", function() { return FundtransferPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _fundtransfer_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./fundtransfer.page */ "./src/app/fundtransfer/fundtransfer.page.ts");




var routes = [
    {
        path: '',
        component: _fundtransfer_page__WEBPACK_IMPORTED_MODULE_3__["FundtransferPage"]
    }
];
var FundtransferPageRoutingModule = /** @class */ (function () {
    function FundtransferPageRoutingModule() {
    }
    FundtransferPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
        })
    ], FundtransferPageRoutingModule);
    return FundtransferPageRoutingModule;
}());



/***/ }),

/***/ "./src/app/fundtransfer/fundtransfer.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/fundtransfer/fundtransfer.module.ts ***!
  \*****************************************************/
/*! exports provided: FundtransferPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FundtransferPageModule", function() { return FundtransferPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _fundtransfer_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./fundtransfer-routing.module */ "./src/app/fundtransfer/fundtransfer-routing.module.ts");
/* harmony import */ var _fundtransfer_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./fundtransfer.page */ "./src/app/fundtransfer/fundtransfer.page.ts");







var FundtransferPageModule = /** @class */ (function () {
    function FundtransferPageModule() {
    }
    FundtransferPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _fundtransfer_routing_module__WEBPACK_IMPORTED_MODULE_5__["FundtransferPageRoutingModule"]
            ],
            declarations: [_fundtransfer_page__WEBPACK_IMPORTED_MODULE_6__["FundtransferPage"]]
        })
    ], FundtransferPageModule);
    return FundtransferPageModule;
}());



/***/ }),

/***/ "./src/app/fundtransfer/fundtransfer.page.scss":
/*!*****************************************************!*\
  !*** ./src/app/fundtransfer/fundtransfer.page.scss ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-content {\n  --ion-background-color:#f3f5f5;\n}\n\nion-item {\n  --ion-background-color:#fff;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZnVuZHRyYW5zZmVyL0U6XFxtZWhhci1iYW5rLWFwcC1tYXN0ZXJcXG1laGFyLWJhbmstYXBwLW1hc3Rlci9zcmNcXGFwcFxcZnVuZHRyYW5zZmVyXFxmdW5kdHJhbnNmZXIucGFnZS5zY3NzIiwic3JjL2FwcC9mdW5kdHJhbnNmZXIvZnVuZHRyYW5zZmVyLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLDhCQUFBO0FDQ0o7O0FERUE7RUFDRywyQkFBQTtBQ0NIIiwiZmlsZSI6InNyYy9hcHAvZnVuZHRyYW5zZmVyL2Z1bmR0cmFuc2Zlci5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tY29udGVudHtcbiAgICAtLWlvbi1iYWNrZ3JvdW5kLWNvbG9yOiNmM2Y1ZjU7XG59XG5cbmlvbi1pdGVte1xuICAgLS1pb24tYmFja2dyb3VuZC1jb2xvcjojZmZmO1xufSIsImlvbi1jb250ZW50IHtcbiAgLS1pb24tYmFja2dyb3VuZC1jb2xvcjojZjNmNWY1O1xufVxuXG5pb24taXRlbSB7XG4gIC0taW9uLWJhY2tncm91bmQtY29sb3I6I2ZmZjtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/fundtransfer/fundtransfer.page.ts":
/*!***************************************************!*\
  !*** ./src/app/fundtransfer/fundtransfer.page.ts ***!
  \***************************************************/
/*! exports provided: FundtransferPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FundtransferPage", function() { return FundtransferPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var FundtransferPage = /** @class */ (function () {
    function FundtransferPage() {
    }
    FundtransferPage.prototype.ngOnInit = function () {
    };
    FundtransferPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-fundtransfer',
            template: __webpack_require__(/*! raw-loader!./fundtransfer.page.html */ "./node_modules/raw-loader/index.js!./src/app/fundtransfer/fundtransfer.page.html"),
            styles: [__webpack_require__(/*! ./fundtransfer.page.scss */ "./src/app/fundtransfer/fundtransfer.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], FundtransferPage);
    return FundtransferPage;
}());



/***/ })

}]);
//# sourceMappingURL=fundtransfer-fundtransfer-module-es5.js.map