(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["home-home-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/home/home.page.html":
/*!***************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/home/home.page.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header  no-border >\n  <ion-toolbar class=\"toolbar-blue\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <ion-title class=\"title-top\">\n      Welcome Gaurav\n    </ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\n  <div class=\"card-bg\"></div>\n\n\n  <div class=\"scroll-x\">\n    <div class=\"item_scroll\" [routerLink]=\"['/alltransaction']\">\n      <h3>Saving Account</h3>\n      <h2>0014 1241 5574</h2>\n      <h4>Check Balance</h4>\n      <h5>statement</h5>\n    </div>\n\n    <div class=\"item_scroll\" [routerLink]=\"['/loanaccount']\">\n      <h3>Loan Account</h3>\n      <h2>5574  1241 0014</h2>\n      <h4>Add Deposit</h4>\n      <h5>statement</h5>\n    </div>\n\n\n    <div class=\"item_scroll\" [routerLink]=\"['/alltransaction']\">\n      <h3>Saving Account</h3>\n      <h2>0014 1241 5574</h2>\n      <h4>Check Balance</h4>\n      <h5>statement</h5>\n    </div>\n\n    <div class=\"item_scroll\" [routerLink]=\"['/alltransaction']\">\n      <h3>Saving Account</h3>\n      <h2>0014 1241 5574</h2>\n      <h4>Check Balance</h4>\n      <h5>statement</h5>\n    </div>\n  </div>\n<ion-grid>\n<div class=\"home_menu\">\n  <ion-row>\n    <ion-col size=\"4\" [routerLink]=\"['/myaccount']\">\n      <div class=\"menu_item\" text-center=\"\">\n\t\t\t\t\t<div class=\"icon_box\">\n\t\t\t\t\t\t<img src=\"assets/imgs/ic_account.png\">\n\t\t\t\t\t</div>\n\t\t\t\t\t<p>Account</p>\n\t\t\t\t</div>\n    </ion-col>\n\t\n\t <ion-col size=\"4\" [routerLink]=\"['/fundtransfer']\">\n      <div class=\"menu_item\" text-center=\"\">\n\t\t\t\t\t<div class=\"icon_box\">\n\t\t\t\t\t\t<img src=\"assets/imgs/ic_fund_transfer.png\">\n\t\t\t\t\t</div>\n\t\t\t\t\t<p>Fund Transfer</p>\n\t\t\t\t</div>\n    </ion-col>\n\t\t <ion-col size=\"4\" [routerLink]=\"['/loanaccount']\">\n      <div class=\"menu_item\" text-center=\"\">\n\t\t\t\t\t<div class=\"icon_box\">\n\t\t\t\t\t\t<img src=\"assets/imgs/ic_loan.png\">\n\t\t\t\t\t</div>\n\t\t\t\t\t<p>Loan</p>\n\t\t\t\t</div>\n    </ion-col>\n\t <ion-col size=\"4\" [routerLink]=\"['/alltransaction']\">\n      <div class=\"menu_item\" text-center=\"\">\n\t\t\t\t\t<div class=\"icon_box\">\n\t\t\t\t\t\t<img src=\"assets/imgs/ic_statement.png\">\n\t\t\t\t\t</div>\n\t\t\t\t\t<p>Statement</p>\n\t\t\t\t</div>\n    </ion-col>\n\t\n\t <ion-col size=\"4\" [routerLink]=\"['/aritoolbooking']\">\n      <div class=\"menu_item\" text-center=\"\">\n\t\t\t\t\t<div class=\"icon_box\">\n\t\t\t\t\t\t<img src=\"assets/imgs/book-tool.png\">\n\t\t\t\t\t</div>\n\t\t\t\t\t<p>Tools Booking</p>\n\t\t\t\t</div>\n    </ion-col>\n\t\n\t <ion-col size=\"4\" [routerLink]=\"['/purchasebooking']\">\n      <div class=\"menu_item\" text-center=\"\">\n\t\t\t\t\t<div class=\"icon_box\">\n\t\t\t\t\t\t<img src=\"assets/imgs/agri-book.png\">\n\t\t\t\t\t</div>\n\t\t\t\t\t<p>Purchase booking</p>\n\t\t\t\t</div>\n    </ion-col>\n\t\n\t\n\t\t <ion-col size=\"4\" [routerLink]=\"['/deposit']\">\n      <div class=\"menu_item\" text-center=\"\">\n\t\t\t\t\t<div class=\"icon_box\">\n\t\t\t\t\t\t<img src=\"assets/imgs/deposite.png\">\n\t\t\t\t\t</div>\n\t\t\t\t\t<p>Deposit</p>\n\t\t\t\t</div>\n    </ion-col>\n\t\n\t <ion-col size=\"4\" [routerLink]=\"['/allrd']\">\n      <div class=\"menu_item\" text-center=\"\">\n\t\t\t\t\t<div class=\"icon_box\">\n\t\t\t\t\t\t<img src=\"assets/imgs/banking_act.png\">\n\t\t\t\t\t</div>\n\t\t\t\t\t<p>RD</p>\n\t\t\t\t</div>\n    </ion-col>\n\t\n  </ion-row>\n  </div>\n</ion-grid>\n\n\n\n\n<ion-list no-lines class=\"banner-bg mt30\">\n\t\t<ion-item no-padding class=\"banner-bg\">\n\t\t<div class=\"img_box\">\n\t\t\t\t<img width=\"123px;\" src=\"assets/imgs/img_farmer.png\">\n\t\t\t</div><div class=\"item-inner\">\n\t\t\t<div class=\"input-wrapper\">\n\t\t\t<ion-label>\n\t\t\t<div class=\"text-box\">\n\t\t\t\t<h3>Loan for Farmers</h3>\n\t\t\t\t<h2>discover yourself !</h2>\n\t\t\t\t<h4 class=\"d-flex\"><span>Apply Now</span>Instant loan at Just 4.9%</h4>\n\t\t\t</div>\n\t\t</ion-label></div>\n\t\t</div><div class=\"button-effect\"></div>\n\t\t</ion-item>\n\n\t\t\n\t</ion-list>\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n</ion-content>\n"

/***/ }),

/***/ "./src/app/home/home.module.ts":
/*!*************************************!*\
  !*** ./src/app/home/home.module.ts ***!
  \*************************************/
/*! exports provided: HomePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageModule", function() { return HomePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _home_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./home.page */ "./src/app/home/home.page.ts");







var HomePageModule = /** @class */ (function () {
    function HomePageModule() {
    }
    HomePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_5__["RouterModule"].forChild([
                    {
                        path: '',
                        component: _home_page__WEBPACK_IMPORTED_MODULE_6__["HomePage"]
                    }
                ])
            ],
            declarations: [_home_page__WEBPACK_IMPORTED_MODULE_6__["HomePage"]]
        })
    ], HomePageModule);
    return HomePageModule;
}());



/***/ }),

/***/ "./src/app/home/home.page.scss":
/*!*************************************!*\
  !*** ./src/app/home/home.page.scss ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".welcome-card img {\n  max-height: 35vh;\n  overflow: hidden;\n}\n\n.item_scroll {\n  background: url('item_bg.png') 0 0/100% 100% no-repeat;\n}\n\n.banner-bg ion-item {\n  --background: url('banner_bg.png');\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaG9tZS9FOlxcbWVoYXItYmFuay1hcHAtbWFzdGVyXFxtZWhhci1iYW5rLWFwcC1tYXN0ZXIvc3JjXFxhcHBcXGhvbWVcXGhvbWUucGFnZS5zY3NzIiwic3JjL2FwcC9ob21lL2hvbWUucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsZ0JBQUE7RUFDQSxnQkFBQTtBQ0NGOztBRENBO0VBQ0Usc0RBQUE7QUNFRjs7QURDQTtFQUNHLGtDQUFBO0FDRUgiLCJmaWxlIjoic3JjL2FwcC9ob21lL2hvbWUucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLndlbGNvbWUtY2FyZCBpbWcge1xuICBtYXgtaGVpZ2h0OiAzNXZoO1xuICBvdmVyZmxvdzogaGlkZGVuO1xufVxuLml0ZW1fc2Nyb2xse1xuICBiYWNrZ3JvdW5kOiB1cmwoLi4vLi4vYXNzZXRzL2ltZ3MvaXRlbV9iZy5wbmcpIDAgMC8xMDAlIDEwMCUgbm8tcmVwZWF0O1xufVxuXG4uYmFubmVyLWJnIGlvbi1pdGVte1xuICAgLS1iYWNrZ3JvdW5kOiB1cmwoLi4vLi4vYXNzZXRzL2ltZ3MvYmFubmVyX2JnLnBuZykgO1xuXHR9XG4iLCIud2VsY29tZS1jYXJkIGltZyB7XG4gIG1heC1oZWlnaHQ6IDM1dmg7XG4gIG92ZXJmbG93OiBoaWRkZW47XG59XG5cbi5pdGVtX3Njcm9sbCB7XG4gIGJhY2tncm91bmQ6IHVybCguLi8uLi9hc3NldHMvaW1ncy9pdGVtX2JnLnBuZykgMCAwLzEwMCUgMTAwJSBuby1yZXBlYXQ7XG59XG5cbi5iYW5uZXItYmcgaW9uLWl0ZW0ge1xuICAtLWJhY2tncm91bmQ6IHVybCguLi8uLi9hc3NldHMvaW1ncy9iYW5uZXJfYmcucG5nKSA7XG59Il19 */"

/***/ }),

/***/ "./src/app/home/home.page.ts":
/*!***********************************!*\
  !*** ./src/app/home/home.page.ts ***!
  \***********************************/
/*! exports provided: HomePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePage", function() { return HomePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");



var HomePage = /** @class */ (function () {
    function HomePage(menu) {
        this.menu = menu;
    }
    HomePage.prototype.ngOnInit = function () {
        this.menu.enable(true);
    };
    HomePage.ctorParameters = function () { return [
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["MenuController"] }
    ]; };
    HomePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-home',
            template: __webpack_require__(/*! raw-loader!./home.page.html */ "./node_modules/raw-loader/index.js!./src/app/home/home.page.html"),
            styles: [__webpack_require__(/*! ./home.page.scss */ "./src/app/home/home.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["MenuController"]])
    ], HomePage);
    return HomePage;
}());



/***/ })

}]);
//# sourceMappingURL=home-home-module-es5.js.map