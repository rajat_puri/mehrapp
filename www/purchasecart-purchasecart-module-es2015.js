(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["purchasecart-purchasecart-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/purchasecart/purchasecart.page.html":
/*!*******************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/purchasecart/purchasecart.page.html ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header  >\n  <ion-toolbar class=\"toolbar-blue\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <ion-title class=\"title-top\">\n    Cart\n    </ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\n<ion-grid>\n  <ion-row> \n  <ion-col size=\"12\">  \n\t<div class=\"transaction-head\">View Cart</div>\t\n </ion-col>\n  </ion-row>\n</ion-grid>\n\n\n<!-- List of Text Items -->\n<ion-list>\n  <ion-item lines=\"none\" >\n    <ion-label text-wrap>\n\t<div class=\"transaction-head\">Seed 4</div>\t\n\t<div class=\"small-gray\">₹ 189.00</div>\n\t</ion-label>\n\t  <ion-badge  color=\"light\" slot=\"end\">  <span class=\"text-right-all quantity-outer\"> <span class=\"quantity-gray-min\">-</span> <span  class=\"quantity\">3</span> <span class=\"quantity-gray-plus\">+</span> </span> </ion-badge> \n\t  </ion-item>\n\t  \n\t    <ion-item lines=\"none\" >\n    <ion-label text-wrap>\n\t<div class=\"transaction-head\">Rice Seed 1024 No. </div>\t\n\t<div class=\"small-gray\">₹ 52.00</div>\n\t</ion-label>\n\t  <ion-badge  color=\"light\" slot=\"end\">  <span class=\"text-right-all quantity-outer\"> <span class=\"quantity-gray-min\">-</span> <span  class=\"quantity\">1</span> <span class=\"quantity-gray-plus\">+</span> </span> </ion-badge> \n\t  </ion-item>\n\t  \n\t    <ion-item lines=\"none\" >\n    <ion-label text-wrap>\n\t<div class=\"transaction-head\">UREA 152</div>\t\n\t<div class=\"small-gray\">₹ 420.00</div>\n\t</ion-label>\n\t  <ion-badge  color=\"light\" slot=\"end\">  <span class=\"text-right-all quantity-outer\"> <span class=\"quantity-gray-min\">-</span> <span  class=\"quantity\">2</span> <span class=\"quantity-gray-plus\">+</span> </span> </ion-badge> \n\t  </ion-item>\n\t  </ion-list>\n<ion-grid>\n  <ion-row> <ion-col size=\"12\">  \n  <div class=\"transaction-head\">Apply Coupon</div>\t\n   </ion-col>\n    <ion-col size=\"7\">        \n<ion-input class=\"input-white-bg\" placeholder=\"Enter Coupon Code\"></ion-input>\n    </ion-col>\n    <ion-col size=\"5\" class=\"successfully-footer\">\n   <button class=\"btngreen\" ion-button block>Apply Coupon</button>\n    </ion-col>\n  \n   \n  </ion-row>\n</ion-grid>\n\n\n<ion-grid>\n  <ion-row> \n  <ion-col size=\"12\">  \n\t<div class=\"transaction-head\">Bill Details</div>\t\n </ion-col>\n  </ion-row>\n</ion-grid>\n\n \n\n\n<ion-grid class=\"bg-white\">\n  <ion-row class=\"bg-white\"> \n  <ion-col size=\"8\">  \n\t<div class=\"small-gray\">Item Total</div>\t\n </ion-col>\n  <ion-col size=\"4\">  \n\t<div class=\"small-gray text-right-all\">₹ 1200</div>\t\n </ion-col>\n  </ion-row>\n  \n  <ion-row> \n  <ion-col size=\"8\">  \n\t<div class=\"small-gray\">Coupon Discount</div>\t\n </ion-col>\n  <ion-col size=\"4\">  \n\t<div class=\"small-gray text-right-all\">₹ 120</div>\t\n </ion-col>\n  </ion-row>\n  \n   <ion-row> \n  <ion-col size=\"8\">  \n\t<div class=\"small-gray\">TAX 3%</div>\t\n </ion-col>\n  <ion-col size=\"4\">  \n\t<div class=\"small-gray text-right-all\">₹ 68</div>\t\n </ion-col>\n  </ion-row>\n  <ion-row> \n  <ion-col size=\"8\">  \n\t<div class=\"small-gray\">To Pay </div>\t\n </ion-col>\n  <ion-col size=\"4\">  \n\t<div class=\"small-gray text-right-all\">₹ 1388</div>\t\n </ion-col>\n  </ion-row>\n</ion-grid>\n \n\n \n\n\n \n\n\n</ion-content>\n<ion-footer no-border class=\"successfully-footer\">\n\n   <ion-grid class=\"successfully-footer\">\n  <ion-row class=\"successfully-footer\">\n    <ion-col size=\"7\" class=\"successfully-footer\">\n        <button class=\"btnlightblue\" ion-button block>8 Items | ₹ 1388.00</button>\n    </ion-col>\n    <ion-col size=\"5\" class=\"successfully-footer\">\n   <button [routerLink]=\"['/payment']\" class=\"btngreen\" ion-button block>Proceed to pay</button>\n    </ion-col>\n  \n   \n  </ion-row>\n</ion-grid>\n\n</ion-footer>\n"

/***/ }),

/***/ "./src/app/purchasecart/purchasecart-routing.module.ts":
/*!*************************************************************!*\
  !*** ./src/app/purchasecart/purchasecart-routing.module.ts ***!
  \*************************************************************/
/*! exports provided: PurchasecartPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PurchasecartPageRoutingModule", function() { return PurchasecartPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _purchasecart_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./purchasecart.page */ "./src/app/purchasecart/purchasecart.page.ts");




const routes = [
    {
        path: '',
        component: _purchasecart_page__WEBPACK_IMPORTED_MODULE_3__["PurchasecartPage"]
    }
];
let PurchasecartPageRoutingModule = class PurchasecartPageRoutingModule {
};
PurchasecartPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], PurchasecartPageRoutingModule);



/***/ }),

/***/ "./src/app/purchasecart/purchasecart.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/purchasecart/purchasecart.module.ts ***!
  \*****************************************************/
/*! exports provided: PurchasecartPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PurchasecartPageModule", function() { return PurchasecartPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _purchasecart_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./purchasecart-routing.module */ "./src/app/purchasecart/purchasecart-routing.module.ts");
/* harmony import */ var _purchasecart_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./purchasecart.page */ "./src/app/purchasecart/purchasecart.page.ts");







let PurchasecartPageModule = class PurchasecartPageModule {
};
PurchasecartPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _purchasecart_routing_module__WEBPACK_IMPORTED_MODULE_5__["PurchasecartPageRoutingModule"]
        ],
        declarations: [_purchasecart_page__WEBPACK_IMPORTED_MODULE_6__["PurchasecartPage"]]
    })
], PurchasecartPageModule);



/***/ }),

/***/ "./src/app/purchasecart/purchasecart.page.scss":
/*!*****************************************************!*\
  !*** ./src/app/purchasecart/purchasecart.page.scss ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-content {\n  --ion-background-color:#f3f5f5;\n}\n\nion-item {\n  --ion-background-color:#fff;\n}\n\nion-footer {\n  background-color: #f3f5f5 !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcHVyY2hhc2VjYXJ0L0U6XFxtZWhhci1iYW5rLWFwcC1tYXN0ZXJcXG1laGFyLWJhbmstYXBwLW1hc3Rlci9zcmNcXGFwcFxccHVyY2hhc2VjYXJ0XFxwdXJjaGFzZWNhcnQucGFnZS5zY3NzIiwic3JjL2FwcC9wdXJjaGFzZWNhcnQvcHVyY2hhc2VjYXJ0LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLDhCQUFBO0FDQ0o7O0FERUE7RUFDRywyQkFBQTtBQ0NIOztBRENBO0VBRVEsb0NBQUE7QUNDUiIsImZpbGUiOiJzcmMvYXBwL3B1cmNoYXNlY2FydC9wdXJjaGFzZWNhcnQucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWNvbnRlbnR7XG4gICAgLS1pb24tYmFja2dyb3VuZC1jb2xvcjojZjNmNWY1O1xufVxuXG5pb24taXRlbXtcbiAgIC0taW9uLWJhY2tncm91bmQtY29sb3I6I2ZmZjtcbn1cbmlvbi1mb290ZXJ7XG4gICAgICAgXG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNmM2Y1ZjUgIWltcG9ydGFudDtcbiAgXG59IiwiaW9uLWNvbnRlbnQge1xuICAtLWlvbi1iYWNrZ3JvdW5kLWNvbG9yOiNmM2Y1ZjU7XG59XG5cbmlvbi1pdGVtIHtcbiAgLS1pb24tYmFja2dyb3VuZC1jb2xvcjojZmZmO1xufVxuXG5pb24tZm9vdGVyIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2YzZjVmNSAhaW1wb3J0YW50O1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/purchasecart/purchasecart.page.ts":
/*!***************************************************!*\
  !*** ./src/app/purchasecart/purchasecart.page.ts ***!
  \***************************************************/
/*! exports provided: PurchasecartPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PurchasecartPage", function() { return PurchasecartPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let PurchasecartPage = class PurchasecartPage {
    constructor() { }
    ngOnInit() {
    }
};
PurchasecartPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-purchasecart',
        template: __webpack_require__(/*! raw-loader!./purchasecart.page.html */ "./node_modules/raw-loader/index.js!./src/app/purchasecart/purchasecart.page.html"),
        styles: [__webpack_require__(/*! ./purchasecart.page.scss */ "./src/app/purchasecart/purchasecart.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], PurchasecartPage);



/***/ })

}]);
//# sourceMappingURL=purchasecart-purchasecart-module-es2015.js.map