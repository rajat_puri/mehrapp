(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["deposit-deposit-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/deposit/deposit.page.html":
/*!*********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/deposit/deposit.page.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header  >\n  <ion-toolbar class=\"toolbar-blue\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <ion-title class=\"title-top\">\n   Deposit\n    </ion-title>\n  </ion-toolbar>\n</ion-header>\n\n\n\n<ion-content>\n<ion-grid class=\"pa15\">\n <ion-row>\n  \t<ion-col class=\"pa0\" size=\"12\">\n\t\n    <ion-label class=\"product-label pa0\">Select Account</ion-label>\n\t</ion-col>\n\t\t<ion-col size=\"12\" class=\"pa0 ma0\">\n    <ion-select class=\"ma0 select-white\" value=\"brown\" okText=\"OK\" cancelText=\"Cancel\">\n      <ion-select-option value=\"brown\">Saving Account</ion-select-option>\n      <ion-select-option value=\"blonde\">Loan Account</ion-select-option>\n      <ion-select-option value=\"black\">RD Account </ion-select-option>\n    </ion-select>\n\n<ion-label class=\"product-label\">Enter Emount</ion-label>\n<ion-input class=\"input-white\" placeholder=\"500\"></ion-input>\n\t\n <button [routerLink]=\"['/fundtransfersuccessfully']\" class=\"btngreen mt20\" ion-button block>Pay From This acc</button>\n<button [routerLink]=\"['/fundtransfersuccessfully']\" class=\"btnlightblue mt20\" ion-button block>Pay With UPI</button>\n\t\n\t\n</ion-col>\n\t\n  </ion-row>\n  \n  \n</ion-grid>\n\n</ion-content>\n\n"

/***/ }),

/***/ "./src/app/deposit/deposit-routing.module.ts":
/*!***************************************************!*\
  !*** ./src/app/deposit/deposit-routing.module.ts ***!
  \***************************************************/
/*! exports provided: DepositPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DepositPageRoutingModule", function() { return DepositPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _deposit_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./deposit.page */ "./src/app/deposit/deposit.page.ts");




var routes = [
    {
        path: '',
        component: _deposit_page__WEBPACK_IMPORTED_MODULE_3__["DepositPage"]
    }
];
var DepositPageRoutingModule = /** @class */ (function () {
    function DepositPageRoutingModule() {
    }
    DepositPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
        })
    ], DepositPageRoutingModule);
    return DepositPageRoutingModule;
}());



/***/ }),

/***/ "./src/app/deposit/deposit.module.ts":
/*!*******************************************!*\
  !*** ./src/app/deposit/deposit.module.ts ***!
  \*******************************************/
/*! exports provided: DepositPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DepositPageModule", function() { return DepositPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _deposit_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./deposit-routing.module */ "./src/app/deposit/deposit-routing.module.ts");
/* harmony import */ var _deposit_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./deposit.page */ "./src/app/deposit/deposit.page.ts");







var DepositPageModule = /** @class */ (function () {
    function DepositPageModule() {
    }
    DepositPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _deposit_routing_module__WEBPACK_IMPORTED_MODULE_5__["DepositPageRoutingModule"]
            ],
            declarations: [_deposit_page__WEBPACK_IMPORTED_MODULE_6__["DepositPage"]]
        })
    ], DepositPageModule);
    return DepositPageModule;
}());



/***/ }),

/***/ "./src/app/deposit/deposit.page.scss":
/*!*******************************************!*\
  !*** ./src/app/deposit/deposit.page.scss ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2RlcG9zaXQvZGVwb3NpdC5wYWdlLnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/deposit/deposit.page.ts":
/*!*****************************************!*\
  !*** ./src/app/deposit/deposit.page.ts ***!
  \*****************************************/
/*! exports provided: DepositPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DepositPage", function() { return DepositPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var DepositPage = /** @class */ (function () {
    function DepositPage() {
    }
    DepositPage.prototype.ngOnInit = function () {
    };
    DepositPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-deposit',
            template: __webpack_require__(/*! raw-loader!./deposit.page.html */ "./node_modules/raw-loader/index.js!./src/app/deposit/deposit.page.html"),
            styles: [__webpack_require__(/*! ./deposit.page.scss */ "./src/app/deposit/deposit.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], DepositPage);
    return DepositPage;
}());



/***/ })

}]);
//# sourceMappingURL=deposit-deposit-module-es5.js.map