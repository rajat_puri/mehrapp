(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["recoverypassword-recoverypassword-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/recoverypassword/recoverypassword.page.html":
/*!***************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/recoverypassword/recoverypassword.page.html ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-title>recoverypassword</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\n</ion-content>\n"

/***/ }),

/***/ "./src/app/recoverypassword/recoverypassword-routing.module.ts":
/*!*********************************************************************!*\
  !*** ./src/app/recoverypassword/recoverypassword-routing.module.ts ***!
  \*********************************************************************/
/*! exports provided: RecoverypasswordPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RecoverypasswordPageRoutingModule", function() { return RecoverypasswordPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _recoverypassword_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./recoverypassword.page */ "./src/app/recoverypassword/recoverypassword.page.ts");




const routes = [
    {
        path: '',
        component: _recoverypassword_page__WEBPACK_IMPORTED_MODULE_3__["RecoverypasswordPage"]
    }
];
let RecoverypasswordPageRoutingModule = class RecoverypasswordPageRoutingModule {
};
RecoverypasswordPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], RecoverypasswordPageRoutingModule);



/***/ }),

/***/ "./src/app/recoverypassword/recoverypassword.module.ts":
/*!*************************************************************!*\
  !*** ./src/app/recoverypassword/recoverypassword.module.ts ***!
  \*************************************************************/
/*! exports provided: RecoverypasswordPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RecoverypasswordPageModule", function() { return RecoverypasswordPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _recoverypassword_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./recoverypassword-routing.module */ "./src/app/recoverypassword/recoverypassword-routing.module.ts");
/* harmony import */ var _recoverypassword_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./recoverypassword.page */ "./src/app/recoverypassword/recoverypassword.page.ts");







let RecoverypasswordPageModule = class RecoverypasswordPageModule {
};
RecoverypasswordPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _recoverypassword_routing_module__WEBPACK_IMPORTED_MODULE_5__["RecoverypasswordPageRoutingModule"]
        ],
        declarations: [_recoverypassword_page__WEBPACK_IMPORTED_MODULE_6__["RecoverypasswordPage"]]
    })
], RecoverypasswordPageModule);



/***/ }),

/***/ "./src/app/recoverypassword/recoverypassword.page.scss":
/*!*************************************************************!*\
  !*** ./src/app/recoverypassword/recoverypassword.page.scss ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3JlY292ZXJ5cGFzc3dvcmQvcmVjb3ZlcnlwYXNzd29yZC5wYWdlLnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/recoverypassword/recoverypassword.page.ts":
/*!***********************************************************!*\
  !*** ./src/app/recoverypassword/recoverypassword.page.ts ***!
  \***********************************************************/
/*! exports provided: RecoverypasswordPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RecoverypasswordPage", function() { return RecoverypasswordPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let RecoverypasswordPage = class RecoverypasswordPage {
    constructor() { }
    ngOnInit() {
    }
};
RecoverypasswordPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-recoverypassword',
        template: __webpack_require__(/*! raw-loader!./recoverypassword.page.html */ "./node_modules/raw-loader/index.js!./src/app/recoverypassword/recoverypassword.page.html"),
        styles: [__webpack_require__(/*! ./recoverypassword.page.scss */ "./src/app/recoverypassword/recoverypassword.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], RecoverypasswordPage);



/***/ })

}]);
//# sourceMappingURL=recoverypassword-recoverypassword-module-es2015.js.map