(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["myaccount-myaccount-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/myaccount/myaccount.page.html":
/*!*************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/myaccount/myaccount.page.html ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header no-border >\n  <ion-toolbar class=\"toolbar-blue\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <ion-title class=\"title-top\">\n      Account\n    </ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n<div class=\"card-bg-full pt0\">\n\n\n\n\n<ion-row [routerLink]=\"['/profile']\" >\n    <ion-col class=\"pl5\" size=\"3\">\n      <img class=\"profile-pic-acc-page\" src=\"assets/imgs/profile.png\">\n    </ion-col>\n    <ion-col class=\"pt25\"  size=\"9\">\n      <div class=\"transaction-head white\">Gaurav Mehta</div>\t\n\t<div class=\"transaction-date white\">View Profile</div>\n    </ion-col>\n    \n  </ion-row>\n\n\n\n</div>\n\n\n<!-- List of Text Items -->\n<ion-list>\n  <ion-item [routerLink]=\"['/changethumimpression']\">\n    <ion-label >\n\t<div class=\"transaction-head\">Change mPin & Thumb impression </div>\t\n\t<div class=\"transaction-date\">Change login mPin and thumb impression</div>\n\t</ion-label>\n  </ion-item>\n  \n   <ion-item [routerLink]=\"['/termsandconditions']\">\n    <ion-label>\n\t<div class=\"transaction-head\">Terms & Conditions</div>\t\n\t<div class=\"transaction-date\">Terms & Conditions of App </div>\n\t</ion-label>\n  </ion-item>\n  \n    <ion-item [routerLink]=\"['/faq']\">\n    <ion-label>\n\t<div class=\"transaction-head\">FAQs</div>\t\n\t<div class=\"transaction-date\"> Frequently Asked Questions</div>\n\t</ion-label>\n  </ion-item>\n  \n  <ion-item >\n    <ion-label>\n\t<div class=\"transaction-head\">24X7 Support </div>\t\n\t<div class=\"transaction-date\">Get your problem solved with 24x7 support tems</div>\n\t</ion-label>\n  </ion-item>\n  \n   <ion-item [routerLink]=\"['/login']\">\n    <ion-label>\n\t<div class=\"transaction-head\">Logout </div>\t\n\t<div class=\"transaction-date\">Logout from App</div>\n\t</ion-label>\n  </ion-item>\n    \n</ion-list>\n\n</ion-content>\n"

/***/ }),

/***/ "./src/app/myaccount/myaccount-routing.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/myaccount/myaccount-routing.module.ts ***!
  \*******************************************************/
/*! exports provided: MyaccountPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MyaccountPageRoutingModule", function() { return MyaccountPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _myaccount_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./myaccount.page */ "./src/app/myaccount/myaccount.page.ts");




var routes = [
    {
        path: '',
        component: _myaccount_page__WEBPACK_IMPORTED_MODULE_3__["MyaccountPage"]
    }
];
var MyaccountPageRoutingModule = /** @class */ (function () {
    function MyaccountPageRoutingModule() {
    }
    MyaccountPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
        })
    ], MyaccountPageRoutingModule);
    return MyaccountPageRoutingModule;
}());



/***/ }),

/***/ "./src/app/myaccount/myaccount.module.ts":
/*!***********************************************!*\
  !*** ./src/app/myaccount/myaccount.module.ts ***!
  \***********************************************/
/*! exports provided: MyaccountPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MyaccountPageModule", function() { return MyaccountPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _myaccount_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./myaccount-routing.module */ "./src/app/myaccount/myaccount-routing.module.ts");
/* harmony import */ var _myaccount_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./myaccount.page */ "./src/app/myaccount/myaccount.page.ts");







var MyaccountPageModule = /** @class */ (function () {
    function MyaccountPageModule() {
    }
    MyaccountPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _myaccount_routing_module__WEBPACK_IMPORTED_MODULE_5__["MyaccountPageRoutingModule"]
            ],
            declarations: [_myaccount_page__WEBPACK_IMPORTED_MODULE_6__["MyaccountPage"]]
        })
    ], MyaccountPageModule);
    return MyaccountPageModule;
}());



/***/ }),

/***/ "./src/app/myaccount/myaccount.page.scss":
/*!***********************************************!*\
  !*** ./src/app/myaccount/myaccount.page.scss ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".card-bg-full ion-item {\n  --ion-background-color:#3880ff;\n}\n\n.card-bg-full ion-list {\n  --ion-background-color:#3880ff;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbXlhY2NvdW50L0U6XFxtZWhhci1iYW5rLWFwcC1tYXN0ZXJcXG1laGFyLWJhbmstYXBwLW1hc3Rlci9zcmNcXGFwcFxcbXlhY2NvdW50XFxteWFjY291bnQucGFnZS5zY3NzIiwic3JjL2FwcC9teWFjY291bnQvbXlhY2NvdW50LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNHLDhCQUFBO0FDQ0g7O0FEQ0E7RUFDRyw4QkFBQTtBQ0VIIiwiZmlsZSI6InNyYy9hcHAvbXlhY2NvdW50L215YWNjb3VudC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuY2FyZC1iZy1mdWxsIGlvbi1pdGVte1xuICAgLS1pb24tYmFja2dyb3VuZC1jb2xvcjojMzg4MGZmO1xufVxuLmNhcmQtYmctZnVsbCBpb24tbGlzdHtcbiAgIC0taW9uLWJhY2tncm91bmQtY29sb3I6IzM4ODBmZjtcbn0iLCIuY2FyZC1iZy1mdWxsIGlvbi1pdGVtIHtcbiAgLS1pb24tYmFja2dyb3VuZC1jb2xvcjojMzg4MGZmO1xufVxuXG4uY2FyZC1iZy1mdWxsIGlvbi1saXN0IHtcbiAgLS1pb24tYmFja2dyb3VuZC1jb2xvcjojMzg4MGZmO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/myaccount/myaccount.page.ts":
/*!*********************************************!*\
  !*** ./src/app/myaccount/myaccount.page.ts ***!
  \*********************************************/
/*! exports provided: MyaccountPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MyaccountPage", function() { return MyaccountPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var MyaccountPage = /** @class */ (function () {
    function MyaccountPage() {
    }
    MyaccountPage.prototype.ngOnInit = function () {
    };
    MyaccountPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-myaccount',
            template: __webpack_require__(/*! raw-loader!./myaccount.page.html */ "./node_modules/raw-loader/index.js!./src/app/myaccount/myaccount.page.html"),
            styles: [__webpack_require__(/*! ./myaccount.page.scss */ "./src/app/myaccount/myaccount.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], MyaccountPage);
    return MyaccountPage;
}());



/***/ })

}]);
//# sourceMappingURL=myaccount-myaccount-module-es5.js.map