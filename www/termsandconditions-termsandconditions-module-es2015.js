(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["termsandconditions-termsandconditions-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/termsandconditions/termsandconditions.page.html":
/*!*******************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/termsandconditions/termsandconditions.page.html ***!
  \*******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar class=\"toolbar-blue\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <ion-title class=\"title-top\">\n  Terms and Conditions\n    </ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n<ion-card>\n  <ion-card-header class=\"pb5\">\n  \n    <ion-card-title class=\"black-heading\"> Lorem Ipsum is</ion-card-title>\n  </ion-card-header>\n  <ion-card-content>\n   Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, \n  </ion-card-content>\n</ion-card>\n<ion-card>\n  <ion-card-header class=\"pb5\">\n  \n    <ion-card-title class=\"black-heading\"> Lorem Ipsum is</ion-card-title>\n  </ion-card-header>\n  <ion-card-content>\n   Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, \n  </ion-card-content>\n</ion-card>\n\n<ion-card>\n  <ion-card-header class=\"pb5\">\n  \n    <ion-card-title class=\"black-heading\"> Lorem Ipsum is</ion-card-title>\n  </ion-card-header>\n  <ion-card-content>\n   Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, \n  </ion-card-content>\n</ion-card>\n\n\n\n</ion-content>"

/***/ }),

/***/ "./src/app/termsandconditions/termsandconditions-routing.module.ts":
/*!*************************************************************************!*\
  !*** ./src/app/termsandconditions/termsandconditions-routing.module.ts ***!
  \*************************************************************************/
/*! exports provided: TermsandconditionsPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TermsandconditionsPageRoutingModule", function() { return TermsandconditionsPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _termsandconditions_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./termsandconditions.page */ "./src/app/termsandconditions/termsandconditions.page.ts");




const routes = [
    {
        path: '',
        component: _termsandconditions_page__WEBPACK_IMPORTED_MODULE_3__["TermsandconditionsPage"]
    }
];
let TermsandconditionsPageRoutingModule = class TermsandconditionsPageRoutingModule {
};
TermsandconditionsPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], TermsandconditionsPageRoutingModule);



/***/ }),

/***/ "./src/app/termsandconditions/termsandconditions.module.ts":
/*!*****************************************************************!*\
  !*** ./src/app/termsandconditions/termsandconditions.module.ts ***!
  \*****************************************************************/
/*! exports provided: TermsandconditionsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TermsandconditionsPageModule", function() { return TermsandconditionsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _termsandconditions_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./termsandconditions-routing.module */ "./src/app/termsandconditions/termsandconditions-routing.module.ts");
/* harmony import */ var _termsandconditions_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./termsandconditions.page */ "./src/app/termsandconditions/termsandconditions.page.ts");







let TermsandconditionsPageModule = class TermsandconditionsPageModule {
};
TermsandconditionsPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _termsandconditions_routing_module__WEBPACK_IMPORTED_MODULE_5__["TermsandconditionsPageRoutingModule"]
        ],
        declarations: [_termsandconditions_page__WEBPACK_IMPORTED_MODULE_6__["TermsandconditionsPage"]]
    })
], TermsandconditionsPageModule);



/***/ }),

/***/ "./src/app/termsandconditions/termsandconditions.page.scss":
/*!*****************************************************************!*\
  !*** ./src/app/termsandconditions/termsandconditions.page.scss ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3Rlcm1zYW5kY29uZGl0aW9ucy90ZXJtc2FuZGNvbmRpdGlvbnMucGFnZS5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/termsandconditions/termsandconditions.page.ts":
/*!***************************************************************!*\
  !*** ./src/app/termsandconditions/termsandconditions.page.ts ***!
  \***************************************************************/
/*! exports provided: TermsandconditionsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TermsandconditionsPage", function() { return TermsandconditionsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let TermsandconditionsPage = class TermsandconditionsPage {
    constructor() { }
    ngOnInit() {
    }
};
TermsandconditionsPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-termsandconditions',
        template: __webpack_require__(/*! raw-loader!./termsandconditions.page.html */ "./node_modules/raw-loader/index.js!./src/app/termsandconditions/termsandconditions.page.html"),
        styles: [__webpack_require__(/*! ./termsandconditions.page.scss */ "./src/app/termsandconditions/termsandconditions.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], TermsandconditionsPage);



/***/ })

}]);
//# sourceMappingURL=termsandconditions-termsandconditions-module-es2015.js.map