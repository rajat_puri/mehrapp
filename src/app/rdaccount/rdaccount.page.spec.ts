import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RdaccountPage } from './rdaccount.page';

describe('RdaccountPage', () => {
  let component: RdaccountPage;
  let fixture: ComponentFixture<RdaccountPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RdaccountPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RdaccountPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
