import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RdaccountPage } from './rdaccount.page';

const routes: Routes = [
  {
    path: '',
    component: RdaccountPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RdaccountPageRoutingModule {}
