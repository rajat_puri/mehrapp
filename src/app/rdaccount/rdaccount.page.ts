import { Component, OnInit } from '@angular/core';
import { MenuController, LoadingController, AlertController } from '@ionic/angular';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { UserService } from "../../services/user.service";
import { CookieService } from "../../services/cookie.service";
import { Router } from "@angular/router";
@Component({
  selector: 'app-rdaccount',
  templateUrl: './rdaccount.page.html',
  styleUrls: ['./rdaccount.page.scss'],
  providers: [UserService, CookieService]

})
export class RdaccountPage implements OnInit {

  startDate: any = '01/04/2016';
  endDate: any = '01/04/2020'
  ledgerDetails: any = [];
  allProducts: any = [];
  productSelected: any = ""
  constructor(public alertController: AlertController, public loadingController: LoadingController, private userService: UserService, private cookieService: CookieService) { }

  ngOnInit() {
    this.fetchProducts();
  }


  async presentAlert(type, subHeader, message) {

    const alert = await this.alertController.create({
      header: type,
      subHeader: subHeader,
      message: message,
      buttons: ['OK']
    });

    await alert.present();
  }


  async fetchLedger() {
    this.ledgerDetails = [];
    if (this.productSelected == "") {
      this.presentAlert("Error", "", "Please Select a Product")
      return false;
    }
    var dataToSend = {
      startDate: this.startDate,
      endDate: this.endDate,
      accTye: '5',
      productId: this.productSelected
    }
    const loading = await this.loadingController.create({
      message: 'loading...',

    });
    await loading.present();
    this.userService.fetchLedger(dataToSend).subscribe((res: any) => {

      loading.dismiss()
      if (res != '') {
        var details = JSON.parse(res);
        this.ledgerDetails = details[0];
        this.ledgerDetails.ledgerdetail.splice(0, 1)
        this.ledgerDetails.ledgerdetail[this.ledgerDetails.ledgerdetail.length - 1].Amount = this.ledgerDetails.ledgerdetail[this.ledgerDetails.ledgerdetail.length - 1].amtrunningbal
        console.log(this.ledgerDetails)
      }


    },
      (err) => {
        console.log(err);
        loading.dismiss();

        this.presentAlert("Error", "", "Please Select Correct Dates")

      });

  }
  onChange(event) {
    this.ledgerDetails = [];
  }


  fetchProducts() {



    this.userService.fetchProducts('5').subscribe((res: any) => {

      console.log(JSON.parse(res))
      this.allProducts = JSON.parse(res)

    },
      (err) => {
        console.log(err);

      });

  }


}
