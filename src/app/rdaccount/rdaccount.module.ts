import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RdaccountPageRoutingModule } from './rdaccount-routing.module';

import { RdaccountPage } from './rdaccount.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RdaccountPageRoutingModule
  ],
  declarations: [RdaccountPage]
})
export class RdaccountPageModule {}
