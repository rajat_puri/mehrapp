import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FingerprintAIO } from '@ionic-native/fingerprint-aio/ngx';
import { AndroidFingerprintAuth } from '@ionic-native/android-fingerprint-auth/ngx';
import { IonicModule } from '@ionic/angular';

import { ChangePasswordPageRoutingModule } from './changepassword-routing.module';

import { ChangePasswordPage } from './changepassword.page';

import {
  FormBuilder,
  FormGroup,
  FormControl,
  FormsModule,
  ReactiveFormsModule,
  Validators,
  FormArray
} from "@angular/forms";


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    ChangePasswordPageRoutingModule
  ],
  declarations: [ChangePasswordPage],
  providers: [FingerprintAIO, AndroidFingerprintAuth]
})
export class ChangePasswordModule { }
