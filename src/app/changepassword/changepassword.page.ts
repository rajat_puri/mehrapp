import { Component, OnInit } from '@angular/core';
import { MenuController, AlertController, LoadingController } from '@ionic/angular';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { LoginService } from "../../services/login.service";
import { CookieService } from "../../services/cookie.service";
import { Router } from "@angular/router";

import { FingerprintAIO } from '@ionic-native/fingerprint-aio/ngx';
import { AndroidFingerprintAuth } from '@ionic-native/android-fingerprint-auth/ngx';
import { async } from 'q';

@Component({
  selector: 'app-changepassword',
  templateUrl: './changepassword.page.html',
  styleUrls: ['./changepassword.page.scss'],
  providers: [LoginService, CookieService]
})
export class ChangePasswordPage implements OnInit {
  changePasswordForm: FormGroup;
  emailRequiredError: boolean = false;
  passwordRequiredError: boolean = false;
  loading: any = {};
  constructor(private fb: FormBuilder,
    private menu: MenuController,
    private router: Router,
    public alertController: AlertController,
    public loadingController: LoadingController,
    private loginService: LoginService,
    private cookieService: CookieService,
    private faio: FingerprintAIO,
    private androidFingerprintAuth: AndroidFingerprintAuth) {
    this.initLoginForm();
  }

  ngOnInit() {


  }







  initLoginForm() {
    this.changePasswordForm = this.fb.group({

      oldPassword: ["", Validators.required],
      newPassword: ["", Validators.required],

    });
  }
  async presentAlert(type, subHeader, message) {

    const alert = await this.alertController.create({
      header: type,
      subHeader: subHeader,
      message: message,
      buttons: ['OK']
    });

    await alert.present();
  }

  async changePassword() {
    if (this.changePasswordForm.valid) {

      let dataToSend = {
        oldPassword: this.changePasswordForm.value.oldPassword,
        newPassword: this.changePasswordForm.value.newPassword
      }
      const loading = await this.loadingController.create({
        message: 'loading...',

      });
      await loading.present();
      this.loginService.changePassword(dataToSend).subscribe(res => {
        console.log(res);
        loading.dismiss();
        // this.loading = false;
        if (res == 1) {
          this.presentAlert("Success", "", "password succesfully changed")



        } else {
          this.presentAlert("Error", "Not Matched", "Old Password not matched")

          //loading.onDidDismiss();

        }

      })
    } else {
      this.emailRequiredError = true;
      this.passwordRequiredError = true;

    }
  }

}
