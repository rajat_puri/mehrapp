import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoanaccountPage } from './loanaccount.page';

const routes: Routes = [
  {
    path: '',
    component: LoanaccountPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LoanaccountPageRoutingModule {}
