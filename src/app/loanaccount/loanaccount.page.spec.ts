import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { LoanaccountPage } from './loanaccount.page';

describe('LoanaccountPage', () => {
  let component: LoanaccountPage;
  let fixture: ComponentFixture<LoanaccountPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoanaccountPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(LoanaccountPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
