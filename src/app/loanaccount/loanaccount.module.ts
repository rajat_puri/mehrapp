import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LoanaccountPageRoutingModule } from './loanaccount-routing.module';

import { LoanaccountPage } from './loanaccount.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LoanaccountPageRoutingModule
  ],
  declarations: [LoanaccountPage]
})
export class LoanaccountPageModule {}
