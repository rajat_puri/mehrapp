import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PurchasecartPageRoutingModule } from './purchasecart-routing.module';

import { PurchasecartPage } from './purchasecart.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PurchasecartPageRoutingModule
  ],
  declarations: [PurchasecartPage]
})
export class PurchasecartPageModule {}
