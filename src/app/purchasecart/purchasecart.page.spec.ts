import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PurchasecartPage } from './purchasecart.page';

describe('PurchasecartPage', () => {
  let component: PurchasecartPage;
  let fixture: ComponentFixture<PurchasecartPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PurchasecartPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PurchasecartPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
