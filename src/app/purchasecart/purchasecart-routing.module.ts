import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PurchasecartPage } from './purchasecart.page';

const routes: Routes = [
  {
    path: '',
    component: PurchasecartPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PurchasecartPageRoutingModule {}
