import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FundtransferPage } from './fundtransfer.page';

const routes: Routes = [
  {
    path: '',
    component: FundtransferPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FundtransferPageRoutingModule {}
