import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FundtransferdetailsPageRoutingModule } from './fundtransferdetails-routing.module';

import { FundtransferdetailsPage } from './fundtransferdetails.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FundtransferdetailsPageRoutingModule
  ],
  declarations: [FundtransferdetailsPage]
})
export class FundtransferdetailsPageModule {}
