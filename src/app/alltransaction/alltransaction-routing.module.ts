import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AlltransactionPage } from './alltransaction.page';

const routes: Routes = [
  {
    path: '',
    component: AlltransactionPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AlltransactionPageRoutingModule {}
