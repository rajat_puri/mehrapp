import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AlltransactionPageRoutingModule } from './alltransaction-routing.module';

import { AlltransactionPage } from './alltransaction.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AlltransactionPageRoutingModule
  ],
  declarations: [AlltransactionPage]
})
export class AlltransactionPageModule {}
