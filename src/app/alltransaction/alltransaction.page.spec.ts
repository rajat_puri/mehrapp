import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AlltransactionPage } from './alltransaction.page';

describe('AlltransactionPage', () => {
  let component: AlltransactionPage;
  let fixture: ComponentFixture<AlltransactionPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlltransactionPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AlltransactionPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
