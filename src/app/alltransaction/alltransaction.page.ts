import { Component, OnInit } from '@angular/core';
import { MenuController, LoadingController, AlertController } from '@ionic/angular';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { UserService } from "../../services/user.service";
import { CookieService } from "../../services/cookie.service";
import { Router } from "@angular/router";

@Component({
  selector: 'app-alltransaction',
  templateUrl: './alltransaction.page.html',
  styleUrls: ['./alltransaction.page.scss'],
  providers: [UserService, CookieService]
})
export class AlltransactionPage implements OnInit {
  startDate: any = '01/04/2016';
  endDate: any = '01/04/2020'
  ledgerDetails: any = [];
  constructor(public alertController: AlertController, public loadingController: LoadingController, private userService: UserService, private cookieService: CookieService) { }

  ngOnInit() {



  }


  async presentAlert() {
    const alert = await this.alertController.create({
      header: 'Error',
      subHeader: 'wrong values passed',
      message: 'Please select correct values',
      buttons: ['OK']
    });

    await alert.present();
  }


  async fetchLedger() {



    var dataToSend = {
      startDate: this.startDate,
      endDate: this.endDate
    }
    const loading = await this.loadingController.create({
      message: 'loading...',

    });
    await loading.present();
    this.userService.fetchLedger(dataToSend).subscribe((res: any) => {

      loading.dismiss()
      var details = JSON.parse(res);
      this.ledgerDetails = details[0];
      console.log(this.ledgerDetails)

    },
      (err) => {
        console.log(err);
        loading.dismiss();

        this.presentAlert()
      });

  }

}
