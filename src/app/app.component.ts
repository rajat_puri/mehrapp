import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  public appPages = [
    {
      title: 'Dashboard',
      url: '/home',
      icon: 'home'
    },



    {
      title: 'My Transaction',
      url: '/alltransaction',
      icon: 'list'
    },
    {
      title: 'Saving Ledger',
      url: '/savingAccount',
      icon: 'list'
    },
    {
      title: 'Loan Ledger',
      url: '/loanaccount',
      icon: 'list'
    },
    {
      title: 'RD Ledger',
      url: '/rdaccount',
      icon: 'list'
    },
    {
      title: 'FD Ledger',
      url: '/fdaccount',
      icon: 'list'
    },

    {
      title: 'Share Money Ledger',
      url: '/sharemoney',
      icon: 'list'
    },
    {
      title: 'Agritool Booked',
      url: '/agritoolbooked',
      icon: 'list'
    },
    {
      title: 'Agritool Detail',
      url: '/agritooldetail',
      icon: 'list'
    },
    {
      title: 'Aritool Booking',
      url: '/aritoolbooking',
      icon: 'list'
    },
    {
      title: 'Fund Transfer',
      url: '/fundtransfer',
      icon: 'list'
    },
    {
      title: 'Fund Transfer Successfully',
      url: '/fundtransfersuccessfully',
      icon: 'list'
    },
    {
      title: 'History',
      url: '/history',
      icon: 'list'
    },
    {
      title: 'My Account',
      url: '/myaccount',
      icon: 'list'
    },
    {
      title: 'Profile',
      url: '/profile',
      icon: 'list'
    },
    {
      title: 'Purchase Booking',
      url: '/purchasebooking',
      icon: 'list'
    },
    {
      title: 'Purchase Cart',
      url: '/purchasecart',
      icon: 'list'
    },
    {
      title: 'Payment',
      url: '/payment',
      icon: 'list'
    },
    {
      title: 'Recurring Deposit ',
      url: '/allrd',
      icon: 'list'
    },
    {
      title: 'Recurring Detail ',
      url: '/rddetail',
      icon: 'list'
    },
    {
      title: 'Search',
      url: '/search',
      icon: 'list'
    },
    {
      title: 'Filter',
      url: '/filter',
      icon: 'list'
    },
    {
      title: 'Change Password',
      url: '/changepassword',
      icon: 'list'
    },
    {
      title: 'Logout',
      url: '/logout',
      icon: 'power'
    }
  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }


}
