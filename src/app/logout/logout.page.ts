import { Component, OnInit } from '@angular/core';
import { MenuController, AlertController } from '@ionic/angular';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { LoginService } from "../../services/login.service";
import { CookieService } from "../../services/cookie.service";
import { Router } from "@angular/router";
import { LoadingController } from '@ionic/angular';
import { FingerprintAIO } from '@ionic-native/fingerprint-aio/ngx';
import { AndroidFingerprintAuth } from '@ionic-native/android-fingerprint-auth/ngx';
import { Location } from '@angular/common';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.page.html',
  styleUrls: ['./logout.page.scss'],
  providers: [LoginService, CookieService]
})
export class LogoutPage implements OnInit {

  loading: boolean = false;
  constructor(private fb: FormBuilder,
    private menu: MenuController,
    private router: Router,
    public alertController: AlertController,
    public loadingController: LoadingController,
    private loginService: LoginService,
    private cookieService: CookieService,
    private location: Location
  ) {


  }

  ngOnInit() {


  }


  ionViewDidEnter() {
    this.menu.enable(false);

    this.presentAlertConfirm()
  }
  async presentAlertConfirm() {
    const alert = await this.alertController.create({
      header: 'Logout!',
      message: 'Want to <strong>Logout?</strong>!!!',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
            this.router.navigate(['/home/'])

          }
        }, {
          text: 'Sure',
          handler: () => {
            console.log('Confirm Okay');
            this.logout()

          }
        }
      ]
    });

    await alert.present();
  }


  logout() {

    this.loginService.logout().subscribe(res => {
      console.log(res)
      // this.loading = false;
      if (res) {
        this.cookieService.removeCookies()
        this.router.navigate(['/login/'])

      } else {

        this.loading = false;

      }

    })
  }


}
