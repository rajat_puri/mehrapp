import { Component, OnInit } from '@angular/core';
import { MenuController, LoadingController, AlertController } from '@ionic/angular';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { UserService } from "../../services/user.service";
import { CookieService } from "../../services/cookie.service";
import { Router } from "@angular/router";
import jsPDF from 'jspdf';

var doc = new jsPDF()
declare var $: any;
@Component({
  selector: 'app-savingaccount',
  templateUrl: './savingaccount.page.html',
  styleUrls: ['./savingaccount.page.scss'],
  providers: [UserService, CookieService]
})
export class savingaccountPage implements OnInit {
  startDate: any = '01/04/2016';
  endDate: any = '01/04/2020'
  ledgerDetails: any = [];
  allProducts: any = [];
  productSelected: any = ""
  constructor(public alertController: AlertController, public loadingController: LoadingController, private userService: UserService, private cookieService: CookieService) { }

  ngOnInit() {
    this.fetchProducts();



  }


  async presentAlert(type, subHeader, message) {

    const alert = await this.alertController.create({
      header: type,
      subHeader: subHeader,
      message: message,
      buttons: ['OK']
    });

    await alert.present();
  }


  async fetchLedger() {
    this.ledgerDetails = [];
    if (this.productSelected == "") {
      this.presentAlert("Error", "", "Please Select a Product")
      return false;
    }
    var dataToSend = {
      startDate: this.startDate,
      endDate: this.endDate,
      accTye: '2',
      productId: this.productSelected
    }
    const loading = await this.loadingController.create({
      message: 'loading...',

    });
    await loading.present();
    this.userService.fetchLedger(dataToSend).subscribe((res: any) => {

      loading.dismiss()
      if (res != '') {
        var details = JSON.parse(res);
        this.ledgerDetails = details[0];
        this.ledgerDetails.ledgerdetail.splice(0, 1)
        this.ledgerDetails.ledgerdetail[this.ledgerDetails.ledgerdetail.length - 1].Amount = this.ledgerDetails.ledgerdetail[this.ledgerDetails.ledgerdetail.length - 1].amtrunningbal
        console.log(this.ledgerDetails)
      }


    },
      (err) => {
        console.log(err);
        loading.dismiss();

        this.presentAlert("Error", "", "Please Select Correct Dates")

      });

  }

  onChange(event) {
    this.ledgerDetails = [];
  }
  fetchProducts() {



    this.userService.fetchProducts('2').subscribe((res: any) => {

      console.log(JSON.parse(res))
      this.allProducts = JSON.parse(res)

    },
      (err) => {
        console.log(err);

      });

  }

  downloadPdf() {



    var doc = new jsPDF();

    // We'll make our own renderer to skip this editor
    var specialElementHandlers = {
      '#editor': function (element, renderer) {
        return true;
      }
    };

    // All units are in the set measurement for the document
    // This can be changed to "pt" (points), "mm" (Default), "cm", "in"
    doc.fromHTML(`<html><table><tr><td>12</td></tr></table></html>`, 15, 15, {
      'width': 170,
      'elementHandlers': specialElementHandlers
    });

    doc.save('Saving Account.pdf');
    console.log(this.ledgerDetails.ledgerdetail)
    this.ledgerDetails.ledgerdetail.forEach(function (detail, i) {
      doc.text(20, 10 + (i * 10),
        "S.No: " + i +
        "Vr. Date: " + detail.date +
        "Vr. Date: " + detail.date +
        "Particulars: " + detail.particulars +
        "Debit: " + detail.intdr +
        "Credit: " + detail.intcr
      );
    });

  }


}
