import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { savingaccountPage } from './savingaccount.page';

const routes: Routes = [
  {
    path: '',
    component: savingaccountPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SavingaccountPageRoutingModule { }
