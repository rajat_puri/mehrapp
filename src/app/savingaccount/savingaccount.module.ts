import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SavingaccountPageRoutingModule } from './savingaccount-routing.module';

import { savingaccountPage } from './savingaccount.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SavingaccountPageRoutingModule
  ],
  declarations: [savingaccountPage]
})
export class SavingaccountPageModule { }
