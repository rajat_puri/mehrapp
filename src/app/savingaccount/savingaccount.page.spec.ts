import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SavingaccountPage } from './savingaccount.page';

describe('SavingaccountPage', () => {
  let component: SavingaccountPage;
  let fixture: ComponentFixture<SavingaccountPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SavingaccountPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SavingaccountPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
