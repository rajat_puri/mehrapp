import { Component, OnInit } from '@angular/core';
import { MenuController, AlertController } from '@ionic/angular';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { LoginService } from "../../services/login.service";
import { CookieService } from "../../services/cookie.service";
import { Router } from "@angular/router";
import { LoadingController } from '@ionic/angular';
import { FingerprintAIO } from '@ionic-native/fingerprint-aio/ngx';
import { AndroidFingerprintAuth } from '@ionic-native/android-fingerprint-auth/ngx';
import { async } from 'q';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
  providers: [LoginService, CookieService]
})
export class LoginPage implements OnInit {
  loginForm: FormGroup;
  emailRequiredError: boolean = false;
  passwordRequiredError: boolean = false;
  loading: any = {};
  constructor(private fb: FormBuilder,
    private menu: MenuController,
    private router: Router,
    public alertController: AlertController,
    public loadingController: LoadingController,
    private loginService: LoginService,
    private cookieService: CookieService,
    private faio: FingerprintAIO,
    private androidFingerprintAuth: AndroidFingerprintAuth) {
    this.initLoginForm();
  }

  ngOnInit() {
    this.menu.enable(false);


  }

  async presentAlert(type, subHeader, message) {

    const alert = await this.alertController.create({
      header: type,
      subHeader: subHeader,
      message: message,
      buttons: ['OK']
    });

    await alert.present();
  }


  async scanFingerPrint() {

    this.androidFingerprintAuth.isAvailable()
      .then((result) => {

        if (result.isAvailable) {
          // it is available

          this.androidFingerprintAuth.encrypt({ clientId: 'myAppName', username: 'myUsername', password: 'myPassword' })
            .then(result => {
              if (result.withFingerprint) {
                console.log('Successfully encrypted credentials.');
                console.log('Encrypted credentials: ' + result.token);
                this.router.navigate(['/home/'])

              } else if (result.withBackup) {
                this.router.navigate(['/home/'])

                console.log('Successfully authenticated with backup password!');
              } else {
                this.presentAlert("Error", " ", "Didn\'t authenticate!")
              }

            })
            .catch(error => {
              if (error === this.androidFingerprintAuth.ERRORS.FINGERPRINT_CANCELLED) {
                console.log('Fingerprint authentication cancelled');
                this.presentAlert("Error", " ", "Fingerprint authentication cancelled")

              } else { this.presentAlert("Error", " ", error) }
            });

        } else {
          // fingerprint auth isn't available
        }
      })
      .catch(error => console.error(error));
  }





  initLoginForm() {
    this.loginForm = this.fb.group({

      userId: ["1000000010001", Validators.required],
      password: ["12345678", Validators.required],

    });
  }


  async login() {
    if (this.loginForm.valid) {

      let dataToSend = {
        customer_id: this.loginForm.value.userId,
        password: this.loginForm.value.password
      }
      const loading = await this.loadingController.create({
        message: 'loading...',

      });
      await loading.present();
      this.loginService.login(dataToSend).subscribe(res => {
        console.log(res);
        loading.dismiss();
        // this.loading = false;
        if (res == 1) {

          this.router.navigate(['/home/'])

          this.cookieService.setItem('CustomerId', this.loginForm.value.userId);



        } else {
          //loading.onDidDismiss();
          this.presentAlert("Error", " ", "Invalid User Id or password ")


        }

      })
    } else {
      this.emailRequiredError = true;
      this.passwordRequiredError = true;

    }
  }

}
