import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PurchasebookingPage } from './purchasebooking.page';

const routes: Routes = [
  {
    path: '',
    component: PurchasebookingPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PurchasebookingPageRoutingModule {}
