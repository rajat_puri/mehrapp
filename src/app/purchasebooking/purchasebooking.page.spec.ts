import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PurchasebookingPage } from './purchasebooking.page';

describe('PurchasebookingPage', () => {
  let component: PurchasebookingPage;
  let fixture: ComponentFixture<PurchasebookingPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PurchasebookingPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PurchasebookingPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
