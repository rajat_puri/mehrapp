import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PurchasebookingPageRoutingModule } from './purchasebooking-routing.module';

import { PurchasebookingPage } from './purchasebooking.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PurchasebookingPageRoutingModule
  ],
  declarations: [PurchasebookingPage]
})
export class PurchasebookingPageModule {}
