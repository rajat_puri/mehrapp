import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AllrdPage } from './allrd.page';

const routes: Routes = [
  {
    path: '',
    component: AllrdPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AllrdPageRoutingModule {}
