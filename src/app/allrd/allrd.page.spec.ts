import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AllrdPage } from './allrd.page';

describe('AllrdPage', () => {
  let component: AllrdPage;
  let fixture: ComponentFixture<AllrdPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllrdPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AllrdPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
