import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AllrdPageRoutingModule } from './allrd-routing.module';

import { AllrdPage } from './allrd.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AllrdPageRoutingModule
  ],
  declarations: [AllrdPage]
})
export class AllrdPageModule {}
