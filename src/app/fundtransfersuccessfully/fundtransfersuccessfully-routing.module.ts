import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FundtransfersuccessfullyPage } from './fundtransfersuccessfully.page';

const routes: Routes = [
  {
    path: '',
    component: FundtransfersuccessfullyPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FundtransfersuccessfullyPageRoutingModule {}
