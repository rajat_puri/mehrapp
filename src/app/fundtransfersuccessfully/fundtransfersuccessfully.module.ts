import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FundtransfersuccessfullyPageRoutingModule } from './fundtransfersuccessfully-routing.module';

import { FundtransfersuccessfullyPage } from './fundtransfersuccessfully.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FundtransfersuccessfullyPageRoutingModule
  ],
  declarations: [FundtransfersuccessfullyPage]
})
export class FundtransfersuccessfullyPageModule {}
