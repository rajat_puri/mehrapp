import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FundtransfersuccessfullyPage } from './fundtransfersuccessfully.page';

describe('FundtransfersuccessfullyPage', () => {
  let component: FundtransfersuccessfullyPage;
  let fixture: ComponentFixture<FundtransfersuccessfullyPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FundtransfersuccessfullyPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FundtransfersuccessfullyPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
