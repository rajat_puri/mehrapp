import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ChangethumimpressionPage } from './changethumimpression.page';

const routes: Routes = [
  {
    path: '',
    component: ChangethumimpressionPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ChangethumimpressionPageRoutingModule {}
