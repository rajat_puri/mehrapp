import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ChangethumimpressionPage } from './changethumimpression.page';

describe('ChangethumimpressionPage', () => {
  let component: ChangethumimpressionPage;
  let fixture: ComponentFixture<ChangethumimpressionPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChangethumimpressionPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ChangethumimpressionPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
