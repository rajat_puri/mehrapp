import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ChangethumimpressionPageRoutingModule } from './changethumimpression-routing.module';

import { ChangethumimpressionPage } from './changethumimpression.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ChangethumimpressionPageRoutingModule
  ],
  declarations: [ChangethumimpressionPage]
})
export class ChangethumimpressionPageModule {}
