import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then(m => m.HomePageModule)
  },
  {
    path: 'changepassword',
    loadChildren: () => import('./changepassword/changepassword.module').then(m => m.ChangePasswordModule)
  },
  {
    path: 'list',
    loadChildren: () => import('./list/list.module').then(m => m.ListPageModule)
  },
  {
    path: 'logout',
    loadChildren: () => import('./logout/logout.module').then(m => m.LogoutPageModule)
  },
  {
    path: 'alltransaction',
    loadChildren: () => import('./alltransaction/alltransaction.module').then(m => m.AlltransactionPageModule)
  },

  {
    path: 'savingAccount',
    loadChildren: () => import('./savingaccount/savingaccount.module').then(m => m.SavingaccountPageModule)
  },
  {
    path: 'loanaccount',
    loadChildren: () => import('./loanaccount/loanaccount.module').then(m => m.LoanaccountPageModule)
  },

  {
    path: 'rdaccount',
    loadChildren: () => import('./rdaccount/rdaccount.module').then(m => m.RdaccountPageModule)
  },
  {
    path: 'sharemoney',
    loadChildren: () => import('./sharemoney/sharemoney.module').then(m => m.SharemoneyPageModule)
  },

  {
    path: 'agritoolbooked',
    loadChildren: () => import('./agritoolbooked/agritoolbooked.module').then(m => m.AgritoolbookedPageModule)
  },

  {
    path: 'aritoolbooking',
    loadChildren: () => import('./aritoolbooking/aritoolbooking.module').then(m => m.AritoolbookingPageModule)
  },
  {
    path: 'fundtransfer',
    loadChildren: () => import('./fundtransfer/fundtransfer.module').then(m => m.FundtransferPageModule)
  },
  {
    path: 'fundtransferdetails',
    loadChildren: () => import('./fundtransferdetails/fundtransferdetails.module').then(m => m.FundtransferdetailsPageModule)
  },
  {
    path: 'fundtransfersuccessfully',
    loadChildren: () => import('./fundtransfersuccessfully/fundtransfersuccessfully.module').then(m => m.FundtransfersuccessfullyPageModule)
  },
  {
    path: 'history',
    loadChildren: () => import('./history/history.module').then(m => m.HistoryPageModule)
  },
  {
    path: 'myaccount',
    loadChildren: () => import('./myaccount/myaccount.module').then(m => m.MyaccountPageModule)
  },
  {
    path: 'profile',
    loadChildren: () => import('./profile/profile.module').then(m => m.ProfilePageModule)
  },
  {
    path: 'purchasebooking',
    loadChildren: () => import('./purchasebooking/purchasebooking.module').then(m => m.PurchasebookingPageModule)
  },
  {
    path: 'purchasecart',
    loadChildren: () => import('./purchasecart/purchasecart.module').then(m => m.PurchasecartPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then(m => m.LoginPageModule)
  },
  {
    path: 'recoverypassword',
    loadChildren: () => import('./recoverypassword/recoverypassword.module').then(m => m.RecoverypasswordPageModule)
  },
  {
    path: 'payment',
    loadChildren: () => import('./payment/payment.module').then(m => m.PaymentPageModule)
  },
  {
    path: 'deposit',
    loadChildren: () => import('./deposit/deposit.module').then(m => m.DepositPageModule)
  },
  {
    path: 'faq',
    loadChildren: () => import('./faq/faq.module').then(m => m.FaqPageModule)
  },
  {
    path: 'termsandconditions',
    loadChildren: () => import('./termsandconditions/termsandconditions.module').then(m => m.TermsandconditionsPageModule)
  },
  {
    path: 'changethumimpression',
    loadChildren: () => import('./changethumimpression/changethumimpression.module').then(m => m.ChangethumimpressionPageModule)
  },
  {
    path: 'allrd',
    loadChildren: () => import('./allrd/allrd.module').then(m => m.AllrdPageModule)
  },
  {
    path: 'rddetail',
    loadChildren: () => import('./rddetail/rddetail.module').then(m => m.RddetailPageModule)
  },
  {
    path: 'search',
    loadChildren: () => import('./search/search.module').then(m => m.SearchPageModule)
  },
  {
    path: 'filter',
    loadChildren: () => import('./filter/filter.module').then(m => m.FilterPageModule)
  },
  {
    path: 'savingaccount',
    loadChildren: () => import('./savingaccount/savingaccount.module').then(m => m.SavingaccountPageModule)
  },
  {
    path: 'rdaccount',
    loadChildren: () => import('./rdaccount/rdaccount.module').then(m => m.RdaccountPageModule)
  },
  {
    path: 'fdaccount',
    loadChildren: () => import('./fdaccount/fdaccount.module').then(m => m.FdaccountPageModule)
  },
  {
    path: 'sharemoney',
    loadChildren: () => import('./sharemoney/sharemoney.module').then(m => m.SharemoneyPageModule)
  }



];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
