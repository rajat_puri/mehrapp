import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RddetailPage } from './rddetail.page';

const routes: Routes = [
  {
    path: '',
    component: RddetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RddetailPageRoutingModule {}
