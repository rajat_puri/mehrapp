import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RddetailPageRoutingModule } from './rddetail-routing.module';

import { RddetailPage } from './rddetail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RddetailPageRoutingModule
  ],
  declarations: [RddetailPage]
})
export class RddetailPageModule {}
