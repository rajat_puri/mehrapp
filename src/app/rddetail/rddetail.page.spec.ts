import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RddetailPage } from './rddetail.page';

describe('RddetailPage', () => {
  let component: RddetailPage;
  let fixture: ComponentFixture<RddetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RddetailPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RddetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
