import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AgritooldetailPageRoutingModule } from './agritooldetail-routing.module';

import { AgritooldetailPage } from './agritooldetail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AgritooldetailPageRoutingModule
  ],
  declarations: [AgritooldetailPage]
})
export class AgritooldetailPageModule {}
