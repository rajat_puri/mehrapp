import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AgritooldetailPage } from './agritooldetail.page';

describe('AgritooldetailPage', () => {
  let component: AgritooldetailPage;
  let fixture: ComponentFixture<AgritooldetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgritooldetailPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AgritooldetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
