import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AgritooldetailPage } from './agritooldetail.page';

const routes: Routes = [
  {
    path: '',
    component: AgritooldetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AgritooldetailPageRoutingModule {}
