import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AritoolbookingPage } from './aritoolbooking.page';

describe('AritoolbookingPage', () => {
  let component: AritoolbookingPage;
  let fixture: ComponentFixture<AritoolbookingPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AritoolbookingPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AritoolbookingPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
