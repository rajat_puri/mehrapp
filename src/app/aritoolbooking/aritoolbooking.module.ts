import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AritoolbookingPageRoutingModule } from './aritoolbooking-routing.module';

import { AritoolbookingPage } from './aritoolbooking.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AritoolbookingPageRoutingModule
  ],
  declarations: [AritoolbookingPage]
})
export class AritoolbookingPageModule {}
