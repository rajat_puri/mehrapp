import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AritoolbookingPage } from './aritoolbooking.page';

const routes: Routes = [
  {
    path: '',
    component: AritoolbookingPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AritoolbookingPageRoutingModule {}
