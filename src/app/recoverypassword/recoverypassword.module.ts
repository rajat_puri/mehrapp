import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RecoverypasswordPageRoutingModule } from './recoverypassword-routing.module';

import { RecoverypasswordPage } from './recoverypassword.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RecoverypasswordPageRoutingModule
  ],
  declarations: [RecoverypasswordPage]
})
export class RecoverypasswordPageModule {}
