import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { sharemoneyPage } from './sharemoney.page';

const routes: Routes = [
  {
    path: '',
    component: sharemoneyPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SharemoneyPageRoutingModule { }
