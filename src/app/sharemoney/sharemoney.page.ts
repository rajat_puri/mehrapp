import { Component, OnInit } from '@angular/core';
import { MenuController, LoadingController, AlertController } from '@ionic/angular';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { UserService } from "../../services/user.service";
import { CookieService } from "../../services/cookie.service";
import { Router } from "@angular/router";

@Component({
  selector: 'app-sharemoney',
  templateUrl: './sharemoney.page.html',
  styleUrls: ['./sharemoney.page.scss'],
  providers: [UserService, CookieService]
})
export class sharemoneyPage implements OnInit {
  startDate: any = '01/04/2016';
  endDate: any = '01/04/2020'
  ledgerDetails: any = [];
  allProducts: any = [];
  productSelected: any = ""
  constructor(public alertController: AlertController, public loadingController: LoadingController, private userService: UserService, private cookieService: CookieService) { }

  ngOnInit() {

  }


  async presentAlert(type, subHeader, message) {

    const alert = await this.alertController.create({
      header: type,
      subHeader: subHeader,
      message: message,
      buttons: ['OK']
    });

    await alert.present();
  }


  async fetchShareMoney() {
    this.ledgerDetails = [];

    var dataToSend = {
      startDate: this.startDate,
      endDate: this.endDate,
      accTye: '4',

    }
    const loading = await this.loadingController.create({
      message: 'loading...',

    });
    await loading.present();
    this.userService.fetchShareMoney(dataToSend).subscribe((res: any) => {

      loading.dismiss()
      if (res != '') {
        var details = JSON.parse(res);
        this.ledgerDetails = details[0];
        this.ledgerDetails.ledgerdetail.splice(0, 1)
        this.ledgerDetails.ledgerdetail[this.ledgerDetails.ledgerdetail.length - 1].Amount = this.ledgerDetails.ledgerdetail[this.ledgerDetails.ledgerdetail.length - 1].amtrunningbal
        console.log(this.ledgerDetails)
      }


    },
      (err) => {
        console.log(err);
        loading.dismiss();

        this.presentAlert("Error", "", "Please Select Correct Dates")

      });

  }



}
