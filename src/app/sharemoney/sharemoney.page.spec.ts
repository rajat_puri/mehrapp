import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SharemoneyPage } from './sharemoney.page';

describe('SharemoneyPage', () => {
  let component: SharemoneyPage;
  let fixture: ComponentFixture<SharemoneyPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SharemoneyPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SharemoneyPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
