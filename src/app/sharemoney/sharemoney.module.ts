import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SharemoneyPageRoutingModule } from './sharemoney-routing.module';

import { sharemoneyPage } from './sharemoney.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharemoneyPageRoutingModule
  ],
  declarations: [sharemoneyPage]
})
export class SharemoneyPageModule { }
