import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AgritoolbookedPage } from './agritoolbooked.page';

const routes: Routes = [
  {
    path: '',
    component: AgritoolbookedPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AgritoolbookedPageRoutingModule {}
