import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AgritoolbookedPageRoutingModule } from './agritoolbooked-routing.module';

import { AgritoolbookedPage } from './agritoolbooked.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AgritoolbookedPageRoutingModule
  ],
  declarations: [AgritoolbookedPage]
})
export class AgritoolbookedPageModule {}
