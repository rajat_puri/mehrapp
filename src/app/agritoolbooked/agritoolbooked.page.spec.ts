import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AgritoolbookedPage } from './agritoolbooked.page';

describe('AgritoolbookedPage', () => {
  let component: AgritoolbookedPage;
  let fixture: ComponentFixture<AgritoolbookedPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgritoolbookedPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AgritoolbookedPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
