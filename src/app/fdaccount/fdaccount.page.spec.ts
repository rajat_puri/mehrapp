import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FdaccountPage } from './fdaccount.page';

describe('FdaccountPage', () => {
  let component: FdaccountPage;
  let fixture: ComponentFixture<FdaccountPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FdaccountPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FdaccountPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
