import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FdaccountPageRoutingModule } from './fdaccount-routing.module';

import { FdaccountPage } from './fdaccount.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FdaccountPageRoutingModule
  ],
  declarations: [FdaccountPage]
})
export class FdaccountPageModule {}
