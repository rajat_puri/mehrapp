// import { window } from "rxjs/operator/window";
import { Injectable } from "@angular/core";
import { HttpClientModule, HttpClient, HttpHeaders } from '@angular/common/http';


import { map } from 'rxjs/operators';
import { catchError } from 'rxjs/operators';

import { Router, ActivatedRoute, Params } from "@angular/router";
import { environment } from "../environments/environment";
import { CookieService } from './cookie.service';
import { tokenName } from '@angular/compiler';


@Injectable()
export class LoginService {
    constructor(
        private router: Router,
        private http: HttpClient,
        private cookie: CookieService
    ) { }


    login(loginDetails) {

        var options = {};
        let headers = new HttpHeaders();
        headers.append("Content-Type", "application/json");
        // return this.http.post<any>('http://csas.meharsoft.com:85/routingmapi/api/routing',

        return this.http.post(environment.baseURL, { "APIName": "1", "CustomerId": `${loginDetails.customer_id}`, "InputParameters": `${loginDetails.customer_id}¤${loginDetails.password}` })
            .pipe(map(res => res));
    }

    logout() {

        var options = {};
        let headers = new Headers();
        headers.append("Content-Type", "application/json");
        let cId = this.cookie.getItem("CustomerId")

        return this.http.post(environment.baseURL, { "APIName": "2", "CustomerId": cId, "InputParameters": `${cId}¤${2}` })
            .pipe(map(res => res));
    }


    changePassword(dataToSend) {
        var options = {};
        let headers = new Headers();
        headers.append("Content-Type", "application/json");
        let cId = this.cookie.getItem("CustomerId")

        return this.http.post(environment.baseURL, { "APIName": "3", "CustomerId": cId, "InputParameters": `${cId}¤${dataToSend.oldPassword}¤${dataToSend.newPassword}` })
            .pipe(map(res => res));
    }




}
