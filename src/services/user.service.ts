// import { window } from "rxjs/operator/window";
import { Injectable } from "@angular/core";
import { HttpClientModule, HttpClient, HttpHeaders } from '@angular/common/http';


import { map } from 'rxjs/operators';
import { catchError } from 'rxjs/operators';

import { Router, ActivatedRoute, Params } from "@angular/router";
import { environment } from "../environments/environment";
import { CookieService } from './cookie.service';
import { tokenName } from '@angular/compiler';


@Injectable()
export class UserService {
    constructor(
        private router: Router,
        private http: HttpClient,
        private cookie: CookieService
    ) { }


    fetchLedger(ledgerPayload) {

        var options = {};
        let headers = new Headers();
        headers.append("Content-Type", "application/json");
        let fromDate = ledgerPayload.startDate;
        let toDate = ledgerPayload.endDate;
        let accType = ledgerPayload.accTye;
        let productId = ledgerPayload.productId;


        let cId = this.cookie.getItem("CustomerId")
        return this.http.post(environment.baseURL, { "APIName": "4", "CustomerId": cId, "InputParameters": `${cId}¤${fromDate}¤${toDate}¤${accType}¤${productId}` })
            .pipe(map(res => res));

    }

    fetchShareMoney(ledgerPayload) {

        var options = {};
        let headers = new Headers();
        headers.append("Content-Type", "application/json");
        let fromDate = ledgerPayload.startDate;
        let toDate = ledgerPayload.endDate;
        let accType = ledgerPayload.accTye;



        let cId = this.cookie.getItem("CustomerId")
        return this.http.post(environment.baseURL, { "APIName": "4", "CustomerId": cId, "InputParameters": `${cId}¤${fromDate}¤${toDate}¤${accType}¤0` })
            .pipe(map(res => res));

    }



    fetchProducts(accType) {

        var options = {};
        let headers = new Headers();
        headers.append("Content-Type", "application/json");


        let cId = this.cookie.getItem("CustomerId")

        return this.http.post(environment.baseURL, { "APIName": "5", "CustomerId": cId, "InputParameters": `${cId}¤${accType}` })
            .pipe(map(res => res));

    }





}
